﻿using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Contract;
using Infrastructure.Data.Interface;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Data.Test.Repositories
{
    [TestFixture]
    class TestRepositoryGeneric
    {

        private IDbContext _dbContext;
        private IWoinUserContract _reposory;

        [SetUp]
        public void SetUp() {

            _dbContext = new WoinContext();
            _reposory = new WoinUserContract(_dbContext);
        }


        [Test]
        public void TestWhereList()
        {
          
            List<WhereList> whereLists = new List<WhereList>
            {
                new WhereList { Key = "username", Value = "9BC1931BF58" },
                new WhereList { Condition = "OR", Key = "username" ,Value = "685265671DE"},
                
            };
            var user = _reposory.Where(whereLists).FirstOrDefault();

            Console.WriteLine(user.Builder().String());


        }
    }
}
