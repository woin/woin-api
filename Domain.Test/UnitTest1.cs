using Domain.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

using Infrastructure.Data.Contract;
using Infrastructure.Data;

namespace Domain.Test
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {

            Console.WriteLine(new WoinAdContract(new WoinContext())?.GetPreference(
             1
            )?.String());
          
        }

        [Test]
        public void TestWhereList()
        {
            string where = "1 = 1";

           


            List<WhereList> whereLists = new List<WhereList>();

            whereLists.Add(new WhereList {
            
                Key = "ejemplo",
                Value = "hola",
            });

            whereLists.Add(new WhereList
            {

                Key = "ejemplo",
                Value = "hola",
            });


        
            object[] parameters = new object[whereLists.Count > 0 ? whereLists.Count : 0];


            int i = 0;


            if(whereLists.Count > 0)
            {
                where = "";
                whereLists?.ForEach(x =>
                {
                    if (x.Key != string.Empty && x.Key != "" && x.Value != string.Empty && x.Value != "")
                    {

                        if (whereLists.Count-1 == i)
                        {
                            where += $"{x.Key} {x.Operator} {i} ";
                        }
                        else
                        {
                            where += $"{x.Key} {x.Operator} {i} {x.Condition} ";
                        }

                        parameters[i] = (x.Value);
                        i++;
                    }
                });
            }

            Console.WriteLine(whereLists.Count);
            Console.WriteLine(string.Join(",", parameters));
            Console.WriteLine(where);


        }

        [Test]
        public void TestBuilder()
        {
            Console.WriteLine(new WoinUser().String());
            int a = 5;
            Console.WriteLine(a.String());
            double b = 5;
            Console.WriteLine(b.String());
            Console.WriteLine(new WoinUserGroup().String());
            Console.WriteLine(new WoinTransaction().String());


        }


    }
}