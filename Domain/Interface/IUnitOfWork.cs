﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Ramiro González López <ramiro.gl@hotmail.com>
 * @name Base
 * @file Domain/Base
 * @observations use abstractions for Entities
 * @HU 0: Dominio
 * @task Task 1: Crear Interfaces del dominio
 */

using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interface
{
    public interface IUnitOfWork : IDisposable
    {
       public int Commit();

        public Task<int> Commit(bool Async);

        public  void CommitCurrent();

        public  void RollBackCurrent();


        public Task CommitCurrentAsync();

        public Task RollBackCurrentAsync();

        public  void CloseCurrent();

        public  Task CloseCurrentAsyn();

        public  IDbContextTransaction BeginTransaction();

        public Task<IDbContextTransaction> BeginTransactionAsync();
       

    }
}
