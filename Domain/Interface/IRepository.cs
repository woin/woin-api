/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Ramiro Gonz�lez L�pez <ramiro.gl@hotmail.com>
 * @name Base
 * @file Domain/Base
 * @observations use abstractions for Entities
 * @HU 0: Dominio
 * @task Task 1: Crear Interfaces del dominio
 */

using Domain.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Interface
{
    public interface IRepository<T> : IDisposable where T : class
    {
        public string Columns { set; get; }
        public T Add(T entity);
        public T Edit(T entity);
        public void Delete(object id);
        public void Delete(T entity);
        public T Find(object id);
        public void AddRange(IEnumerable<T> entities);
        public void DeleteRange(IEnumerable<T> entities);
        public void EditRange(IEnumerable<T> entities);
        public IEnumerable<T> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate, int pageIndex = 0, int pageSize = int.MaxValue);
       
        public Task<T> FindAsync(object id);
        public Task<IEnumerable<T>> GetAllAsync(int pageIndex = 0, int pageSize = int.MaxValue);
        public Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate, int pageIndex = 0, int pageSize = int.MaxValue);
         public Task<T> AddAsync(T entity);
        public Task AddRangeAsync(IEnumerable<T> entities);
        public Task DeleteAsync(T entity);
        public Task DeleteAsync(object id);
        public IEnumerable<T> FromSqlRaw(string query,  int pageIndex = 0, int pageSize = int.MaxValue, params object[] parameters);
        public Task<IEnumerable<T>> FromSqlRawAsync(string query, int pageIndex = 0, int pageSize = int.MaxValue, params object[] parameters);

        public IEnumerable<T> GetLike(string column, string value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, string value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, int value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, float value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, double value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, short value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, bool value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, BitArray value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, long value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(string column, char value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);


        public IEnumerable<T> WhereIn(string column, int[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> WhereIn(string column, string[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> WhereIn(string column, double[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> WhereIn(string column, float[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> WhereIn(string column, string values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<T> Where(List<WhereList> whereLists, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue, Expression<Func<T, object>> include = null);

        public IEnumerable<T> Where(string column, decimal value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue);


        public bool Any(Expression<Func<T, bool>> predicate = null);
    }
}