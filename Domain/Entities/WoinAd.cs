﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinAd : BaseEntity<WoinAd>
    {
        public WoinAd()
        {
            InverseAdParentNavigation = new HashSet<WoinAd>();
            WoinAdsCompanies = new HashSet<WoinAdCompany>();
            WoinAdsIndicators = new HashSet<WoinAdIndicator>();
            WoinAdsProducts = new HashSet<WoinAdProduct>();
            WoinAdsViews = new HashSet<WoinAdView>();
            WoinColumnsValues = new HashSet<WoinColumnValue>();
            WoinFreesAds = new HashSet<WoinFreeAd>();
            WoinMultimediasAds = new HashSet<WoinMultimediaAd>();
            WoinOnlinePurchases = new HashSet<WoinOnlinePurchase>();
            WoinPaidsAds = new HashSet<WoinPaidAd>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long InitialTime { get; set; }
        public long FinalTime { get; set; }
        public decimal Price { get; set; }
        public short GiftPercentage { get; set; }
        public short DiscountPercentage { get; set; }
        public int InitialStock { get; set; }
        public int CurrentStock { get; set; }
        public short State { get; set; }
        public short Type { get; set; }
        public int AdParent { get; set; }
        public int WoinerId { get; set; }
        public int SubcategoryId { get; set; }
        public int ProductId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinAd AdParentNavigation { get; set; } //parend Ad db
        public virtual WoinSubcategory Subcategory { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
        public virtual ICollection<WoinAd> InverseAdParentNavigation { get; set; } //no builder //lista de padres en woinAd
        public virtual ICollection<WoinAdCompany> WoinAdsCompanies { get; set; } 
        public virtual ICollection<WoinAdIndicator> WoinAdsIndicators { get; set; } //no builder
        public virtual ICollection<WoinAdProduct> WoinAdsProducts { get; set; }
        public virtual ICollection<WoinAdView> WoinAdsViews { get; set; }
        public virtual ICollection<WoinColumnValue> WoinColumnsValues { get; set; }
        public virtual ICollection<WoinFreeAd> WoinFreesAds { get; set; }
        public virtual ICollection<WoinMultimediaAd> WoinMultimediasAds { get; set; }
        public virtual ICollection<WoinOnlinePurchase> WoinOnlinePurchases { get; set; }
        public virtual ICollection<WoinPaidAd> WoinPaidsAds { get; set; }
    }


    public static partial class BuilderEntity
    {

       

        public static WoinAd ToObject(this WoinAd entity)
        {
			if (entity == null) return entity;
            return new WoinAd
            {
                UpdatedAt = entity.UpdatedAt,
                AdParent = entity.AdParent,
                CreatedAt = entity.CreatedAt,
                CurrentStock = entity.CurrentStock,
                Description = entity.Description,
                DiscountPercentage = entity.DiscountPercentage,
                FinalTime = entity.FinalTime,
                Id = entity.Id,
                GiftPercentage = entity.GiftPercentage,
                InitialStock = entity.InitialStock,
                InitialTime = entity.InitialTime,
                Price = entity.Price,
                ProductId = entity.ProductId,
                State = entity.State,
                SubcategoryId = entity.SubcategoryId,
                Title = entity.Title,
                Type = entity.Type,
                WoinerId = entity.WoinerId,
            };
        }

        public static WoinAd Builder(this WoinAd entity)
        {
			if (entity == null) return entity;
            WoinAd woinAd = new WoinAd
            {
                UpdatedAt = entity.UpdatedAt,
                AdParent = entity.AdParent,
                CreatedAt = entity.CreatedAt,
                CurrentStock = entity.CurrentStock,
                Description = entity.Description,
                DiscountPercentage = entity.DiscountPercentage,
                FinalTime = entity.FinalTime,
                Id = entity.Id,
                GiftPercentage = entity.GiftPercentage,
                InitialStock = entity.InitialStock,
                InitialTime = entity.InitialTime,
                Price = entity.Price,
                ProductId = entity.ProductId,
                State = entity.State,
                SubcategoryId = entity.SubcategoryId,
                Title = entity.Title,
                Type = entity.Type,
                WoinerId = entity.WoinerId,
                Subcategory = new WoinSubcategory
                {
                    State = entity.Subcategory.State,
                    CategoryId = entity.Subcategory.CategoryId,
                    CreatedAt = entity.Subcategory.CreatedAt,
                    Id = entity.Subcategory.Id,
                    Name = entity.Subcategory.Name,
                    ParentId = entity.Subcategory.ParentId,
                    UpdatedAt = entity.Subcategory.UpdatedAt,  
                },
                AdParentNavigation = new WoinAd
                {
                    UpdatedAt = entity.AdParentNavigation.UpdatedAt,
                    AdParent = entity.AdParentNavigation.AdParent,
                    CreatedAt = entity.AdParentNavigation.CreatedAt,
                    CurrentStock = entity.AdParentNavigation.CurrentStock,
                    Description = entity.AdParentNavigation.Description,
                    DiscountPercentage = entity.AdParentNavigation.DiscountPercentage,
                    FinalTime = entity.AdParentNavigation.FinalTime,
                    Id = entity.AdParentNavigation.Id,
                    GiftPercentage = entity.AdParentNavigation.GiftPercentage,
                    InitialStock = entity.AdParentNavigation.InitialStock,
                    InitialTime = entity.AdParentNavigation.InitialTime,
                    Price = entity.AdParentNavigation.Price,
                    ProductId = entity.AdParentNavigation.ProductId,
                    State = entity.AdParentNavigation.State,
                    SubcategoryId = entity.AdParentNavigation.SubcategoryId,
                    Title = entity.AdParentNavigation.Title,
                    Type = entity.AdParentNavigation.Type,
                    WoinerId = entity.AdParentNavigation.WoinerId,
                },
                Woiner = new WoinWoiner
                {
                    UpdatedAt = entity.Woiner.UpdatedAt,
                    Id = entity.Woiner.Id,
                    CreatedAt = entity.Woiner.CreatedAt,
                    Biography = entity.Woiner.Biography,
                    Codewoiner = entity.Woiner.Codewoiner,
                    PersonId = entity.Woiner.PersonId,
                    ProfileId = entity.Woiner.ProfileId,
                    Reference = entity.Woiner.Reference,
                    State = entity.Woiner.State,
                    RoleId = entity.Woiner.RoleId,
                    UserId = entity.Woiner.UserId,
                }
            };

            entity.WoinAdsCompanies.ToList().ForEach(x => {
                woinAd.WoinAdsCompanies.Add(new WoinAdCompany
                {
                    AdId = x.AdId,
                    CompanyId = x.CompanyId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    UpdatedAt = x.UpdatedAt
                });
            });


            entity.WoinAdsProducts.ToList().ForEach(x =>
            {
                woinAd.WoinAdsProducts.Add(new WoinAdProduct
                {
                    UpdatedAt = x.UpdatedAt,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    AdId = x.AdId,
                    ProdctId = x.ProdctId
                });
            });


            entity.WoinAdsViews.ToList().ForEach(x =>
            {
                woinAd.WoinAdsViews.Add(new WoinAdView
                {
                    AdId = x.AdId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Like = x.Like,
                    Preferred = x.Preferred,
                    Shared = x.Shared,
                    UpdatedAt = x.UpdatedAt,
                    WoinerId = x.WoinerId
                });
            });


            entity.WoinColumnsValues.ToList().ForEach(x =>
            {
                woinAd.WoinColumnsValues.Add(new WoinColumnValue
                {
                    UpdatedAt = x.UpdatedAt,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    AdId = x.AdId,
                    ColumnId = x.ColumnId,
                    State = x.State
                });

            });

            entity.WoinFreesAds.ToList().ForEach(x =>
            {
                woinAd.WoinFreesAds.Add(new WoinFreeAd
                {
                    AdId = x.AdId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    UpdatedAt = x.UpdatedAt,
                    End = x.End,
                    PacketId = x.PacketId,
                    Start = x.Start
                });
            });

            entity.WoinMultimediasAds.ToList().ForEach(x =>
            {
                woinAd.WoinMultimediasAds.Add(new WoinMultimediaAd
                {
                    UpdatedAt = x.UpdatedAt,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    AdId = x.AdId,
                    State = x.State,
                    MultimediaId = x.MultimediaId,
                });
            });

            entity.WoinOnlinePurchases.ToList().ForEach(x =>
            {
                woinAd.WoinOnlinePurchases.Add(new WoinOnlinePurchase
                {
                    AdId = x.AdId,
                    CreatedAt  = x.CreatedAt,
                    Id = x.Id,
                    UpdatedAt = x.UpdatedAt,
                    PurchaseId = x.PurchaseId,
                    Quantity = x.Quantity,
                    
                });
            });


            entity.WoinPaidsAds.ToList().ForEach(x =>
            {
                woinAd.WoinPaidsAds.Add(new WoinPaidAd
                {
                    UpdatedAt = x.UpdatedAt,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    AdId = x.AdId,
                    Start = x.Start,
                    End = x.End,
                    PacketId = x.PacketId,
                    TransactionId = x.TransactionId
                });
            });




            return woinAd;
        }
    }

}
