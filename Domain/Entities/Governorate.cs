﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class Governorate : BaseEntity<Governorate>
    {
        public Governorate()
        {
            Cities = new HashSet<City>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public int CountryId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<City> Cities { get; set; }
    }


    public static partial class BuilderEntity
    {

        public static Governorate ToObject(this Governorate entity)
        {
            if (entity == null) return entity;
            return new Governorate
            {
                State = entity.State,
                Id = entity.Id,
                CreatedAt = entity.CreatedAt,
                UpdatedAt = entity.UpdatedAt,
                CountryId = entity.CountryId,
                Name = entity.Name,
            };
        }

        public static Governorate Builder(this Governorate entity)
        {
            if (entity == null) return entity;
            Governorate governorate = new Governorate
            {
                State = entity.State,
                Id = entity.Id,
                CreatedAt = entity.CreatedAt,
                UpdatedAt = entity.UpdatedAt,
                CountryId = entity.CountryId,
                Name = entity.Name,
                Country = new Country
                {
                    Name = entity.Country.Name,
                    UpdatedAt = entity.Country.UpdatedAt,
                    CreatedAt = entity.Country.CreatedAt,
                    Id = entity.Country.Id,
                    State = entity.Country.State,
                },
            };

            governorate.Cities.ToList().ForEach(x =>
            {
                governorate.Cities.Add(new City
                {
                    CreatedAt = x.CreatedAt,
                    State = x.State,
                    Id = x.Id,
                    UpdatedAt = x.UpdatedAt,
                    Name = x.Name,
                    GovernorateId = x.GovernorateId
                });
            });

            return governorate;

        }
    }
}
