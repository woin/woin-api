﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinOnlinePurchase : BaseEntity<WoinOnlinePurchase>
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int PurchaseId { get; set; }
        public int AdId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinPurchase Purchase { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinOnlinePurchase ToObject(this WoinOnlinePurchase entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Purchase = null;
            return entity;
        }

        public static WoinOnlinePurchase Builder(this WoinOnlinePurchase entity)
        {
            if (entity == null) return entity;
            WoinOnlinePurchase toObject = entity.ToObject();
            toObject.Ad = entity.Ad.ToObject();
            toObject.Purchase = entity.Purchase.ToObject();
            return toObject;
        }
    }

}
