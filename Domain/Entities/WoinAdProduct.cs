﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinAdProduct : BaseEntity<WoinAdProduct>
    {
        public int Id { get; set; }
        public int AdId { get; set; }
        public int ProdctId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinProduct Prodct { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinAdProduct ToObject(this WoinAdProduct entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Prodct = null;
            return entity;
        }


        public static WoinAdProduct Builder(this WoinAdProduct entity)
        {
            if (entity == null) return entity;
            WoinAdProduct woinAdProduct = new WoinAdProduct
            {
                AdId = entity.AdId,
                CreatedAt = entity.CreatedAt,
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                ProdctId = entity.ProdctId,
                Ad = new WoinAd
                {
                    UpdatedAt = entity.Ad.UpdatedAt,
                    AdParent = entity.Ad.AdParent,
                    CreatedAt = entity.Ad.CreatedAt,
                    CurrentStock = entity.Ad.CurrentStock,
                    Description = entity.Ad.Description,
                    DiscountPercentage = entity.Ad.DiscountPercentage,
                    FinalTime = entity.Ad.FinalTime,
                    Id = entity.Ad.Id,
                    GiftPercentage = entity.Ad.GiftPercentage,
                    InitialStock = entity.Ad.InitialStock,
                    InitialTime = entity.Ad.InitialTime,
                    Price = entity.Ad.Price,
                    ProductId = entity.Ad.ProductId,
                    State = entity.Ad.State,
                    SubcategoryId = entity.Ad.SubcategoryId,
                    Title = entity.Ad.Title,
                    Type = entity.Ad.Type,
                    WoinerId = entity.Ad.WoinerId,
                },
                Prodct = new WoinProduct
                {
                    CreatedAt = entity.Prodct.CreatedAt,
                    Description = entity.Prodct.Description,
                    Id = entity.Prodct.Id,
                    Name = entity.Prodct.Name,
                    Price = entity.Prodct.Price,
                    SubcategoryId = entity.Prodct.SubcategoryId,
                    UpdatedAt = entity.Prodct.UpdatedAt,
                }
                
            };
           
            return woinAdProduct;
        }
    }


}
