﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinRolePermission : BaseEntity<WoinRolePermission>
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinPermission Permission { get; set; }
        public virtual WoinRole Role { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinRolePermission ToObject(this WoinRolePermission entity)
        {
            if (entity == null) return entity;
            entity.Permission = null;
            entity.Role = null;
            return entity;
        }

        public static WoinRolePermission Builder(this WoinRolePermission entity)
        {
            if (entity == null) return entity;
            WoinRolePermission toObject = entity.ToObject();
            toObject.Permission = entity.Permission.ToObject();
            toObject.Role = entity.Role.ToObject();
            return toObject;
        }
    }

}
