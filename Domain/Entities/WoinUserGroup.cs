﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinUserGroup : BaseEntity<WoinUserGroup>
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinGroup Group { get; set; }
        public virtual WoinUser User { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinUserGroup ToObject(this WoinUserGroup entity)
        {
            entity.Group = null;
            entity.User = null;
            return entity;
        }

        public static WoinUserGroup Builder(this WoinUserGroup entity)
        {
            WoinUserGroup toObject = entity.ToObject();
            toObject.Group = entity.Group.ToObject();
            toObject.User = entity.User.ToObject();
            return toObject;
        }
    }

}
