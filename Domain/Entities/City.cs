﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Domain.Entities
{
    public partial class City : BaseEntity<City>
    {

        public City()
        {
            WoinDocuments = new HashSet<WoinDocument>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public int GovernorateId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual Governorate Governorate { get; set; }
        public virtual ICollection<WoinDocument> WoinDocuments { get; set; }

        
    }

    public static partial class BuilderEntity
    {
        public static City ToObject(this City entity)
        {
            if (entity == null) return entity;
            return new City
            {
                CreatedAt = entity.CreatedAt,
                GovernorateId = entity.GovernorateId,
                Id = entity.Id,
                Name = entity.Name,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
            };
           
        }


  
        public static City Builder(this City entity)
        {
            if (entity == null) return entity;
            City city = new City
            {
                CreatedAt = entity.CreatedAt,
                Governorate =  new Governorate
                {
                    CountryId = entity.Governorate.CountryId,
                    CreatedAt = entity.Governorate.CreatedAt,
                    Id = entity.Governorate.Id,
                    Name = entity.Governorate.Name,
                    UpdatedAt = entity.Governorate.UpdatedAt,
                    State = entity.Governorate.State,
                },
                GovernorateId = entity.GovernorateId,
                Id = entity.Id,
                Name = entity.Name,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                
            };

            entity.WoinDocuments.ToList().ForEach(x =>
            {
                city.WoinDocuments.Add(new WoinDocument {
                     State = x.State,
                     CityId = x.CityId,
                     CreatedAt = x.CreatedAt,
                     Id = x.Id,
                     Number = x.Number,
                     PersonId = x.PersonId,
                     Type = x.Type,
                     UpdatedAt = x.UpdatedAt,
                });
            });


            return city;
        }
    }
}
