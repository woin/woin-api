﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinFreeAd : BaseEntity<WoinFreeAd>
    {
        public int Id { get; set; }
        public long Start { get; set; }
        public long End { get; set; }
        public int AdId { get; set; }
        public int PacketId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinAdPacket Packet { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinFreeAd ToObject(this WoinFreeAd entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Packet = null;
            return entity;
        }

        public static WoinFreeAd Builder(this WoinFreeAd entity)
        {
            if (entity == null) return entity;
            WoinFreeAd toObject = entity.ToObject();
            toObject.Ad = entity.Ad.ToObject();
            toObject.Packet = entity.Packet.ToObject();
            return toObject;
        }
    }

}
