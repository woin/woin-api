﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinDynamicKey : BaseEntity<WoinDynamicKey>
    {
        public int Id { get; set; }
        public int DynamicKey { get; set; }
        public short State { get; set; }
        public int WoinerId { get; set; }
        public int TransactionId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinTransaction Transaction { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinDynamicKey ToObject(this WoinDynamicKey entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            entity.Transaction = null;
            return entity;
        }

        public static WoinDynamicKey Builder(this WoinDynamicKey entity)
        {
            if (entity == null) return entity;
            WoinDynamicKey toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            toObject.Transaction = entity.Transaction.ToObject();
            return toObject;
        }
    }

}
