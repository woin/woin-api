﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinMovement : BaseEntity<WoinMovement>
    {
        public WoinMovement()
        {
            WoinRecharges = new HashSet<WoinRecharge>();
        }

        public int Id { get; set; }
        public decimal Amount { get; set; }
        public short State { get; set; }
        public short Type { get; set; }
        public int TransactionId { get; set; }
        public int WalletId { get; set; }
        public decimal CurrentBalance { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinTransaction Transaction { get; set; }
        public virtual WoinWallet Wallet { get; set; }
        public virtual ICollection<WoinRecharge> WoinRecharges { get; set; }
    }



    public static partial class BuilderEntity
    {
        public static WoinMovement ToObject(this WoinMovement entity)
        {
            if (entity == null) return entity;
            return new WoinMovement
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                TransactionId = entity.TransactionId,
                Amount = entity.Amount, 
                CurrentBalance = entity.CurrentBalance,
                Type = entity.Type,
                WalletId = entity.WalletId,
               

            };
        }


        public static WoinMovement Builder(this WoinMovement entity)
        {
            if (entity == null) return entity;
            WoinMovement woinMovement = entity.ToObject();
            woinMovement.Transaction = entity.Transaction.ToObject();
            woinMovement.Wallet = entity.Wallet.ToObject();

            entity.WoinRecharges.ToList().ForEach(x =>
            {
                woinMovement.WoinRecharges.Add(x.ToObject());
            });

            return woinMovement;
        }
    }


}
