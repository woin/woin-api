﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinAdIndicator : BaseEntity<WoinAdIndicator>
    {
        public int Id { get; set; }
        public int AdId { get; set; }
        public decimal Rating { get; set; }
        public int ExpectedViews { get; set; }
        public int CurrentViews { get; set; }
        public decimal ViewsPercentage { get; set; }
        public int Purchases { get; set; }
        public decimal PurchasesPercentage { get; set; }
        public long TotalDuration { get; set; }
        public long RemainingDuration { get; set; }
        public int Shared { get; set; }
        public int Likes { get; set; }
        public decimal PercentageLikesViews { get; set; }
        public decimal IndexView { get; set; }
        public decimal IndexPreferred { get; set; }
        public decimal IndexPurchased { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinAdIndicator ToObject(this WoinAdIndicator entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            return entity;
        }


        public static WoinAdIndicator Builder(this WoinAdIndicator entity)
        {
            if (entity == null) return entity;
            WoinAdIndicator toObject = entity.ToObject();

            toObject.Ad = new WoinAd
            {
                UpdatedAt = entity.Ad.UpdatedAt,
                AdParent = entity.Ad.AdParent,
                CreatedAt = entity.Ad.CreatedAt,
                CurrentStock = entity.Ad.CurrentStock,
                Description = entity.Ad.Description,
                DiscountPercentage = entity.Ad.DiscountPercentage,
                FinalTime = entity.Ad.FinalTime,
                Id = entity.Ad.Id,
                GiftPercentage = entity.Ad.GiftPercentage,
                InitialStock = entity.Ad.InitialStock,
                InitialTime = entity.Ad.InitialTime,
                Price = entity.Ad.Price,
                ProductId = entity.Ad.ProductId,
                State = entity.Ad.State,
                SubcategoryId = entity.Ad.SubcategoryId,
                Title = entity.Ad.Title,
                Type = entity.Ad.Type,
                WoinerId = entity.Ad.WoinerId,
            };

            return toObject;
        }
    }




}
