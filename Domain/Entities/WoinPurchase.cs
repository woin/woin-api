﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinPurchase : BaseEntity<WoinPurchase>
    {
        public int Id { get; set; }
        public decimal Points { get; set; }
        public decimal Gift { get; set; }
        public short State { get; set; }
        public string Detail { get; set; }
        public int? TransactionId { get; set; }
        public int PurchaserId { get; set; }
        public int VendorId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinWoiner Purchaser { get; set; }
        public virtual WoinTransaction Transaction { get; set; }
        public virtual WoinWoiner Vendor { get; set; }
        public virtual WoinFaceToFacePurchase WoinFaceToFacePurchases { get; set; }
        public virtual WoinOnlinePurchase WoinOnlinePurchases { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinPurchase ToObject(this WoinPurchase entity)
        {
            if (entity == null) return entity;
            entity.Purchaser = null;
            entity.Transaction = null;
            entity.Vendor = null;
            entity.WoinFaceToFacePurchases = null;
            entity.WoinOnlinePurchases = null;
            return entity;
        }

        public static WoinPurchase Builder(this WoinPurchase entity)
        {
            if (entity == null) return entity;
            WoinPurchase toObject = entity.ToObject();
            toObject.Purchaser = entity.Purchaser.ToObject();
            toObject.Transaction = entity.Transaction.ToObject();
            toObject.Vendor = entity.Vendor.ToObject();
            toObject.WoinFaceToFacePurchases = entity.WoinFaceToFacePurchases.ToObject();
            toObject.WoinOnlinePurchases = entity.WoinOnlinePurchases.ToObject();
            return toObject;
        }
    }

}
