﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinLocation : BaseEntity<WoinLocation>
    {
        public WoinLocation()
        {
            WoinCompanies = new HashSet<WoinCompany>();
            WoinOffices = new HashSet<WoinOffice>();
            WoinSessions = new HashSet<WoinSession>();
            WoinVisitors = new HashSet<WoinVisitor>();
        }

        public int Id { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal? Altitude { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual ICollection<WoinCompany> WoinCompanies { get; set; }
        public virtual ICollection<WoinOffice> WoinOffices { get; set; }
        public virtual ICollection<WoinSession> WoinSessions { get; set; }
        public virtual ICollection<WoinVisitor> WoinVisitors { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinLocation ToObject(this WoinLocation entity)
        {
            if (entity == null) return entity;
            entity.WoinCompanies = null; ;
            entity.WoinOffices = null;
            entity.WoinSessions = null;
            entity.WoinVisitors = null;
            return entity;
        }

        public static WoinLocation Builder(this WoinLocation entity)
        {
            if (entity == null) return entity;
            WoinLocation toObject = entity.ToObject();


            entity.WoinCompanies.ToList().ForEach(x =>
            {
                toObject.WoinCompanies.Add(x.ToObject());
            });

            entity.WoinOffices.ToList().ForEach(x =>
            {
                toObject.WoinOffices.Add(x.ToObject());
            });

            entity.WoinSessions.ToList().ForEach(x =>
            {
                toObject.WoinSessions.Add(x.ToObject());
            });

            entity.WoinVisitors.ToList().ForEach(x =>
            {
                toObject.WoinVisitors.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
