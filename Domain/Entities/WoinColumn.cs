﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinColumn : BaseEntity<WoinColumn>
    {
        public WoinColumn()
        {
            WoinColumnsSubcategories = new HashSet<WoinColumnSubcategory>();
            WoinColumnsValues = new HashSet<WoinColumnValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual ICollection<WoinColumnSubcategory> WoinColumnsSubcategories { get; set; }
        public virtual ICollection<WoinColumnValue> WoinColumnsValues { get; set; }
    }


    public static partial class BuilderEntity
    {
       
        public static WoinColumn ToObject(this WoinColumn entity)
        {
            if (entity == null) return entity;
            entity.WoinColumnsSubcategories = null;
            entity.WoinColumnsValues = null;
            return entity;
        }

        public static WoinColumn Builder(this WoinColumn entity)
        {
            if (entity == null) return entity;
            WoinColumn toObject = entity.ToObject();

            entity.WoinColumnsSubcategories.ToList().ForEach(x =>
            {
                toObject.WoinColumnsSubcategories.Add(x.ToObject());
            });

            entity.WoinColumnsValues.ToList().ForEach(x =>
            {
                toObject.WoinColumnsValues.Add(x.ToObject());
            });


            return toObject;
        }
    }

}
