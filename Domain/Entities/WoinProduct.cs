﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinProduct : BaseEntity<WoinProduct>
    {
        public WoinProduct()
        {
            WoinAdsProducts = new HashSet<WoinAdProduct>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SubcategoryId { get; set; }
        public decimal Price { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinSubcategory Subcategory { get; set; }
        public virtual ICollection<WoinAdProduct> WoinAdsProducts { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinProduct ToObject(this WoinProduct entity)
        {
            if (entity == null) return entity;
            entity.Subcategory = null;
            return entity;
        }

        public static WoinProduct Builder(this WoinProduct entity)
        {
            if (entity == null) return entity;
            WoinProduct toObject = entity.ToObject();
            toObject.Subcategory = entity.Subcategory.ToObject();

            entity.WoinAdsProducts.ToList().ForEach(x =>
            {
                toObject.WoinAdsProducts.Add(x.ToObject());
            });
            return toObject;
        }
    }

}
