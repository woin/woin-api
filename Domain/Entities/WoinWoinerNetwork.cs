﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinWoinerNetwork : BaseEntity<WoinWoinerNetwork>
    {
        public int Id { get; set; }
        public short Level { get; set; }
        public short Type { get; set; }
        public short GiftPercentage { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinWoinerNetwork ToObject(this WoinWoinerNetwork entity)
        {
           
            return entity;
        }

        public static WoinWoinerNetwork Builder(this WoinWoinerNetwork entity)
        {
            if (entity == null) return entity;
            WoinWoinerNetwork toObject = entity.ToObject();
           
            return toObject;
        }
    }

}
