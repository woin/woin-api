﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinAgreement : BaseEntity<WoinAgreement>
    {
        public WoinAgreement()
        {
            WoinRecharges = new HashSet<WoinRecharge>();
        }

        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual ICollection<WoinRecharge> WoinRecharges { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinAgreement ToObject(this WoinAgreement entity)
        {
            if (entity == null) return entity;
            return new WoinAgreement
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                Name = entity.Name,
                Number = entity.Number,
            };
        }


        public static WoinAgreement Builder(this WoinAgreement entity)
        {
            if (entity == null) return entity;
            WoinAgreement woinAgreement = entity.ToObject();

            woinAgreement.WoinRecharges.ToList().ForEach(x =>
            {
                woinAgreement.WoinRecharges.Add(x.ToObject());
            });

            return woinAgreement;

        }
    }

 }
