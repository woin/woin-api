﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinWoinWoiner : BaseEntity<WoinWoinWoiner>
    {
        public int Id { get; set; }
        public int WoinerId { get; set; }
        public int CountryId { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual Country Country { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinWoinWoiner ToObject(this WoinWoinWoiner entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            entity.Country = null;
            return entity;
        }

        public static WoinWoinWoiner Builder(this WoinWoinWoiner entity)
        {
            if (entity == null) return entity;
            WoinWoinWoiner toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            toObject.Country = entity.Country.ToObject();
            return toObject;
        }
    }
}
