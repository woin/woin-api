﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinPermission : BaseEntity<WoinPermission>
    {
        public WoinPermission()
        {
            WoinRolesPermissions = new HashSet<WoinRolePermission>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public short Level { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual ICollection<WoinRolePermission> WoinRolesPermissions { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinPermission ToObject(this WoinPermission entity)
        {
            if (entity == null) return entity;
            entity.WoinRolesPermissions = null;
            return entity;
        }

        public static WoinPermission Builder(this WoinPermission entity)
        {
            if (entity == null) return entity;
            WoinPermission toObject = entity.ToObject();

            entity.WoinRolesPermissions.ToList().ForEach(x =>
            {
                toObject.WoinRolesPermissions.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
