﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinColumnSubcategory : BaseEntity<WoinColumnSubcategory>
    {
        public int Id { get; set; }
        public int ColumnId { get; set; }
        public int SubcategoryId { get; set; }
        public short Type { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinColumn Column { get; set; }
        public virtual WoinSubcategory Subcategory { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinColumnSubcategory ToObject(this WoinColumnSubcategory entity)
        {
            if (entity == null) return entity;
            entity.Column = null;
            entity.Subcategory = null;
            return entity;
        }

        public static WoinColumnSubcategory Builder(this WoinColumnSubcategory entity)
        {
            if (entity == null) return entity;
            WoinColumnSubcategory toObject = entity.ToObject();
            toObject.Subcategory = entity.Subcategory.ToObject();
            toObject.Column = entity.Column.ToObject();
            return toObject;
        }
    }

}
