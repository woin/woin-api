﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinAdView : BaseEntity<WoinAdView>
    {
        public int Id { get; set; }
        public int WoinerId { get; set; }
        public int AdId { get; set; }
        public short Preferred { get; set; }
        public short Like { get; set; }
        public short Shared { get; set; }
        public int CreatedAt { get; set; }
        public int UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinAdView ToObject(this WoinAdView entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Woiner = null;
            return entity;
        }


        public static WoinAdView Builder(this WoinAdView entity)
        {
            if (entity == null) return entity;
            WoinAdView woinAdView = new WoinAdView
            {
                AdId = entity.AdId,
                WoinerId = entity.WoinerId,
                UpdatedAt = entity.UpdatedAt,
                Shared = entity.Shared,
                Preferred = entity.Preferred,
                Like = entity.Like,
                Id = entity.Id,
                CreatedAt = entity.CreatedAt,
                Ad = new WoinAd
                {
                    UpdatedAt = entity.Ad.UpdatedAt,
                    AdParent = entity.Ad.AdParent,
                    CreatedAt = entity.Ad.CreatedAt,
                    CurrentStock = entity.Ad.CurrentStock,
                    Description = entity.Ad.Description,
                    DiscountPercentage = entity.Ad.DiscountPercentage,
                    FinalTime = entity.Ad.FinalTime,
                    Id = entity.Ad.Id,
                    GiftPercentage = entity.Ad.GiftPercentage,
                    InitialStock = entity.Ad.InitialStock,
                    InitialTime = entity.Ad.InitialTime,
                    Price = entity.Ad.Price,
                    ProductId = entity.Ad.ProductId,
                    State = entity.Ad.State,
                    SubcategoryId = entity.Ad.SubcategoryId,
                    Title = entity.Ad.Title,
                    Type = entity.Ad.Type,
                    WoinerId = entity.Ad.WoinerId,
                },
                Woiner = new WoinWoiner
                {
                    CreatedAt = entity.Woiner.CreatedAt,
                    Id = entity.Woiner.Id,
                    Biography = entity.Woiner.Biography,
                    Codewoiner = entity.Woiner.Codewoiner,
                    PersonId = entity.Woiner.PersonId,
                    ProfileId = entity.Woiner.ProfileId,
                    Reference = entity.Woiner.Reference,
                    RoleId = entity.Woiner.RoleId,
                    State = entity.Woiner.State,
                    UpdatedAt = entity.Woiner.UpdatedAt,
                    UserId = entity.Woiner.UserId,
                }
            };
            return woinAdView;
        }
    }
 }
