﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinCompanyPhone : BaseEntity<WoinCompanyPhone>
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int PhoneId { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinCompany Company { get; set; }
        public virtual Phone Phone { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinCompanyPhone ToObject(this WoinCompanyPhone entity)
        {
            entity.Company = null;
            entity.Phone = null;
            return entity;
        }

        public static WoinCompanyPhone Builder(this WoinCompanyPhone entity)
        {
            WoinCompanyPhone toObject = entity.ToObject();
            toObject.Company = entity.Company.ToObject();
            toObject.Phone = entity.Phone.ToObject();
            return toObject;
        }
    }

}
