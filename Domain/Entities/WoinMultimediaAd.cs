﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinMultimediaAd : BaseEntity<WoinMultimediaAd>
    {
        public int Id { get; set; }
        public int MultimediaId { get; set; }
        public int AdId { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinMultimedia Multimedia { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinMultimediaAd ToObject(this WoinMultimediaAd entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Multimedia = null;
            return entity;
        }

        public static WoinMultimediaAd Builder(this WoinMultimediaAd entity)
        {
            if (entity == null) return entity;
            WoinMultimediaAd toObject = entity.ToObject();
            toObject.Ad = entity.Ad.ToObject();
            toObject.Multimedia = entity.Multimedia.ToObject();
            return toObject;
        }
    }

}
