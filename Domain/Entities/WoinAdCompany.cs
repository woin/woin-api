﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinAdCompany : BaseEntity<WoinAdCompany>
    {
        public int Id { get; set; }
        public int AdId { get; set; }
        public int CompanyId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinCompany Company { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinAdCompany ToObject(this WoinAdCompany entity)
        {
            if (entity == null) return entity;
            return new WoinAdCompany
            {
                AdId = entity.AdId,
                CompanyId = entity.CompanyId,
                Id = entity.Id,
                UpdatedAt = entity.UpdatedAt,
                CreatedAt = entity.CreatedAt
            };
        }

        public static WoinAdCompany Builder(this WoinAdCompany entity)
        {
            if (entity == null) return entity;
            return new WoinAdCompany
            {
                AdId = entity.AdId,
                CompanyId = entity.CompanyId,
                Id = entity.Id,
                UpdatedAt = entity.UpdatedAt,
                CreatedAt = entity.CreatedAt,
                Ad = new WoinAd
                {
                    CreatedAt = entity.Ad.CreatedAt,
                    UpdatedAt = entity.Ad.UpdatedAt,
                    Id = entity.Ad.Id,
                    AdParent = entity.Ad.AdParent,
                    CurrentStock = entity.Ad.CurrentStock,
                    Description = entity.Ad.Description,
                    DiscountPercentage = entity.Ad.DiscountPercentage,
                    FinalTime = entity.Ad.FinalTime,
                    GiftPercentage = entity.Ad.GiftPercentage,
                    InitialStock = entity.Ad.InitialStock,
                    InitialTime = entity.Ad.InitialTime,
                    Price = entity.Ad.Price,
                    ProductId = entity.Ad.ProductId,
                    State = entity.Ad.State,
                    SubcategoryId = entity.Ad.SubcategoryId,
                    Title = entity.Ad.Title,
                    Type = entity.Ad.Type,
                    WoinerId = entity.Ad.WoinerId,
                },
                Company = new WoinCompany
                {
                    Type = entity.Company.Type,
                    State = entity.Company.State,
                    CreatedAt = entity.Company.CreatedAt,
                    EmwoinerId = entity.Company.EmwoinerId,
                    Id = entity.Company.Id,
                    LocationId = entity.Company.LocationId,
                    Name = entity.Company.Name,
                    UpdatedAt = entity.Company.UpdatedAt,
                },
            };

        }

    }

 }
