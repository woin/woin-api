﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinWoinerPreference : BaseEntity<WoinWoinerPreference>
    {
        public int Id { get; set; }
        public int WoinerId { get; set; }
        public int SubcategoryId { get; set; }
        public short Priority { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinSubcategory Subcategory { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinWoinerPreference ToObject(this WoinWoinerPreference entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            entity.Subcategory = null;
            return entity;
        }

        public static WoinWoinerPreference Builder(this WoinWoinerPreference entity)
        {
            if (entity == null) return entity;
            WoinWoinerPreference toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            toObject.Subcategory = entity.Subcategory.ToObject();
            return toObject;
        }
    }

}
