﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class PrioritizedAdView : BaseEntity<PrioritizedAdView>
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long? InitialTime { get; set; }
        public long? FinalTime { get; set; }
        public decimal? Price { get; set; }
        public short? GiftPercentage { get; set; }
        public short? DiscountPercentage { get; set; }
        public int? InitialStock { get; set; }
        public int? CurrentStock { get; set; }
        public short? State { get; set; }
        public int? AdParent { get; set; }
        public int? WoinerId { get; set; }
        public int? SubcategoryId { get; set; }
        public long? CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }
        public short? Type { get; set; }
        public decimal? Index { get; set; }
    }

    public static partial class BuilderEntity
    {

        public static PrioritizedAdView ToObject(this PrioritizedAdView entity)
        {
            return entity;
        }
        public static PrioritizedAdView Builder(this PrioritizedAdView entity)
        {
            return entity;
        }
    }

}
