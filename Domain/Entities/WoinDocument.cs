﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinDocument : BaseEntity<WoinDocument>

    {
        public int Id { get; set; }
        public string Number { get; set; }
        public short State { get; set; }
        public short Type { get; set; }
        public int CityId { get; set; }
        public int PersonId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual City City { get; set; }
        public virtual WoinPerson Person { get; set; }
    }

    public static partial class BuilderEntity
    {
        
        public static WoinDocument ToObject(this WoinDocument entity)
        {
            if (entity == null) return entity;
            entity.Person = null;
            entity.City = null;
            return entity;
        }

        public static WoinDocument Builder(this WoinDocument entity)
        {
            if (entity == null) return entity;
            WoinDocument toObject = entity.ToObject();
            toObject.Person = entity.Person.ToObject();
            toObject.City = entity.City.ToObject();
            return toObject;
        }
    }

}
