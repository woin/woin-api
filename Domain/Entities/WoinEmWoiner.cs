﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinEmWoiner : BaseEntity<WoinEmWoiner>
    {
        public WoinEmWoiner()
        {
            WoinCompanies = new HashSet<WoinCompany>();
        }

        public int Id { get; set; }
        public int WoinerId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinWoiner Woiner { get; set; }
        public virtual ICollection<WoinCompany> WoinCompanies { get; set; }
    }

    public static partial class BuilderEntity
    {

        public static WoinEmWoiner ToObject(this WoinEmWoiner entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            return entity;
        }

        public static WoinEmWoiner Builder(this WoinEmWoiner entity)
        {
            if (entity == null) return entity;
            WoinEmWoiner toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();


            entity.WoinCompanies.ToList().ForEach(x =>
            {
                toObject.WoinCompanies.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
