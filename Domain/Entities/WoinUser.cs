﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinUser : BaseEntity<WoinUser>
    {
        public WoinUser()
        {
            Devices = new HashSet<Device>();
            WoinGifts = new HashSet<WoinGift>();
            WoinSessions = new HashSet<WoinSession>();
            WoinUserGroups = new HashSet<WoinUserGroup>();
            WoinUsersRoles = new HashSet<WoinUserRole>();
            WoinWoiners = new HashSet<WoinWoiner>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RememberPassword { get; set; }
        public short State { get; set; }
        public int RoleId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinPerson WoinPersons { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<WoinGift> WoinGifts { get; set; }
        public virtual ICollection<WoinSession> WoinSessions { get; set; }
        public virtual ICollection<WoinUserGroup> WoinUserGroups { get; set; }
        public virtual ICollection<WoinUserRole> WoinUsersRoles { get; set; }
        public virtual ICollection<WoinWoiner> WoinWoiners { get; set; }
    }

    public  static partial class BuilderEntity
    {

        public static WoinUser ToObject(this WoinUser entity)
        {
            if (entity == null) return entity;
            entity.WoinPersons = null;
            entity.Devices = null;
            entity.WoinGifts = null;
            entity.WoinSessions = null;
            entity.WoinUserGroups = null;
            entity.WoinUsersRoles = null;
            entity.WoinWoiners = null;
            return entity;
        }

        public static WoinUser Builder(this WoinUser woinUser)
        {
            if (woinUser == null) return woinUser;
            var user =  new WoinUser() {
                Id = woinUser.Id,
                UpdatedAt = woinUser.UpdatedAt,
                CreatedAt = woinUser.CreatedAt,
                Email = woinUser.Email,
                Password = woinUser.Password,
                RememberPassword = woinUser.RememberPassword,
                RoleId = woinUser.RoleId,
                State = woinUser.State,
                Username = woinUser.Username,
                WoinPersons = new WoinPerson
                {
                    Birthdate = woinUser.WoinPersons.Birthdate,
                    CreatedAt = woinUser.WoinPersons.CreatedAt,
                    FirstLastname = woinUser.WoinPersons.FirstLastname,
                    Firstname = woinUser.WoinPersons.Firstname,
                    Gender = woinUser.WoinPersons.Gender,
                    Id = woinUser.WoinPersons.Id,
                    SecondLastname = woinUser.WoinPersons.SecondLastname,
                    Secondname = woinUser.WoinPersons.Secondname,
                    UpdatedAt = woinUser.WoinPersons.UpdatedAt,
                    UserId = woinUser.Id
                }  
            };

            woinUser.WoinSessions.ToList().ForEach(x => {
                user.WoinSessions.Add(new WoinSession
                {
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    DeviceId = x.DeviceId,
                    LocationId = x.LocationId,
                    State = x.State,
                    UserId = x.UserId,
                });
            });


            woinUser.WoinUserGroups.ToList().ForEach(x =>
            {
                user.WoinUserGroups.Add(new WoinUserGroup
                {
                    CreatedAt = x.CreatedAt,
                    GroupId = x.GroupId,
                    Id = x.Id,
                    UpdatedAt = x.UpdatedAt,
                    UserId = x.UserId
                });
            });

            woinUser.Devices.ToList().ForEach(x =>
            {
                user.Devices.Add(new Device
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    CreatedAt = x.CreatedAt,
                    Ip = x.Ip, 
                    Mac = x.Mac,
                    Name = x.Name,
                    State = x.State,
                    UpdatedAt = x.UpdatedAt,
                    
                });
            });

            woinUser.WoinGifts.ToList().ForEach(x =>
            {
                user.WoinGifts.Add(new WoinGift
                {
                    UpdatedAt = x.UpdatedAt,
                     Amount = x.Amount,
                     CreatedAt = x.CreatedAt,
                     Id = x.Id,
                     State = x.State,
                     UserId = x.UserId,
                });
            });

            woinUser.WoinUsersRoles.ToList().ForEach(x =>
            {
                user.WoinUsersRoles.Add(new WoinUserRole
                {
                    UserId = x.UserId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    RoleId = x.RoleId,
                    UpdatedAt = x.UpdatedAt
                });
            });


            woinUser.WoinWoiners.ToList().ForEach(x =>
           {
               user.WoinWoiners.Add(new WoinWoiner
               {
                   UpdatedAt = x.UpdatedAt,
                   Biography = x.Biography,
                   Codewoiner = x.Codewoiner,
                   CreatedAt = x.CreatedAt,
                   Id = x.Id,
                   PersonId = x.PersonId,
                   ProfileId = x.ProfileId,
                   RoleId = x.RoleId,
                   UserId = x.UserId,
                   State = x.State,
               });
           });
            return user;
        }
    }

 }
