﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Base;

namespace Domain.Entities
{
    public partial class Device : BaseEntity<Device>
    {
        public Device()
        {
            WoinSessions = new HashSet<WoinSession>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public string Mac { get; set; }
        public string Ip { get; set; }
        public int UserId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinUser User { get; set; }
        public virtual ICollection<WoinSession> WoinSessions { get; set; }


    }


    public static partial class BuilderEntity
    {

        public static Device ToObject(this Device entity)
        {
            if (entity == null) return entity;
            return new Device
            {
                Name = entity.Name,
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                State = entity.State,
                CreatedAt = entity.CreatedAt,
                Ip = entity.Ip,
                Mac = entity.Mac,
                UserId = entity.UserId,
            };
        }

        public static Device Builder(this Device entity)
        {
            if (entity == null) return entity;
            Device device = new Device
            {
                Name = entity.Name,
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                State = entity.State,
                CreatedAt = entity.CreatedAt,
                Ip = entity.Ip,
                Mac = entity.Mac,
                UserId = entity.UserId,
                User = new WoinUser
                {
                    CreatedAt = entity.User.CreatedAt,
                    State = entity.User.State,
                    Id = entity.User.Id,
                    UpdatedAt = entity.User.UpdatedAt,
                    Email = entity.User.Email,
                    Password = entity.User.Password,
                    RememberPassword = entity.User.RememberPassword,
                    RoleId = entity.User.RoleId,
                    Username = entity.User.Username,
                }
            };

            entity.WoinSessions.ToList().ForEach(x =>
            {
                device.WoinSessions.Add(new WoinSession { 
                    State = x.State,
                    CreatedAt = x.CreatedAt,
                    DeviceId = x.DeviceId,
                    Id = x.Id,
                    LocationId = x.LocationId,
                    UpdatedAt = x.UpdatedAt,
                    UserId = x.UserId,
                });
            });

            return device;
        }
    }
}
