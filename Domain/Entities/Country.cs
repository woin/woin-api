﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class Country : BaseEntity<Country>
    {
        public Country()
        {
            Governorates = new HashSet<Governorate>();
            Phones = new HashSet<Phone>();
            WoinWallets = new HashSet<WoinWallet>();
            WoinWoinWoiners = new HashSet<WoinWoinWoiner>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual ICollection<Governorate> Governorates { get; set; }
        public virtual ICollection<Phone> Phones { get; set; }
        public virtual ICollection<WoinWallet> WoinWallets { get; set; }
        public virtual ICollection<WoinWoinWoiner> WoinWoinWoiners { get; set; }
    }


    public static partial class BuilderEntity
    {

        public static Country ToObject(this Country entity)
        {
            if (entity == null) return entity;
            return new Country
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                Name = entity.Name,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
            };
        }

        public static Country Builder(this Country entity)
        {
            if (entity == null) return entity;
            Country country = new Country
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                Name = entity.Name,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
            };

            entity.Governorates.ToList().ForEach(x =>
            {
                country.Governorates.Add(new Governorate { 
                    CountryId = x.CountryId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Name = x.Name,
                    State = x.State,
                    UpdatedAt = x.UpdatedAt
                });
            });

            entity.Phones.ToList().ForEach(x =>
            {
                country.Phones.Add(new Phone
                {
                    CountryId = x.CountryId,
                    UpdatedAt = x.UpdatedAt,
                    State = x.State,
                    Id = x.Id,
                    Number = x.Number,
                });
            });

            entity.WoinWallets.ToList().ForEach(x =>
            {
                country.WoinWallets.Add(new WoinWallet
                {
                    CountryId = x.CountryId,
                    Number = x.Number,
                    Id = x.Id,
                    Balance = x.Balance,
                    CreatedAt = x.CreatedAt,
                    State = x.State,
                    Type = x.Type,
                    UpdatedAt = x.UpdatedAt,
                    WoinerId = x.WoinerId,
                    
                });

            });

            entity.WoinWoinWoiners.ToList().ForEach(x =>
            {
                country.WoinWoinWoiners.Add(new WoinWoinWoiner
                {
                    CountryId = x.CountryId,
                    WoinerId = x.WoinerId,
                    UpdatedAt = x.UpdatedAt,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    State = x.State
                });
            });

            return country;
        }
     }
 }
