﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinCategory : BaseEntity<WoinCategory>
    {
        public WoinCategory()
        {
            WoinMultimediasCategories = new HashSet<WoinMultimediaCategory>();
            WoinSubcategories = new HashSet<WoinSubcategory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual ICollection<WoinMultimediaCategory> WoinMultimediasCategories { get; set; }
        public virtual ICollection<WoinSubcategory> WoinSubcategories { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinCategory ToObject(this WoinCategory entity)
        {
            if (entity == null) return entity;
            entity.WoinMultimediasCategories = null;
            entity.WoinSubcategories = null;
            return entity;
        }

        public static WoinCategory Builder(this WoinCategory entity)
        {
            if (entity == null) return entity;
            WoinCategory woinCategory = entity.ToObject();


            entity.WoinMultimediasCategories.ToList().ForEach(x =>
            {
                woinCategory.WoinMultimediasCategories.Add(x.ToObject());
            });

            entity.WoinSubcategories.ToList().ForEach(x =>
            {
                woinCategory.WoinSubcategories.Add(x.ToObject());
            });


            return woinCategory;
        }
    }

}
