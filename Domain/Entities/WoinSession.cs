﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinSession : BaseEntity<WoinSession>
    {
        public WoinSession()
        {
            WoinTransactions = new HashSet<WoinTransaction>();
        }

        public int Id { get; set; }
        public short State { get; set; }
        public int UserId { get; set; }
        public int? DeviceId { get; set; }
        public int LocationId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual Device Device { get; set; }
        public virtual WoinLocation Location { get; set; }
        public virtual WoinUser User { get; set; }
        public virtual ICollection<WoinTransaction> WoinTransactions { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinSession ToObject(this WoinSession entity)
        {
            if (entity == null) return entity;
            return new WoinSession
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                DeviceId = entity.DeviceId,
                LocationId = entity.LocationId,
                UserId =entity.UserId,

            };
        }


        public static WoinSession Builder(this WoinSession entity)
        {
            if (entity == null) return entity;
            WoinSession session = entity.ToObject();
            session.Device = entity.Device.ToObject();
            session.Location = entity.Location.ToObject();
            session.User = entity.User.ToObject();


            entity.WoinTransactions.ToList().ForEach(x =>
            {
                session.WoinTransactions.Add(x.ToObject());
            });

            return session;
        }
    }

}
