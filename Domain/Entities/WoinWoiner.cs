﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinWoiner : BaseEntity<WoinWoiner>
    {
        public WoinWoiner()
        {
            InverseReferenceNavigation = new HashSet<WoinWoiner>();
            WoinAds = new HashSet<WoinAd>();
            WoinAdsViews = new HashSet<WoinAdView>();
            WoinCalificationsWoinerReceiver = new HashSet<WoinCalification>();
            WoinCalificationsWoinerSender = new HashSet<WoinCalification>();
            WoinCliwoiners = new HashSet<WoinCliwoiner>();
            WoinDynamicsKeys = new HashSet<WoinDynamicKey>();
            WoinEmailsGifts = new HashSet<WoinEmailGift>();
            WoinEmwoiners = new HashSet<WoinEmWoiner>();
            WoinFaceToFacePurchases = new HashSet<WoinFaceToFacePurchase>();
            WoinPayForViewViews = new HashSet<WoinPayForViewView>();
            WoinPurchasesPurchaser = new HashSet<WoinPurchase>();
            WoinPurchasesVendor = new HashSet<WoinPurchase>();
            WoinSocialProfiles = new HashSet<WoinSocialProfile>();
            WoinWallets = new HashSet<WoinWallet>();
            WoinWoinWoiners = new HashSet<WoinWoinWoiner>();
            WoinWoinersConfigs = new HashSet<WoinWoinerConfig>();
            WoinWoinersPreferences = new HashSet<WoinWoinerPreference>();
        }

        public int Id { get; set; }
        public string Codewoiner { get; set; }
        public short State { get; set; }
        public string Biography { get; set; }
        public int PersonId { get; set; }
        public int UserId { get; set; }
        public int ProfileId { get; set; }
        public int RoleId { get; set; }
        public int? Reference { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinPerson Person { get; set; }
        public virtual WoinMultimedia Profile { get; set; }
        public virtual WoinWoiner ReferenceNavigation { get; set; }
        public virtual WoinRole Role { get; set; }
        public virtual WoinUser User { get; set; }
        public virtual ICollection<WoinWoiner> InverseReferenceNavigation { get; set; }
        public virtual ICollection<WoinAd> WoinAds { get; set; }
        public virtual ICollection<WoinAdView> WoinAdsViews { get; set; }
        public virtual ICollection<WoinCalification> WoinCalificationsWoinerReceiver { get; set; }
        public virtual ICollection<WoinCalification> WoinCalificationsWoinerSender { get; set; }
        public virtual ICollection<WoinCliwoiner> WoinCliwoiners { get; set; }
        public virtual ICollection<WoinDynamicKey> WoinDynamicsKeys { get; set; }
        public virtual ICollection<WoinEmailGift> WoinEmailsGifts { get; set; }
        public virtual ICollection<WoinEmWoiner> WoinEmwoiners { get; set; }
        public virtual ICollection<WoinFaceToFacePurchase> WoinFaceToFacePurchases { get; set; }
        public virtual ICollection<WoinPayForViewView> WoinPayForViewViews { get; set; }
        public virtual ICollection<WoinPurchase> WoinPurchasesPurchaser { get; set; }
        public virtual ICollection<WoinPurchase> WoinPurchasesVendor { get; set; }
        public virtual ICollection<WoinSocialProfile> WoinSocialProfiles { get; set; }
        public virtual ICollection<WoinWallet> WoinWallets { get; set; }
        public virtual ICollection<WoinWoinWoiner> WoinWoinWoiners { get; set; }
        public virtual ICollection<WoinWoinerConfig> WoinWoinersConfigs { get; set; }
        public virtual ICollection<WoinWoinerPreference> WoinWoinersPreferences { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinWoiner ToObject(this WoinWoiner entity)
        {
            if (entity == null) return entity;
            return new WoinWoiner
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                Biography = entity.Biography,
                Codewoiner = entity.Codewoiner,
                PersonId = entity.PersonId,
                ProfileId = entity.ProfileId,
                Reference = entity.Reference,
                RoleId = entity.RoleId,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                UserId = entity.UserId,
            };
        }


        public static WoinWoiner Builder(this WoinWoiner entity)
        {
            if (entity == null) return entity;
            WoinWoiner woinWoiner = new WoinWoiner
            {
                Person = new WoinPerson
                {
                    Birthdate = entity.Person.Birthdate,
                    CreatedAt = entity.Person.CreatedAt,
                    FirstLastname = entity.Person.FirstLastname,
                    Firstname = entity.Person.Firstname,
                    Gender = entity.Person.Gender,
                    Id = entity.Person.Id,
                    SecondLastname = entity.Person.SecondLastname,
                    Secondname = entity.Person.Secondname,
                    UpdatedAt = entity.Person.UpdatedAt,
                    UserId = entity.Id
                },
                User = new WoinUser
                {
                    Id = entity.User.Id,
                    UpdatedAt = entity.User.UpdatedAt,
                    CreatedAt = entity.User.CreatedAt,
                    Email = entity.User.Email,
                    Password = entity.User.Password,
                    RememberPassword = entity.User.RememberPassword,
                    RoleId = entity.User.RoleId,
                    State = entity.User.State,
                    Username = entity.User.Username,
                },
                ReferenceNavigation = new WoinWoiner
                {
                    CreatedAt = entity.ReferenceNavigation.CreatedAt,
                    Id = entity.ReferenceNavigation.Id,
                    Biography = entity.ReferenceNavigation.Biography,
                    Codewoiner = entity.ReferenceNavigation.Codewoiner,
                    PersonId = entity.ReferenceNavigation.PersonId,
                    ProfileId = entity.ReferenceNavigation.ProfileId,
                    Reference = entity.ReferenceNavigation.Reference,
                    RoleId = entity.ReferenceNavigation.RoleId,
                    State = entity.ReferenceNavigation.State,
                    UpdatedAt = entity.ReferenceNavigation.UpdatedAt,
                    UserId = entity.ReferenceNavigation.UserId,
                },
                Profile = new WoinMultimedia
                {
                    CreatedAt = entity.Profile.CreatedAt,
                    Description = entity.Profile.Description,
                    Id = entity.Profile.Id,
                    Source = entity.Profile.Source,
                    State = entity.Profile.State,
                    Type = entity.Profile.Type,
                    UpdatedAt = entity.Profile.UpdatedAt,
                },
                Role = new WoinRole
                {
                    UpdatedAt = entity.Role.UpdatedAt,
                    State = entity.Role.State,
                    Id = entity.Role.Id,
                    Description = entity.Role.Description,
                    CreatedAt = entity.Role.CreatedAt,
                    Level = entity.Role.Level,
                    Name = entity.Role.Name,
                }
            };

            entity.InverseReferenceNavigation.ToList().ForEach(x =>
            {
                woinWoiner.InverseReferenceNavigation.Add(new WoinWoiner
                {
                    Biography = x.Biography,
                    Id = x.Id,
                    Codewoiner = x.Codewoiner,
                    CreatedAt = x.CreatedAt,
                    PersonId = x.PersonId,
                    ProfileId = x.ProfileId,
                    Reference = x.Reference,
                    RoleId = x.RoleId,
                    UpdatedAt = x.UpdatedAt,
                    UserId = x.UserId,
                });
            });

            entity.WoinAds.ToList().ForEach(x =>
            {
                woinWoiner.WoinAds.Add(new WoinAd
                {
                    UpdatedAt = x.UpdatedAt,
                    AdParent = x.AdParent,
                    CreatedAt = x.CreatedAt,
                    CurrentStock = x.CurrentStock,
                    Description = x.Description,
                    DiscountPercentage = x.DiscountPercentage,
                    FinalTime = x.FinalTime,
                    Id = x.Id,
                    GiftPercentage = x.GiftPercentage,
                    InitialStock = x.InitialStock,
                    InitialTime = x.InitialTime,
                    Price = x.Price,
                    ProductId = x.ProductId,
                    State = x.State,
                    SubcategoryId = x.SubcategoryId,
                    Title = x.Title,
                    Type = x.Type,
                    WoinerId = x.WoinerId,
                });
            });

            entity.WoinAdsViews.ToList().ForEach(x =>
            {
                woinWoiner.WoinAdsViews.Add(new WoinAdView
                {
                    AdId = x.AdId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Like = x.Like,
                    Preferred = x.Preferred,
                    Shared = x.Shared,
                    UpdatedAt= x.UpdatedAt,
                    WoinerId = x.WoinerId
                });
            });

            entity.WoinCalificationsWoinerReceiver.ToList().ForEach(x =>
            {
                woinWoiner.WoinCalificationsWoinerReceiver.Add(new WoinCalification { 
                    UpdatedAt = x.UpdatedAt,
                    Comment = x.Comment,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Value = x.Value,
                    WoinerReceiverId = x.WoinerReceiverId,
                    WoinerSenderId = x.WoinerSenderId
                });

            });


            entity.WoinCalificationsWoinerSender.ToList().ForEach(x =>
            {
                woinWoiner.WoinCalificationsWoinerSender.Add(new WoinCalification
                {
                    WoinerSenderId = x.WoinerSenderId,
                    WoinerReceiverId = x.WoinerReceiverId,
                    Value = x.Value,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    Comment = x.Comment,
                    UpdatedAt = x.UpdatedAt
                });
            });

            entity.WoinCliwoiners.ToList().ForEach(x =>
            {
                woinWoiner.WoinCliwoiners.Add(new WoinCliwoiner
                {
                    UpdatedAt = x.UpdatedAt,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    WoinerId = x.WoinerId,
                });
            });


            entity.WoinDynamicsKeys.ToList().ForEach(x =>
            {
                woinWoiner.WoinDynamicsKeys.Add(new WoinDynamicKey
                {
                    WoinerId = x.WoinerId,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    DynamicKey = x.DynamicKey,
                    State = x.State,
                    TransactionId = x.TransactionId,
                    UpdatedAt = x.UpdatedAt
                });
            });

            entity.WoinEmailsGifts.ToList().ForEach(x =>
            {
                woinWoiner.WoinEmailsGifts.Add(new WoinEmailGift
                {
                    UpdatedAt = x.UpdatedAt,
                    TransactionId = x.TransactionId,
                    State = x.State,
                    Amount = x.Amount,
                    CreatedAt = x.CreatedAt,
                    Email = x.Email,
                    Id = x.Id,
                    WoinerId = x.WoinerId
                });
            });

            entity.WoinEmwoiners.ToList().ForEach(x =>
            {
                woinWoiner.WoinEmwoiners.Add(new WoinEmWoiner
                {
                    WoinerId = x.WoinerId,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt,
                });
            });


            entity.WoinFaceToFacePurchases.ToList().ForEach(x =>
            {
                woinWoiner.WoinFaceToFacePurchases.Add(new WoinFaceToFacePurchase
                {
                    UpdatedAt = x.UpdatedAt,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Cash = x.Cash,
                    PurchaseId = x.PurchaseId,
                    VendorId = x.VendorId,
                });
            });

            entity.WoinPayForViewViews.ToList().ForEach(x =>
            {
                woinWoiner.WoinPayForViewViews.Add(new WoinPayForViewView
                {
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    PayForViewId = x.PayForViewId,
                    UpdatedAt = x.UpdatedAt,
                    WoinerId = x.WoinerId,
                    
                });
            });

            entity.WoinPurchasesPurchaser.ToList().ForEach(x =>
            {
                woinWoiner.WoinPurchasesPurchaser.Add(new WoinPurchase
                {
                    UpdatedAt = x.UpdatedAt,
                    CreatedAt = x.CreatedAt,
                    Detail = x.Detail,
                    Gift = x.Gift,
                    Id = x.Id,
                    Points = x.Points,
                    PurchaserId = x.PurchaserId,
                    State = x.State,
                    TransactionId = x.TransactionId,
                    VendorId = x.VendorId,
                });
            });

            entity.WoinPurchasesVendor.ToList().ForEach(x =>
            {
                woinWoiner.WoinPurchasesVendor.Add(new WoinPurchase
                {
                    VendorId = x.VendorId,
                    TransactionId = x.TransactionId,
                    State = x.State,
                    CreatedAt = x.CreatedAt,
                    Detail = x.Detail,
                    Gift = x.Gift,
                    Id = x.Id,
                    Points = x.Points,
                    PurchaserId = x.PurchaserId,
                    UpdatedAt = x.UpdatedAt,
                    
                });
            });

            entity.WoinSocialProfiles.ToList().ForEach(x =>
            {
                woinWoiner.WoinSocialProfiles.Add(new WoinSocialProfile
                {
                    UpdatedAt = x.UpdatedAt,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Profile = x.Profile,
                    SocialNetworkId = x.SocialNetworkId,
                    WoinerId = x.WoinerId,
                    State = x.State
                });
            });

            entity.WoinWallets.ToList().ForEach(x =>
            {
                woinWoiner.WoinWallets.Add(new WoinWallet
                {
                    State = x.State,
                    WoinerId = x.WoinerId, 
                    Balance = x.Balance,
                    CountryId = x.CountryId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Number = x.Number,
                    Type = x.Type,
                    UpdatedAt = x.UpdatedAt, 
                });
            });


            entity.WoinWoinWoiners.ToList().ForEach(x =>
            {
                woinWoiner.WoinWoinWoiners.Add(new WoinWoinWoiner
                {
                    UpdatedAt = x.UpdatedAt,
                    CountryId = x.CountryId,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    State = x.State,
                    WoinerId = x.WoinerId,
                });
            });


            entity.WoinWoinersConfigs.ToList().ForEach(x =>
            {
                woinWoiner.WoinWoinersConfigs.Add(new WoinWoinerConfig
                {
                    WoinerId = x.WoinerId,
                    State = x.State,
                    Id = x.Id,
                    CreatedAt = x.CreatedAt,
                    GiftForCash = x.GiftForCash,
                    MinimumGiftLimitAmount = x.MinimumGiftLimitAmount,
                    MinimumGiftPercentage = x.MinimumGiftPercentage,
                    UpdatedAt = x.UpdatedAt
                });
            });

            entity.WoinWoinersPreferences.ToList().ForEach(x =>
            {
                woinWoiner.WoinWoinersPreferences.Add(new WoinWoinerPreference
                {
                    UpdatedAt = x.UpdatedAt,
                    CreatedAt = x.CreatedAt,
                    Id = x.Id,
                    Priority = x.Priority,
                    State = x.State,
                    SubcategoryId = x.SubcategoryId,
                    WoinerId = x.WoinerId
                });
            });


            return woinWoiner;

        }
    }
}
