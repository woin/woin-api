﻿using System;
using System.Collections.Generic;
using System.Collections;
using Domain.Base;

namespace Domain.Entities
{
    public partial class WoinWoinerConfig : BaseEntity<WoinWoinerConfig>
    {
        public int Id { get; set; }
        public short State { get; set; }
        public int WoinerId { get; set; }
        public short MinimumGiftPercentage { get; set; }
        public BitArray GiftForCash { get; set; }
        public short MinimumGiftLimitAmount { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinWoiner Woiner { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinWoinerConfig ToObject(this WoinWoinerConfig entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            return entity;
        }

        public static WoinWoinerConfig Builder(this WoinWoinerConfig entity)
        {
            if (entity == null) return entity;
            WoinWoinerConfig toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            return toObject;
        }
    }

}
