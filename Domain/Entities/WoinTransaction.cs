﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinTransaction : BaseEntity<WoinTransaction>
    {
        public WoinTransaction()
        {
            WoinDynamicsKeys = new HashSet<WoinDynamicKey>();
            WoinEmailsGifts = new HashSet<WoinEmailGift>();
            WoinMovements = new HashSet<WoinMovement>();
            WoinPaidsAds = new HashSet<WoinPaidAd>();
            WoinPurchases = new HashSet<WoinPurchase>();
            WoinTransactionsTransactionsParent = new HashSet<WoinTransactionTransaction>();
        }

        public int Id { get; set; }
        public decimal Amount { get; set; }
        public short State { get; set; }
        public short Type { get; set; }
        public string Detail { get; set; }
        public int WalletSender { get; set; }
        public int WalletReceiver { get; set; }
        public int SessionId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinSession Session { get; set; }
        public virtual WoinWallet WalletReceiverNavigation { get; set; }
        public virtual WoinWallet WalletSenderNavigation { get; set; }
        public virtual WoinTransactionTransaction WoinTransactionsTransactionsChild { get; set; }
        public virtual ICollection<WoinDynamicKey> WoinDynamicsKeys { get; set; }
        public virtual ICollection<WoinEmailGift> WoinEmailsGifts { get; set; }
        public virtual ICollection<WoinMovement> WoinMovements { get; set; }
        public virtual ICollection<WoinPaidAd> WoinPaidsAds { get; set; }
        public virtual ICollection<WoinPurchase> WoinPurchases { get; set; }
        public virtual ICollection<WoinTransactionTransaction> WoinTransactionsTransactionsParent { get; set; }
    }


    public static partial class BuilderEntity
    {

        public static WoinTransaction ToObject(this WoinTransaction entity)
        {
            if (entity == null) return entity;
            return new WoinTransaction
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                Amount = entity.Amount,
                Detail = entity.Detail,
                SessionId = entity.SessionId,
                Type = entity.Type,
                WalletReceiver = entity.WalletReceiver,
                WalletSender = entity.WalletSender,
               
            };
        }


        public static WoinTransaction Builder(this WoinTransaction entity)
        {
            if (entity == null) return entity;
            WoinTransaction woinTransaction = entity.ToObject();
            woinTransaction.Session = entity.Session.ToObject();
            woinTransaction.WalletReceiverNavigation = entity.WalletReceiverNavigation.ToObject();
            woinTransaction.WoinTransactionsTransactionsChild = entity.WoinTransactionsTransactionsChild.ToObject();
            woinTransaction.WalletSenderNavigation = entity.WalletSenderNavigation.ToObject();


            entity.WoinDynamicsKeys.ToList().ForEach(x =>
            {
                woinTransaction.WoinDynamicsKeys.Add(x.ToObject());
            });

            entity.WoinEmailsGifts.ToList().ForEach(x =>
            {
                woinTransaction.WoinEmailsGifts.Add(x.ToObject());
            });
            entity.WoinMovements.ToList().ForEach(x =>
            {
                woinTransaction.WoinMovements.Add(x.ToObject());
            });
            entity.WoinPaidsAds.ToList().ForEach(x =>
            {
                woinTransaction.WoinPaidsAds.Add(x.ToObject());
            });
            entity.WoinPaidsAds.ToList().ForEach(x =>
            {
                woinTransaction.WoinPaidsAds.Add(x.ToObject());
            });
            entity.WoinTransactionsTransactionsParent.ToList().ForEach(x =>
            {
                woinTransaction.WoinTransactionsTransactionsParent.Add(x.ToObject());
            });


            return woinTransaction;
        }
    }


}
