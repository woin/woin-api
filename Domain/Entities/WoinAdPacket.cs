﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinAdPacket : BaseEntity<WoinAdPacket>
    {
        public WoinAdPacket()
        {
            WoinFreesAds = new HashSet<WoinFreeAd>();
            WoinPaidsAds = new HashSet<WoinPaidAd>();
        }

        public int Id { get; set; }
        public long Duration { get; set; }
        public decimal Price { get; set; }
        public long BannerTime { get; set; }
        public short Type { get; set; }
        public short Schedule { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual ICollection<WoinFreeAd> WoinFreesAds { get; set; }
        public virtual ICollection<WoinPaidAd> WoinPaidsAds { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinAdPacket ToObject(this WoinAdPacket entity)
        {
            if (entity == null) return entity;
            return new WoinAdPacket { 
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                BannerTime = entity.BannerTime,
                CreatedAt = entity.CreatedAt,
                Duration = entity.Duration,
                Price = entity.Price,
                Schedule = entity.Schedule,
                State = entity.State,
                Type = entity.Type,
               
            };
        }


        public static WoinAdPacket Builder(this WoinAdPacket entity)
        {
            if (entity == null) return entity;
            WoinAdPacket woinAdPacket = new WoinAdPacket
            {
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                BannerTime = entity.BannerTime,
                CreatedAt = entity.CreatedAt,
                Duration = entity.Duration,
                Price = entity.Price,
                Schedule = entity.Schedule,
                State = entity.State,
                Type = entity.Type,
            };

            entity.WoinFreesAds.ToList().ForEach(x =>
            {
                woinAdPacket.WoinFreesAds.Add(new WoinFreeAd { 
                    AdId = x.AdId,
                    CreatedAt = x.CreatedAt,
                    End = x.End,
                    Id = x.Id,
                    PacketId = x.PacketId,
                    Start = x.Start,
                    UpdatedAt = x.UpdatedAt
                });

            });

            entity.WoinPaidsAds.ToList().ForEach(x =>
            {
                woinAdPacket.WoinPaidsAds.Add(new WoinPaidAd { 
                    UpdatedAt = x.UpdatedAt,
                    Start = x.Start,
                    PacketId = x.PacketId,
                    Id = x.Id,
                    End = x.End,
                    CreatedAt = x.CreatedAt,
                    AdId = x.AdId,
                    TransactionId = x.TransactionId
                });
            });

            return woinAdPacket;
        }
    }

}
