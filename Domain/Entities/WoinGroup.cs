﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinGroup : BaseEntity<WoinGroup>
    {
        public WoinGroup()
        {
            WoinUserGroups = new HashSet<WoinUserGroup>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinRole Role { get; set; }
        public virtual ICollection<WoinUserGroup> WoinUserGroups { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinGroup ToObject(this WoinGroup entity)
        {
            if (entity == null) return entity;
            entity.Role = null;
            entity.WoinUserGroups = null;
            return entity;
        }

        public static WoinGroup Builder(this WoinGroup entity)
        {
            if (entity == null) return entity;
            WoinGroup toObject = entity.ToObject();
            toObject.Role = entity.Role.ToObject();

            entity.WoinUserGroups.ToList().ForEach(x =>
            {
                toObject.WoinUserGroups.Add(x.ToObject());
            });


            return toObject;
        }
    }

}
