﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinRecharge : BaseEntity<WoinRecharge>
    {
        public int Id { get; set; }
        public short State { get; set; }
        public int TransactionId { get; set; }
        public int AgreementId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinAgreement Agreement { get; set; }
        public virtual WoinMovement Transaction { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinRecharge ToObject(this WoinRecharge entity)
        {
            if (entity == null) return entity;
            return new WoinRecharge
            {
                CreatedAt = entity.CreatedAt,
                Id = entity.Id,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt,
                AgreementId = entity.AgreementId,
                TransactionId = entity.TransactionId,
                
            };
        }


        public static WoinRecharge Builder(this WoinRecharge entity)
        {
            if (entity == null) return entity;
            WoinRecharge woinRecharge = entity.ToObject();
            woinRecharge.Agreement = entity.Agreement.ToObject();
            woinRecharge.Transaction = entity.Transaction.ToObject();
            return woinRecharge;
        }
    }

}
