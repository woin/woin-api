﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinWallet : BaseEntity<WoinWallet>
    {
        public WoinWallet()
        {
            InverseWalletParentNavigation = new HashSet<WoinWallet>();
            WoinGiftsWalletReceiverNavigation = new HashSet<WoinGift>();
            WoinGiftsWalletSenderNavigation = new HashSet<WoinGift>();
            WoinMovements = new HashSet<WoinMovement>();
            WoinOffices = new HashSet<WoinOffice>();
            WoinTransactionsWalletReceiverNavigation = new HashSet<WoinTransaction>();
            WoinTransactionsWalletSenderNavigation = new HashSet<WoinTransaction>();
        }

        public int Id { get; set; }
        public string Number { get; set; }
        public short State { get; set; }
        public decimal Balance { get; set; }
        public short Type { get; set; }
        public int WoinerId { get; set; }
        public int? WalletParent { get; set; }
        public int? CountryId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual Country Country { get; set; }
        public virtual WoinWallet WalletParentNavigation { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
        public virtual ICollection<WoinWallet> InverseWalletParentNavigation { get; set; }
        public virtual ICollection<WoinGift> WoinGiftsWalletReceiverNavigation { get; set; }
        public virtual ICollection<WoinGift> WoinGiftsWalletSenderNavigation { get; set; }
        public virtual ICollection<WoinMovement> WoinMovements { get; set; }
        public virtual ICollection<WoinOffice> WoinOffices { get; set; }
        public virtual ICollection<WoinTransaction> WoinTransactionsWalletReceiverNavigation { get; set; }
        public virtual ICollection<WoinTransaction> WoinTransactionsWalletSenderNavigation { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinWallet ToObject(this WoinWallet entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            entity.WalletParentNavigation = null;
            entity.Country = null;
            entity.WoinGiftsWalletReceiverNavigation = null;
            entity.InverseWalletParentNavigation = null;
            entity.WoinGiftsWalletSenderNavigation = null;
            entity.WoinMovements = null;
            entity.WoinOffices = null;
            entity.WoinTransactionsWalletReceiverNavigation = null;
            entity.WoinTransactionsWalletSenderNavigation = null;
            return entity;
        }

        public static WoinWallet Builder(this WoinWallet entity)
        {
            if (entity == null) return entity;
            WoinWallet toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();

            toObject.WalletParentNavigation = entity.WalletParentNavigation.ToObject();
            toObject.Country = entity.Country.ToObject();

            entity.WoinGiftsWalletReceiverNavigation.ToList().ForEach(x =>
            {
                toObject.WoinGiftsWalletReceiverNavigation.Add(x.ToObject());
            });

            entity.InverseWalletParentNavigation.ToList().ForEach(x =>
            {
                toObject.InverseWalletParentNavigation.Add(x.ToObject());
            });
            entity.WoinGiftsWalletSenderNavigation.ToList().ForEach(x =>
            {
                toObject.WoinGiftsWalletSenderNavigation.Add(x.ToObject());
            });
            entity.WoinMovements.ToList().ForEach(x =>
            {
                toObject.WoinMovements.Add(x.ToObject());
            });
            entity.WoinOffices.ToList().ForEach(x =>
            {
                toObject.WoinOffices.Add(x.ToObject());
            });
            entity.WoinTransactionsWalletReceiverNavigation.ToList().ForEach(x =>
            {
                toObject.WoinTransactionsWalletReceiverNavigation.Add(x.ToObject());
            });
            entity.WoinTransactionsWalletSenderNavigation.ToList().ForEach(x =>
            {
                toObject.WoinTransactionsWalletSenderNavigation.Add(x.ToObject());
            });
            return toObject;
        }
    }

}
