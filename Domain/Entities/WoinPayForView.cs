﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinPayForView : BaseEntity<WoinPayForView>
    {
        public WoinPayForView()
        {
            WoinPayForViewViews = new HashSet<WoinPayForViewView>();
        }

        public int Id { get; set; }
        public decimal InitialAmount { get; set; }
        public decimal CurrentAmount { get; set; }
        public decimal PersonAmount { get; set; }
        public long ViewDuration { get; set; }
        public int ViewGift { get; set; }
        public int PaidId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinPaidAd Paid { get; set; }
        public virtual ICollection<WoinPayForViewView> WoinPayForViewViews { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinPayForView ToObject(this WoinPayForView entity)
        {
            if (entity == null) return entity;
            entity.Paid = null;
            entity.WoinPayForViewViews = null;
            return entity;
        }

        public static WoinPayForView Builder(this WoinPayForView entity)
        {
            if (entity == null) return entity;
            WoinPayForView toObject = entity.ToObject();
            toObject.Paid = entity.Paid.ToObject();

            entity.WoinPayForViewViews.ToList().ForEach(x =>
            {
                toObject.WoinPayForViewViews.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
