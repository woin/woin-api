﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinFaceToFacePurchase : BaseEntity<WoinFaceToFacePurchase>
    {
        public int Id { get; set; }
        public decimal Cash { get; set; }
        public int PurchaseId { get; set; }
        public int VendorId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinPurchase Purchase { get; set; }
        public virtual WoinWoiner Vendor { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinFaceToFacePurchase ToObject(this WoinFaceToFacePurchase entity)
        {
            if (entity == null) return entity;
            entity.Purchase = null;
            entity.Vendor = null;
            return entity;
        }

        public static WoinFaceToFacePurchase Builder(this WoinFaceToFacePurchase entity)
        {
            if (entity == null) return entity;
            WoinFaceToFacePurchase toObject = entity.ToObject();
            toObject.Purchase = entity.Purchase.ToObject();
            toObject.Vendor = entity.Vendor.ToObject();
            return toObject;
        }
    }

}
