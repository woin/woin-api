﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinPaidAd : BaseEntity<WoinPaidAd>
    {
        public WoinPaidAd()
        {
            WoinPayForViews = new HashSet<WoinPayForView>();
        }

        public int Id { get; set; }
        public long Start { get; set; }
        public long End { get; set; }
        public int AdId { get; set; }
        public int PacketId { get; set; }
        public int TransactionId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinAdPacket Packet { get; set; }
        public virtual WoinTransaction Transaction { get; set; }
        public virtual ICollection<WoinPayForView> WoinPayForViews { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinPaidAd ToObject(this WoinPaidAd entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Packet = null;
            entity.Transaction = null;
            entity.WoinPayForViews = null;
            return entity;
        }

        public static WoinPaidAd Builder(this WoinPaidAd entity)
        {
            if (entity == null) return entity;
            WoinPaidAd toObject = entity.ToObject();
            toObject.Ad = entity.Ad.ToObject();
            toObject.Packet = entity.Packet.ToObject();
            toObject.Transaction = entity.Transaction.ToObject();

            entity.WoinPayForViews.ToList().ForEach(x =>
            {
                toObject.WoinPayForViews.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
