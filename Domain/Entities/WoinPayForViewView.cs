﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinPayForViewView : BaseEntity<WoinPayForViewView>
    {
        public int Id { get; set; }
        public int WoinerId { get; set; }
        public int PayForViewId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinPayForView PayForView { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinPayForViewView ToObject(this WoinPayForViewView entity)
        {
            if (entity == null) return entity;
            entity.PayForView = null;
            entity.Woiner = null;
            return entity;
        }

        public static WoinPayForViewView Builder(this WoinPayForViewView entity)
        {
            if (entity == null) return entity;
            WoinPayForViewView toObject = entity.ToObject();
            toObject.PayForView = entity.PayForView.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            return toObject;
        }
    }
}
