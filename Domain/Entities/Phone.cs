﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class Phone : BaseEntity<Phone>
    {
        public Phone()
        {
            WoinCompaniesPhones = new HashSet<WoinCompanyPhone>();
        }

        public int Id { get; set; }
        public string Number { get; set; }
        public short State { get; set; }
        public int CountryId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<WoinCompanyPhone> WoinCompaniesPhones { get; set; }
    }

    public static partial class BuilderEntity
    {

        public static Phone ToObject(this Phone entity)
        {
			if (entity == null) return entity;
            return new Phone
            {
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                CountryId = entity.CountryId,
                CreatedAt = entity.CreatedAt,
                Number = entity.Number,
                State = entity.State
            };
        }

        public static Phone Builder(this Phone entity)
        {
			if (entity == null) return entity;
            Phone phone = new Phone
            {
                UpdatedAt = entity.UpdatedAt,
                Id = entity.Id,
                CountryId = entity.CountryId,
                CreatedAt = entity.CreatedAt,
                Number = entity.Number,
                State = entity.State
            };


            entity.WoinCompaniesPhones.ToList().ForEach(x =>
            {
                phone.WoinCompaniesPhones.Add(new WoinCompanyPhone
                {
                    State = x.State,
                    CreatedAt = x.CreatedAt,
                    CompanyId = x.CompanyId,
                    Id = x.Id,
                    PhoneId = x.PhoneId,
                    UpdatedAt = x.UpdatedAt
                });
            });

            return phone;

        }

    }
}
