﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinCalification : BaseEntity<WoinCalification>
    {
        public int Id { get; set; }
        public short Value { get; set; }
        public string Comment { get; set; }
        public int WoinerSenderId { get; set; }
        public int WoinerReceiverId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinWoiner WoinerReceiver { get; set; }
        public virtual WoinWoiner WoinerSender { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinCalification ToObject(this WoinCalification entity)
        {
            entity.WoinerReceiver = null;
            entity.WoinerSender = null;
            return entity;
        }

        public static WoinCalification Builder(this WoinCalification entity)
        {
            WoinCalification woinCalification = entity.ToObject();
            woinCalification.WoinerSender = entity.WoinerSender.ToObject();
            woinCalification.WoinerReceiver = entity.WoinerReceiver.ToObject();
            return woinCalification;
        }
    }

 }
