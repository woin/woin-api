﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinSubcategory : BaseEntity<WoinSubcategory>
    {
        public WoinSubcategory()
        {
            InverseParent = new HashSet<WoinSubcategory>();
            WoinAds = new HashSet<WoinAd>();
            WoinColumnsSubcategories = new HashSet<WoinColumnSubcategory>();
            WoinProducts = new HashSet<WoinProduct>();
            WoinWoinersPreferences = new HashSet<WoinWoinerPreference>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public int CategoryId { get; set; }
        public int ParentId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinCategory Category { get; set; }
        public virtual WoinSubcategory Parent { get; set; }
        public virtual ICollection<WoinSubcategory> InverseParent { get; set; }
        public virtual ICollection<WoinAd> WoinAds { get; set; }
        public virtual ICollection<WoinColumnSubcategory> WoinColumnsSubcategories { get; set; }
        public virtual ICollection<WoinProduct> WoinProducts { get; set; }
        public virtual ICollection<WoinWoinerPreference> WoinWoinersPreferences { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinSubcategory ToObject(this WoinSubcategory entity)
        {
            if (entity == null) return entity;

            return new WoinSubcategory { 
                 CategoryId = entity.CategoryId,
                 CreatedAt = entity.CreatedAt,
                 Id = entity.Id,
                 Name = entity.Name,
                 ParentId = entity.ParentId,
                 State = entity.State,
                 UpdatedAt = entity.UpdatedAt,
            };
        }

        public static WoinSubcategory Builder(this WoinSubcategory entity)
        {
            if (entity == null) return entity;
            WoinSubcategory toObject = entity.ToObject();
            toObject.Category = entity.Category.ToObject();
            toObject.Parent = entity.Parent.ToObject();

            entity.InverseParent.ToList().ForEach(x =>
            {
                toObject.InverseParent.Add(x.ToObject());
            });

            entity.WoinAds.ToList().ForEach(x =>
            {
                toObject.WoinAds.Add(x.ToObject());
            });

            entity.WoinColumnsSubcategories.ToList().ForEach(x =>
            {
                toObject.WoinColumnsSubcategories.Add(x.ToObject());
            });


            entity.WoinProducts.ToList().ForEach(x =>
            {
                toObject.WoinProducts.Add(x.ToObject());
            });

            entity.WoinWoinersPreferences.ToList().ForEach(x =>
            {
                toObject.WoinWoinersPreferences.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
