﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinOffice : BaseEntity<WoinOffice>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public short Type { get; set; }
        public short State { get; set; }
        public int CompanyId { get; set; }
        public int LocationId { get; set; }
        public int WalletId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinCompany Company { get; set; }
        public virtual WoinLocation Location { get; set; }
        public virtual WoinWallet Wallet { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinOffice ToObject(this WoinOffice entity)
        {
            if (entity == null) return entity;
            entity.Company = null;
            entity.Location = null;
            entity.Wallet = null;
            return entity;
        }

        public static WoinOffice Builder(this WoinOffice entity)
        {
            if (entity == null) return entity;
            WoinOffice toObject = entity.ToObject();
            toObject.Company = entity.Company.ToObject();
            toObject.Location = entity.Location.ToObject();
            toObject.Wallet = entity.Wallet.ToObject();
            return toObject;
        }
    }

}
