﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinRole : BaseEntity<WoinRole>
    {
        public WoinRole()
        {
            WoinGroups = new HashSet<WoinGroup>();
            WoinRolesPermissions = new HashSet<WoinRolePermission>();
            WoinUsersRoles = new HashSet<WoinUserRole>();
            WoinVisitors = new HashSet<WoinVisitor>();
            WoinWoiners = new HashSet<WoinWoiner>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public short Level { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual ICollection<WoinGroup> WoinGroups { get; set; }
        public virtual ICollection<WoinRolePermission> WoinRolesPermissions { get; set; }
        public virtual ICollection<WoinUserRole> WoinUsersRoles { get; set; }
        public virtual ICollection<WoinVisitor> WoinVisitors { get; set; }
        public virtual ICollection<WoinWoiner> WoinWoiners { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinRole ToObject(this WoinRole entity)
        {
            if (entity == null) return entity;

            return new WoinRole
            {
                CreatedAt = entity.CreatedAt,
                Description = entity.Description,
                Id = entity.Id,
                Level = entity.Level,
                Name = entity.Name,
                State = entity.State,
                UpdatedAt = entity.UpdatedAt, 
                
            };
        }

        public static WoinRole Builder(this WoinRole entity)
        {
            if (entity == null) return entity;
            WoinRole toObject = entity.ToObject();



            entity.WoinGroups.ToList().ForEach(x =>
            {
                toObject.WoinGroups.Add(x.ToObject());
            });

            entity.WoinRolesPermissions.ToList().ForEach(x =>
            {
                toObject.WoinRolesPermissions.Add(x.ToObject());
            });

            entity.WoinUsersRoles.ToList().ForEach(x =>
            {
                toObject.WoinUsersRoles.Add(x.ToObject());
            });

            entity.WoinVisitors.ToList().ForEach(x =>
            {
                toObject.WoinVisitors.Add(x.ToObject());
            });

            entity.WoinWoiners.ToList().ForEach(x =>
            {
                toObject.WoinWoiners.Add(x.ToObject());
            });
            return toObject;
        }
    }

}
