﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinCliwoiner : BaseEntity<WoinCliwoiner>
    {
        public int Id { get; set; }
        public int WoinerId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinWoiner Woiner { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinCliwoiner ToObject(this WoinCliwoiner entity)
        {
            entity.Woiner = null;
            return entity;
        }

        public static WoinCliwoiner Builder(this WoinCliwoiner entity)
        {
            WoinCliwoiner toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            return toObject;
        }
    }


}
