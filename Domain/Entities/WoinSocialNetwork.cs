﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinSocialNetwork : BaseEntity<WoinSocialNetwork>
    {
        public WoinSocialNetwork()
        {
            WoinSocialProfiles = new HashSet<WoinSocialProfile>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public int MultimediaId { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinMultimedia Multimedia { get; set; }
        public virtual ICollection<WoinSocialProfile> WoinSocialProfiles { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinSocialNetwork ToObject(this WoinSocialNetwork entity)
        {
            if (entity == null) return entity;
            entity.Multimedia = null;
            return entity;
        }

        public static WoinSocialNetwork Builder(this WoinSocialNetwork entity)
        {
            if (entity == null) return entity;
            WoinSocialNetwork toObject = entity.ToObject();
            toObject.Multimedia = entity.Multimedia.ToObject();

            entity.WoinSocialProfiles.ToList().ForEach(x =>
            {
                toObject.WoinSocialProfiles.Add(x.ToObject());
            });

            return toObject;
        }
    }
}
