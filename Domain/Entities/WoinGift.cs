﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinGift : BaseEntity<WoinGift>
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public short State { get; set; }
        public int[] Transactions { get; set; }
        public int WalletSender { get; set; }
        public int WalletReceiver { get; set; }
        public int UserId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinUser User { get; set; }
        public virtual WoinWallet WalletReceiverNavigation { get; set; }
        public virtual WoinWallet WalletSenderNavigation { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinGift ToObject(this WoinGift entity)
        {
            if (entity == null) return entity;
            entity.User = null;
            entity.WalletReceiverNavigation = null;
            entity.WalletSenderNavigation = null;
            return entity;
        }

        public static WoinGift Builder(this WoinGift entity)
        {
            if (entity == null) return entity;
            WoinGift toObject = entity.ToObject();
            toObject.WalletReceiverNavigation = entity.WalletReceiverNavigation.ToObject();
            toObject.WalletSenderNavigation = entity.WalletSenderNavigation.ToObject();
            toObject.User = entity.User.ToObject();
            return toObject;
        }
    }

}
