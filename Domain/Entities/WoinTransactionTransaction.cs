﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinTransactionTransaction : BaseEntity<WoinTransactionTransaction>
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int ChildId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinTransaction Child { get; set; }
        public virtual WoinTransaction Parent { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinTransactionTransaction ToObject(this WoinTransactionTransaction entity)
        {
            if (entity == null) return entity;
            entity.Child = null;
            entity.Parent = null;
            return entity;
        }

        public static WoinTransactionTransaction Builder(this WoinTransactionTransaction entity)
        {
            if (entity == null) return entity;
            WoinTransactionTransaction toObject = entity.ToObject();
            toObject.Child = entity.Child.ToObject();
            toObject.Parent = entity.Parent.ToObject();
            return toObject;
        }
    }

}
