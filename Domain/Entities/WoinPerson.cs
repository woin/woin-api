﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinPerson : BaseEntity<WoinPerson>
    {
        public WoinPerson()
        {
            WoinDocuments = new HashSet<WoinDocument>();
            WoinWoiners = new HashSet<WoinWoiner>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Secondname { get; set; }
        public string FirstLastname { get; set; }
        public string SecondLastname { get; set; }
        public long Birthdate { get; set; }
        public short Gender { get; set; }
        public int? UserId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinUser User { get; set; }
        public virtual ICollection<WoinDocument> WoinDocuments { get; set; }
        public virtual ICollection<WoinWoiner> WoinWoiners { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinPerson ToObject(this WoinPerson entity)
        {
            if (entity == null) return entity;
            entity.User = null;
            entity.WoinDocuments = null;
            entity.WoinWoiners = null;
            return entity;
        }

        public static WoinPerson Builder(this WoinPerson entity)
        {
            if (entity == null) return entity;
            WoinPerson toObject = entity.ToObject();
            toObject.User = entity.User.ToObject();
            entity.WoinDocuments.ToList().ForEach(x =>
            {
                toObject.WoinDocuments.Add(x.ToObject());

            });
            entity.WoinWoiners.ToList().ForEach(x =>
            {
               toObject.WoinWoiners.Add(x.ToObject());

            });
            return toObject;
        }
    }
}
