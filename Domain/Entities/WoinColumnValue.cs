﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinColumnValue : BaseEntity<WoinColumnValue>
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public short State { get; set; }
        public int AdId { get; set; }
        public int ColumnId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinAd Ad { get; set; }
        public virtual WoinColumn Column { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinColumnValue ToObject(this WoinColumnValue entity)
        {
            if (entity == null) return entity;
            entity.Ad = null;
            entity.Column = null;
            return entity;
        }

        public static WoinColumnValue Builder(this WoinColumnValue entity)
        {
            if (entity == null) return entity;
            WoinColumnValue toObject = entity.ToObject();
            toObject.Ad = entity.Ad.ToObject();
            toObject.Column = entity.Column.ToObject();
            return toObject;
        }
    }

}
