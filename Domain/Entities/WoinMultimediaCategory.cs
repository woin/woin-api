﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinMultimediaCategory : BaseEntity<WoinMultimediaCategory>
    {
        public int Id { get; set; }
        public int MultmediaId { get; set; }
        public int CategoryId { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinCategory Category { get; set; }
        public virtual WoinMultimedia Multmedia { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinMultimediaCategory ToObject(this WoinMultimediaCategory entity)
        {
            if (entity == null) return entity;
            entity.Category = null;
            entity.Multmedia = null;
            return entity;
        }

        public static WoinMultimediaCategory Builder(this WoinMultimediaCategory entity)
        {
            if (entity == null) return entity;
            WoinMultimediaCategory toObject = entity.ToObject();
            toObject.Category = entity.Category.ToObject();
            toObject.Multmedia = entity.Multmedia.ToObject();
            return toObject;
        }
    }

}
