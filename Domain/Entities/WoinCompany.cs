﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinCompany : BaseEntity<WoinCompany>
    {
        public WoinCompany()
        {
            WoinCompaniesPhones = new HashSet<WoinCompanyPhone>();
            WoinOffices = new HashSet<WoinOffice>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short State { get; set; }
        public short Type { get; set; }
        public int EmwoinerId { get; set; }
        public int LocationId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinEmWoiner Emwoiner { get; set; }
        public virtual WoinLocation Location { get; set; }
        public virtual WoinAdCompany WoinAdsCompanies { get; set; }
        public virtual ICollection<WoinCompanyPhone> WoinCompaniesPhones { get; set; }
        public virtual ICollection<WoinOffice> WoinOffices { get; set; }

        
    }


    public static partial class BuilderEntity
    {
        public static WoinCompany ToObject(this WoinCompany entity)
        {
            if (entity == null) return entity;
            entity.Location = null;
            entity.Emwoiner = null;
            entity.WoinAdsCompanies = null;
            return entity;
        }

        public static WoinCompany Builder(this WoinCompany entity)
        {
            if (entity == null) return entity;
            WoinCompany toObject = entity.ToObject();
            toObject.Emwoiner = entity.Emwoiner.ToObject();
            toObject.Location = entity.Location.ToObject();
            toObject.WoinAdsCompanies = entity.WoinAdsCompanies.ToObject();

            entity.WoinCompaniesPhones.ToList().ForEach(x =>
            {
                toObject.WoinCompaniesPhones.Add(x.ToObject());
            });

            entity.WoinOffices.ToList().ForEach(x =>
            {
                toObject.WoinOffices.Add(x.ToObject());
            });


            return toObject;
        }
    }

}
