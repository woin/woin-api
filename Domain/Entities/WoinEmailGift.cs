﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinEmailGift : BaseEntity<WoinEmailGift>
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public decimal Amount { get; set; }
        public short State { get; set; }
        public int WoinerId { get; set; }
        public int? TransactionId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinTransaction Transaction { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinEmailGift ToObject(this WoinEmailGift entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            entity.Transaction = null;
            return entity;
        }

        public static WoinEmailGift Builder(this WoinEmailGift entity)
        {
            if (entity == null) return entity;
            WoinEmailGift toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            entity.Transaction = entity.Transaction.ToObject();
            return toObject;
        }
    }

}
