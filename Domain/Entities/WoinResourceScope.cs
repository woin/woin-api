﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinResourceScope : BaseEntity<WoinResourceScope>
    {
        public int Id { get; set; }
        public int? CountryId { get; set; }
        public int? GovernorateId { get; set; }
        public int? CityId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }
    }


    public static partial class BuilderEntity
    {
        public static WoinResourceScope ToObject(this WoinResourceScope entity)
        {
          
            return entity;
        }

        public static WoinResourceScope Builder(this WoinResourceScope entity)
        {
            if (entity == null) return entity;
            WoinResourceScope toObject = entity.ToObject();
            
            return toObject;
        }
    }

}
