﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinUserRole : BaseEntity<WoinUserRole>
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinRole Role { get; set; }
        public virtual WoinUser User { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinUserRole ToObject(this WoinUserRole entity)
        {
            if (entity == null) return entity;
            entity.Role = null;
            entity.User = null;
            return entity;
        }

        public static WoinUserRole Builder(this WoinUserRole entity)
        {
            if (entity == null) return entity;
            WoinUserRole toObject = entity.ToObject();
            toObject.Role = entity.Role.ToObject();
            toObject.User = entity.User.ToObject();
            return toObject;
        }
    }

}
