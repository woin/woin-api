﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinVisitor : BaseEntity<WoinVisitor>
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public string Mac { get; set; }
        public int? LocationId { get; set; }
        public int RoleId { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual WoinLocation Location { get; set; }
        public virtual WoinRole Role { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinVisitor ToObject(this WoinVisitor entity)
        {
            if (entity == null) return entity;
            entity.Location = null;
            entity.Role = null;
            return entity;
        }

        public static WoinVisitor Builder(this WoinVisitor entity)
        {
            if (entity == null) return entity;
            WoinVisitor toObject = entity.ToObject();
            toObject.Location = entity.Location.ToObject();
            toObject.Role = entity.Role.ToObject();
            return toObject;
        }
    }

}
