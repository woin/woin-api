﻿using Domain.Base;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class WoinSocialProfile : BaseEntity<WoinSocialProfile>
    {
        public int Id { get; set; }
        public string Profile { get; set; }
        public short State { get; set; }
        public int SocialNetworkId { get; set; }
        public int WoinerId { get; set; }
        public long CreatedAt { get; set; }
        public long UpdatedAt { get; set; }

        public virtual WoinSocialNetwork SocialNetwork { get; set; }
        public virtual WoinWoiner Woiner { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinSocialProfile ToObject(this WoinSocialProfile entity)
        {
            if (entity == null) return entity;
            entity.Woiner = null;
            entity.SocialNetwork = null;
            return entity;
        }

        public static WoinSocialProfile Builder(this WoinSocialProfile entity)
        {
            if (entity == null) return entity;
            WoinSocialProfile toObject = entity.ToObject();
            toObject.Woiner = entity.Woiner.ToObject();
            toObject.SocialNetwork = entity.SocialNetwork.ToObject();
            return toObject;
        }
    }



}
