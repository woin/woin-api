﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public partial class WoinMultimedia : BaseEntity<WoinMultimedia>
    {
        public WoinMultimedia()
        {
            WoinMultimediasAds = new HashSet<WoinMultimediaAd>();
            WoinMultimediasCategories = new HashSet<WoinMultimediaCategory>();
            WoinSocialNetworks = new HashSet<WoinSocialNetwork>();
            WoinWoiners = new HashSet<WoinWoiner>();
        }

        public int Id { get; set; }
        public string Source { get; set; }
        public string Description { get; set; }
        public short Type { get; set; }
        public short State { get; set; }
        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }

        public virtual ICollection<WoinMultimediaAd> WoinMultimediasAds { get; set; }
        public virtual ICollection<WoinMultimediaCategory> WoinMultimediasCategories { get; set; }
        public virtual ICollection<WoinSocialNetwork> WoinSocialNetworks { get; set; }
        public virtual ICollection<WoinWoiner> WoinWoiners { get; set; }
    }

    public static partial class BuilderEntity
    {
        public static WoinMultimedia ToObject(this WoinMultimedia entity)
        {
            if (entity == null) return entity;
            entity.WoinMultimediasAds = null;
            entity.WoinMultimediasCategories = null;
            entity.WoinSocialNetworks = null;
            entity.WoinWoiners = null;
            return entity;
        }

        public static WoinMultimedia Builder(this WoinMultimedia entity)
        {
            if (entity == null) return entity;
            WoinMultimedia toObject = entity.ToObject();


            entity.WoinMultimediasAds.ToList().ForEach(x =>
            {
                toObject.WoinMultimediasAds.Add(x.ToObject());
            });

            entity.WoinMultimediasCategories.ToList().ForEach(x =>
            {
                toObject.WoinMultimediasCategories.Add(x.ToObject());
            });

            entity.WoinSocialNetworks.ToList().ForEach(x =>
            {
                toObject.WoinSocialNetworks.Add(x.ToObject());
            });

            entity.WoinWoiners.ToList().ForEach(x =>
            {
                toObject.WoinWoiners.Add(x.ToObject());
            });

            return toObject;
        }
    }

}
