﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

public class WhereList
{
    [Required]
    public string Key { set; get; }
    [Required]
    public string Value { set; get; }

    [Required]
    public string Condition { set; get; } = "";

    public string Operator { set; get; } = "=";
}

