﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;

public static partial class ObjectCustomer
{
    public static string String<T>(this T entity)
    {
        return JsonConvert.SerializeObject(entity);
    }

}