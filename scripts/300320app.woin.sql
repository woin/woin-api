CREATE TABLE "woin_persons" (
"id" serial NOT NULL,
"firstname" varchar(50) NOT NULL,
"secondname" varchar(50),
"first_lastname" varchar(45) NOT NULL,
"second_lastname" varchar(45),
"birthdate" bigint NOT NULL,
"gender" smallint NOT NULL,
"user_id" integer,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_persons_users" UNIQUE ("user_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_persons" IS 'Tabla que contiene las personas que participan en el sistema';
COMMENT ON COLUMN "woin_persons"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_persons"."firstname" IS 'Primer nombre de la persona';
COMMENT ON COLUMN "woin_persons"."secondname" IS 'Segundo nombre de la persona';
COMMENT ON COLUMN "woin_persons"."first_lastname" IS 'Primer apellido de la persona';
COMMENT ON COLUMN "woin_persons"."second_lastname" IS 'Segundo apellido de la persona';
COMMENT ON COLUMN "woin_persons"."birthdate" IS 'Fecha de nacimiento de la persona';
COMMENT ON COLUMN "woin_persons"."gender" IS 'Género de la persona (masculino, femenino, otro)';
COMMENT ON COLUMN "woin_persons"."user_id" IS 'Usuario al que pertenece la persona';
COMMENT ON COLUMN "woin_persons"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_persons"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_users" (
"id" serial NOT NULL,
"username" varchar(12) NOT NULL,
"email" varchar(50) NOT NULL,
"password" varchar(255) NOT NULL,
"remember_password" varchar(255),
"state" smallint NOT NULL DEFAULT 0,
"role_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "username_uq" UNIQUE ("username")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_users" IS 'Tabla que contiene los usuarios registrados en la aplicación o sistema';
COMMENT ON COLUMN "woin_users"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_users"."username" IS 'Nombre de usuario para inicio de sesión';
COMMENT ON COLUMN "woin_users"."email" IS 'Usuario del usuario para inicio de sesión y recuperación de contraseña';
COMMENT ON COLUMN "woin_users"."password" IS 'Contraseña del usuario para inicio de sesión';
COMMENT ON COLUMN "woin_users"."remember_password" IS 'Contraseña anterior del usuario';
COMMENT ON COLUMN "woin_users"."state" IS 'Estado del usuario (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_users"."role_id" IS 'Rol del usuario con el que se gestionarán sus permisos';
COMMENT ON COLUMN "woin_users"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_users"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "devices" (
"id" serial NOT NULL,
"name" varchar(50) NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"mac" varchar(45),
"ip" varchar(45),
"user_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "devices" IS 'Tabla que contiene los dispositivos pertenecientes a los usuarios';
COMMENT ON COLUMN "devices"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "devices"."name" IS 'Nombre del dispositivo';
COMMENT ON COLUMN "devices"."state" IS 'Estado actual del dispositivo';
COMMENT ON COLUMN "devices"."mac" IS 'Identificación MAC del dispositivo';
COMMENT ON COLUMN "devices"."ip" IS 'Identificación IP del dispositivo';
COMMENT ON COLUMN "devices"."user_id" IS 'Usuario al que pertenece el dispositivo';
COMMENT ON COLUMN "devices"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "devices"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_sessions" (
"id" serial NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"user_id" integer NOT NULL,
"device_id" integer,
"location_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_sessions" IS 'Tabla que contiene las sesiones de los usuarios en el sistema';
COMMENT ON COLUMN "woin_sessions"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_sessions"."state" IS 'Estado de la sesión (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_sessions"."user_id" IS 'Usuario al que pertenece la sesión';
COMMENT ON COLUMN "woin_sessions"."device_id" IS 'Dispositivo desde el que se inició sesión';
COMMENT ON COLUMN "woin_sessions"."location_id" IS 'Ubicación gps desde la que se inició la sesión';
COMMENT ON COLUMN "woin_sessions"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_sessions"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_documents" (
"id" serial NOT NULL,
"number" varchar(15) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"city_id" integer NOT NULL,
"person_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_documents" IS 'Tabla que contiene los documentos personales (identificaciones)';
COMMENT ON COLUMN "woin_documents"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_documents"."number" IS 'Número del documento de identidad';
COMMENT ON COLUMN "woin_documents"."state" IS 'Estado del documento (activo, inactivo, en espera, etc)';
COMMENT ON COLUMN "woin_documents"."type" IS 'Tipo de documento (cédula, extranjero, etc)';
COMMENT ON COLUMN "woin_documents"."city_id" IS 'Ciudad de registro del documento';
COMMENT ON COLUMN "woin_documents"."person_id" IS 'Persona a la que pertenece el documento';
COMMENT ON COLUMN "woin_documents"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_documents"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "cities" (
"id" serial NOT NULL,
"name" varchar(50) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"governorate_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "cities" IS 'Tabla que contiene las ciudades que se usarán en la aplicación';
COMMENT ON COLUMN "cities"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "cities"."name" IS 'Nombre de la ciudad';
COMMENT ON COLUMN "cities"."state" IS 'Estado actual de la ciudad';
COMMENT ON COLUMN "cities"."governorate_id" IS 'Gobernación a la que pertenece';
COMMENT ON COLUMN "cities"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "cities"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "governorates" (
"id" serial NOT NULL,
"name" varchar(50) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"country_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "governorates" IS 'Tabla que contiene las gobernaciones quienes contienen a ciudades y pertenecen a un país';
COMMENT ON COLUMN "governorates"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "governorates"."name" IS 'Nombre de la gobernación';
COMMENT ON COLUMN "governorates"."state" IS 'Estado actual de la gobernación';
COMMENT ON COLUMN "governorates"."country_id" IS 'País al que pertenece la gobernación';
COMMENT ON COLUMN "governorates"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "governorates"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "countries" (
"id" serial NOT NULL,
"name" varchar(50) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "countries" IS 'Tabla que contiene los países que se usarán en la aplicación';
COMMENT ON COLUMN "countries"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "countries"."name" IS 'Nombre del país';
COMMENT ON COLUMN "countries"."state" IS 'Estado actual del país';
COMMENT ON COLUMN "countries"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "countries"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "phones" (
"id" serial NOT NULL,
"number" varchar(45) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"country_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "phones" IS 'Tabla que contiene los teléfonos usados en la aplicación por woiners, compañías, oficinas, etc';
COMMENT ON COLUMN "phones"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "phones"."number" IS 'Número telefónico';
COMMENT ON COLUMN "phones"."state" IS 'Estado actual del teléfono';
COMMENT ON COLUMN "phones"."country_id" IS 'País al que pertenece el teléfono';
COMMENT ON COLUMN "phones"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "phones"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_woiners" (
"id" serial NOT NULL,
"codewoiner" varchar(12) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"biography" text,
"person_id" integer NOT NULL,
"user_id" integer NOT NULL,
"profile_id" integer NOT NULL,
"role_id" integer NOT NULL,
"reference" integer,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_woiners" IS 'Tabla que contiene los woiners quienes son los principales actores del sistema';
COMMENT ON COLUMN "woin_woiners"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_woiners"."codewoiner" IS 'Código que representa al woiner de manera pública';
COMMENT ON COLUMN "woin_woiners"."state" IS 'Estado del woiner (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_woiners"."biography" IS 'Biografía o descripción del woiner';
COMMENT ON COLUMN "woin_woiners"."person_id" IS 'Persona a la que pertenece el woiner';
COMMENT ON COLUMN "woin_woiners"."user_id" IS 'Usuario al que pertenece el woiner';
COMMENT ON COLUMN "woin_woiners"."profile_id" IS 'Imagen o multimedia de perfil del woiner';
COMMENT ON COLUMN "woin_woiners"."role_id" IS 'Rol que se le asigna al woiner para controlar sus permisos';
COMMENT ON COLUMN "woin_woiners"."reference" IS 'Woiner referido, es decir el que nos trajo a la plataforma convirtiéndose en padre de la red';
COMMENT ON COLUMN "woin_woiners"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_woiners"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_woiners_configs" (
"id" serial NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"woiner_id" integer NOT NULL,
"minimum_gift_percentage" smallint NOT NULL DEFAULT 10,
"gift_for_cash" bit NOT NULL,
"minimum_gift_limit_amount" smallint NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_woiners_configs" IS 'Tabla que contiene las configuraciones de los woiners';
COMMENT ON COLUMN "woin_woiners_configs"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_woiners_configs"."state" IS 'Estado de la configuración del woiner';
COMMENT ON COLUMN "woin_woiners_configs"."woiner_id" IS 'Woiner al que pertenece la configuración';
COMMENT ON COLUMN "woin_woiners_configs"."minimum_gift_percentage" IS 'Porcentaje mínimo de regalo del woiner en sus anuncios';
COMMENT ON COLUMN "woin_woiners_configs"."gift_for_cash" IS 'Valor que indica si el woiner regalará por compras en efectivo (0 = No, 1 = Sí)';
COMMENT ON COLUMN "woin_woiners_configs"."minimum_gift_limit_amount" IS 'Monto a partir del cual quiere regalar menos del porcentaje mínimo de regalo';
COMMENT ON COLUMN "woin_woiners_configs"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_woiners_configs"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_roles" (
"id" serial NOT NULL,
"name" varchar(50) NOT NULL,
"description" text,
"level" smallint NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_roles" IS 'Tabla que contiene los roles que se manejarán en el sistema (woiner, cliwoiner, etc)';
COMMENT ON COLUMN "woin_roles"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_roles"."name" IS 'Nombre del rol o código asignado por woin';
COMMENT ON COLUMN "woin_roles"."description" IS 'Descripción o detalle del rol';
COMMENT ON COLUMN "woin_roles"."level" IS 'Nivel del rol (primario, secundario, etc)';
COMMENT ON COLUMN "woin_roles"."state" IS 'Estado del rol (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_roles"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_roles"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_permissions" (
"id" serial NOT NULL,
"name" varchar(45) NOT NULL,
"description" text,
"level" smallint NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_permissions" IS 'Tabla que contiene los permisos que permitirán ejecutar acciones en la aplicación';
COMMENT ON COLUMN "woin_permissions"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_permissions"."name" IS 'Nombre del permiso o código asignado por woin';
COMMENT ON COLUMN "woin_permissions"."description" IS 'Descripción o detalle del permiso, explicación de este';
COMMENT ON COLUMN "woin_permissions"."level" IS 'Nivel del permiso (prioritario, crítico, etc)';
COMMENT ON COLUMN "woin_permissions"."state" IS 'Estado del permiso (activo, inactivo)';
COMMENT ON COLUMN "woin_permissions"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_permissions"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_roles_permissions" (
"id" serial NOT NULL,
"role_id" integer NOT NULL,
"permission_id" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_roles_id_permission_id" UNIQUE ("role_id", "permission_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_roles_permissions" IS 'Tabla intercepto que contiene los permisos relacionados a un rol';
COMMENT ON COLUMN "woin_roles_permissions"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_roles_permissions"."role_id" IS 'Identificador del rol';
COMMENT ON COLUMN "woin_roles_permissions"."permission_id" IS 'Identificador del permiso relacionado';
COMMENT ON COLUMN "woin_roles_permissions"."state" IS 'Estado del rol (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_roles_permissions"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_roles_permissions"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_ads" (
"id" serial NOT NULL,
"title" varchar(100) NOT NULL,
"description" text,
"initial_time" bigint NOT NULL,
"final_time" bigint NOT NULL,
"price" decimal(17,2) NOT NULL,
"gift_percentage" smallint NOT NULL DEFAULT 0,
"discount_percentage" smallint NOT NULL DEFAULT 0,
"initial_stock" integer NOT NULL DEFAULT 0,
"current_stock" integer NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"ad_parent" integer NOT NULL,
"woiner_id" integer NOT NULL,
"subcategory_id" integer NOT NULL,
"product_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_ads" IS 'Tabla que contiene las publicaciones o anuncios creados en la aplicación';
COMMENT ON COLUMN "woin_ads"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_ads"."title" IS 'Título de la publicación';
COMMENT ON COLUMN "woin_ads"."description" IS 'Descripción o cuerpo de la publicación';
COMMENT ON COLUMN "woin_ads"."initial_time" IS 'Tiempo de inicio para mostrar la publicación';
COMMENT ON COLUMN "woin_ads"."final_time" IS 'Tiempo de finalización de la publicación';
COMMENT ON COLUMN "woin_ads"."price" IS 'Precio de venta del objeto/servicio publicado';
COMMENT ON COLUMN "woin_ads"."gift_percentage" IS 'Porcentaje del price que se regalará';
COMMENT ON COLUMN "woin_ads"."discount_percentage" IS 'Porcentaje descontado de la publicación';
COMMENT ON COLUMN "woin_ads"."initial_stock" IS 'Cantidad inicial disponible de la publicación';
COMMENT ON COLUMN "woin_ads"."current_stock" IS 'Cantidad actual disponible';
COMMENT ON COLUMN "woin_ads"."state" IS 'Estado actual de la publicación';
COMMENT ON COLUMN "woin_ads"."type" IS 'Tipo de anuncio (gratis, pago)';
COMMENT ON COLUMN "woin_ads"."ad_parent" IS 'Publicación anterior o padre (en caso de que esta sea creada a partir de una publicación anterior)';
COMMENT ON COLUMN "woin_ads"."woiner_id" IS 'Woiner al que pertenece la publicación';
COMMENT ON COLUMN "woin_ads"."subcategory_id" IS 'Subcategoría a la que pertenece el anuncio';
COMMENT ON COLUMN "woin_ads"."product_id" IS 'Producto/recurso que se está promocionando';
COMMENT ON COLUMN "woin_ads"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_ads"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_califications" (
"id" serial NOT NULL,
"value" smallint NOT NULL DEFAULT 0,
"comment" varchar(255),
"woiner_sender_id" integer NOT NULL,
"woiner_receiver_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_califications" IS 'Tabla que contiene las calificaciones hechas de un woiner a otro';
COMMENT ON COLUMN "woin_califications"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_califications"."value" IS 'Valor de la calificación (1…5)';
COMMENT ON COLUMN "woin_califications"."comment" IS 'Comentario hecho al momento de calificar';
COMMENT ON COLUMN "woin_califications"."woiner_sender_id" IS 'Woiner que califica (calificador)';
COMMENT ON COLUMN "woin_califications"."woiner_receiver_id" IS 'Woiner al que se califica (calificado)';
COMMENT ON COLUMN "woin_califications"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_califications"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_emwoiners" (
"id" serial NOT NULL,
"woiner_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_emwoiners" IS 'Tabla que contiene los registros de los emwoiners, que son tipos de woiners';
COMMENT ON COLUMN "woin_emwoiners"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_emwoiners"."woiner_id" IS 'Woiner al que hace referencia';
COMMENT ON COLUMN "woin_emwoiners"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_emwoiners"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_companies" (
"id" serial NOT NULL,
"name" varchar(255) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"emwoiner_id" integer NOT NULL,
"location_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_woin_companies_name" UNIQUE ("name")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_companies" IS 'Tabla que contiene las compañías pertenecientes a un emwoiner';
COMMENT ON COLUMN "woin_companies"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_companies"."name" IS 'Nombre de la compañía';
COMMENT ON COLUMN "woin_companies"."state" IS 'Estado de la compañía (activa, inactiva, etc)';
COMMENT ON COLUMN "woin_companies"."type" IS 'Tipo de compañía';
COMMENT ON COLUMN "woin_companies"."emwoiner_id" IS 'Emwoiner al que pertenece la compañía';
COMMENT ON COLUMN "woin_companies"."location_id" IS 'Localización o ubicación de la compañía en gps';
COMMENT ON COLUMN "woin_companies"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_companies"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_companies_phones" (
"id" serial NOT NULL,
"company_id" integer NOT NULL,
"phone_id" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_companies_id_phones_id" UNIQUE ("company_id", "phone_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_companies_phones" IS 'Tabla intercepto que contiene teléfonos relacionados a compañías';
COMMENT ON COLUMN "woin_companies_phones"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_companies_phones"."company_id" IS 'Identificador de la compañía a la que pertenece';
COMMENT ON COLUMN "woin_companies_phones"."phone_id" IS 'Identificador del teléfono al que se hace referencia';
COMMENT ON COLUMN "woin_companies_phones"."state" IS 'Estado del teléfono para esa compañía';
COMMENT ON COLUMN "woin_companies_phones"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_companies_phones"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_visitors" (
"id" serial NOT NULL,
"ip" varchar(45),
"mac" varchar(45),
"location_id" integer,
"role_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_visitors" IS 'Tabla que contiene los visitantes o personas que ingresan a la aplicación si registrarse';
COMMENT ON COLUMN "woin_visitors"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_visitors"."ip" IS 'Identificación IP del visitante';
COMMENT ON COLUMN "woin_visitors"."mac" IS 'Identificación MAC del visitante';
COMMENT ON COLUMN "woin_visitors"."location_id" IS 'Localización del visitante';
COMMENT ON COLUMN "woin_visitors"."role_id" IS 'Rol que se le asignará al visitante para definir sus permisos';
COMMENT ON COLUMN "woin_visitors"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_visitors"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_multimedias" (
"id" serial NOT NULL,
"source" text NOT NULL,
"description" text,
"type" smallint NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_multimedias" IS 'Tabla que contiene las multimedias que serán usadas por otras tablas';
COMMENT ON COLUMN "woin_multimedias"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_multimedias"."source" IS 'Ruta o ubicación del recurso';
COMMENT ON COLUMN "woin_multimedias"."description" IS 'Descripción o detalle de la multimedia';
COMMENT ON COLUMN "woin_multimedias"."type" IS 'Tipo de multimedia (imagen, vídeo, etc)';
COMMENT ON COLUMN "woin_multimedias"."state" IS 'Estado de la multimedia (eliminada, activa)';
COMMENT ON COLUMN "woin_multimedias"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_multimedias"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_multimedias_ads" (
"id" serial NOT NULL,
"multimedia_id" integer NOT NULL,
"ad_id" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_multimedia_id_ad_id" UNIQUE ("multimedia_id", "ad_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_multimedias_ads" IS 'Tabla intercepto que contiene las multimedias relacionadas a una publicación';
COMMENT ON COLUMN "woin_multimedias_ads"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_multimedias_ads"."multimedia_id" IS 'Identificador de la multimedia';
COMMENT ON COLUMN "woin_multimedias_ads"."ad_id" IS 'Identificador de la publicación o anuncio';
COMMENT ON COLUMN "woin_multimedias_ads"."state" IS 'Estado de la multimedia para esa publicación';
COMMENT ON COLUMN "woin_multimedias_ads"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_multimedias_ads"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_social_networks" (
"id" serial NOT NULL,
"name" varchar(255) NOT NULL,
"domain" varchar(255) NOT NULL,
"multimedia_id" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_woin_social_networks_domain" UNIQUE ("domain")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_social_networks" IS 'Tabla que contiene las redes sociales que se usarán para los perfiles de los woiners';
COMMENT ON COLUMN "woin_social_networks"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_social_networks"."name" IS 'Nombre de la red social (facebook, twitter, etc)';
COMMENT ON COLUMN "woin_social_networks"."domain" IS 'Dominio de la red social (woin.com, etc)';
COMMENT ON COLUMN "woin_social_networks"."multimedia_id" IS 'Multimedia de la red social (ícono, imagen)';
COMMENT ON COLUMN "woin_social_networks"."state" IS 'Estado de la red social (activa, inactiva, etc)';
COMMENT ON COLUMN "woin_social_networks"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_social_networks"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_locations" (
"id" serial NOT NULL,
"latitude" decimal(18,15) NOT NULL,
"longitude" decimal(18,15) NOT NULL,
"altitude" decimal(18,15),
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_locations" IS 'Tabla que contiene los registros de ubicaciones gps';
COMMENT ON COLUMN "woin_locations"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_locations"."latitude" IS 'Latitud de coordenada gps';
COMMENT ON COLUMN "woin_locations"."longitude" IS 'Longitud de coordenada gps';
COMMENT ON COLUMN "woin_locations"."altitude" IS 'Altitud de coordenada gps';
COMMENT ON COLUMN "woin_locations"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_locations"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_columns_values" (
"id" serial NOT NULL,
"content" varchar(45) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"ad_id" integer NOT NULL,
"column_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_columns_values_ad_column" UNIQUE ("ad_id", "column_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_columns_values" IS 'Tabla que contiene los valores de columnas o propiedades de una publicación';
COMMENT ON COLUMN "woin_columns_values"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_columns_values"."content" IS 'Contenido de la columna para una respectiva publicación';
COMMENT ON COLUMN "woin_columns_values"."state" IS 'Estado del contenido de la columna (eliminada, etc)';
COMMENT ON COLUMN "woin_columns_values"."ad_id" IS 'Publicación a la que pertenece este valor o contenido';
COMMENT ON COLUMN "woin_columns_values"."column_id" IS 'Columna a la que hace referencia el contenido o valor';
COMMENT ON COLUMN "woin_columns_values"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_columns_values"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_columns" (
"id" serial NOT NULL,
"name" varchar(45) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_columns" IS 'Tabla de columnas que se usarán como propiedades en las publicaciones (marca, color, tamaño, etc)';
COMMENT ON COLUMN "woin_columns"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_columns"."name" IS 'Nombre de la columna o título de la propiedad';
COMMENT ON COLUMN "woin_columns"."state" IS 'Estado de la columna o propiedad (activa, …)';
COMMENT ON COLUMN "woin_columns"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_columns"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_social_profiles" (
"id" serial NOT NULL,
"profile" text NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"social_network_id" integer NOT NULL,
"woiner_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_social_profiles" IS 'Tabla que contiene los perfiles de redes sociales de los woiners';
COMMENT ON COLUMN "woin_social_profiles"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_social_profiles"."profile" IS 'Perfil del woiner (@username)';
COMMENT ON COLUMN "woin_social_profiles"."state" IS 'Estado de los perfiles sociales';
COMMENT ON COLUMN "woin_social_profiles"."social_network_id" IS 'Red social al que hace referencia el perfil';
COMMENT ON COLUMN "woin_social_profiles"."woiner_id" IS 'Woiner al que pertenece el perfil';
COMMENT ON COLUMN "woin_social_profiles"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_social_profiles"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_cliwoiners" (
"id" serial NOT NULL,
"woiner_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_cliwoiners" IS 'Tabla hija de woin_woiners, con el fin de generar una herencia';
COMMENT ON COLUMN "woin_cliwoiners"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_cliwoiners"."woiner_id" IS 'Woiner padre o del que se hereda';
COMMENT ON COLUMN "woin_cliwoiners"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_cliwoiners"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_frees_ads" (
"id" serial NOT NULL,
"start" bigint NOT NULL,
"end" bigint NOT NULL,
"ad_id" integer NOT NULL,
"packet_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_frees_ads" IS 'Tabla que contiene los anuncios o publicaciones gratis';
COMMENT ON COLUMN "woin_frees_ads"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_frees_ads"."start" IS 'Fecha de inicio del anuncio gratis';
COMMENT ON COLUMN "woin_frees_ads"."end" IS 'Fecha de fin del anuncio gratis';
COMMENT ON COLUMN "woin_frees_ads"."ad_id" IS 'Anuncio al que hace referencia';
COMMENT ON COLUMN "woin_frees_ads"."packet_id" IS 'Paquete usado para publicar el anuncio';
COMMENT ON COLUMN "woin_frees_ads"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_frees_ads"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_paids_ads" (
"id" serial NOT NULL,
"start" bigint NOT NULL,
"end" bigint NOT NULL,
"ad_id" integer NOT NULL,
"packet_id" integer NOT NULL,
"transaction_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_paids_ads" IS 'Tabla que contiene los anuncios o publicaciones pagas a través de un paquete ofrecido por woin';
COMMENT ON COLUMN "woin_paids_ads"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_paids_ads"."start" IS 'Fecha de inicio del anuncio gratis';
COMMENT ON COLUMN "woin_paids_ads"."end" IS 'Fecha de fin del anuncio gratis';
COMMENT ON COLUMN "woin_paids_ads"."ad_id" IS 'Anuncio al que hace referencia';
COMMENT ON COLUMN "woin_paids_ads"."packet_id" IS 'Paquete usado para publicar el anuncio';
COMMENT ON COLUMN "woin_paids_ads"."transaction_id" IS 'Transacción generada para el pago de paquete';
COMMENT ON COLUMN "woin_paids_ads"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_paids_ads"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_ads_packets" (
"id" serial NOT NULL,
"duration" bigint NOT NULL DEFAULT 0,
"price" decimal(17,2) NOT NULL DEFAULT 0,
"banner_time" bigint NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"schedule" smallint NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_ads_packets" IS 'Tabla que contiene los paquetes ofrecidos por woin para las publicaciones hechas por woiners';
COMMENT ON COLUMN "woin_ads_packets"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_ads_packets"."duration" IS 'Duración (visibilidad) del paquete en total';
COMMENT ON COLUMN "woin_ads_packets"."price" IS 'Precio o costo del paquete publicitario';
COMMENT ON COLUMN "woin_ads_packets"."banner_time" IS 'Tiempo que durará la publicación en el banner';
COMMENT ON COLUMN "woin_ads_packets"."type" IS 'Tipo de paquete (gratis, pago)';
COMMENT ON COLUMN "woin_ads_packets"."schedule" IS 'Tipo de horario de visibilidad (tradicional, especial)';
COMMENT ON COLUMN "woin_ads_packets"."state" IS 'Estado del paquete (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_ads_packets"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_ads_packets"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_purchases" (
"id" serial NOT NULL,
"points" decimal(17,2) NOT NULL,
"gift" decimal(17,2) NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"detail" varchar(255) NOT NULL,
"transaction_id" integer,
"purchaser_id" integer NOT NULL,
"vendor_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_purchases" IS 'Tabla que contiene las ventas registradas en la aplicación';
COMMENT ON COLUMN "woin_purchases"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_purchases"."points" IS 'Valor de la venta';
COMMENT ON COLUMN "woin_purchases"."state" IS 'Estado de la compra (espera, correcta, errónea, cancelada, etc)';
COMMENT ON COLUMN "woin_purchases"."detail" IS 'Detalle de la compra';
COMMENT ON COLUMN "woin_purchases"."transaction_id" IS 'Transacción generada por la compra';
COMMENT ON COLUMN "woin_purchases"."purchaser_id" IS 'Comprado o woiner que recibe el producto';
COMMENT ON COLUMN "woin_purchases"."vendor_id" IS 'Vendedor o woiner que recibe el pago';
COMMENT ON COLUMN "woin_purchases"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_purchases"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_online_purchases" (
"id" serial NOT NULL,
"quantity" integer NOT NULL DEFAULT 1,
"purchase_id" integer NOT NULL,
"ad_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_online_purchases_purchase_id" UNIQUE ("purchase_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_online_purchases" IS 'Tabla que contiene las ventas online, es decir, aquellas que se hacen por medio de una publicación o anuncio';
COMMENT ON COLUMN "woin_online_purchases"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_online_purchases"."quantity" IS 'Cantidad de artículos en la compra';
COMMENT ON COLUMN "woin_online_purchases"."purchase_id" IS 'Venta a la que hace referencia';
COMMENT ON COLUMN "woin_online_purchases"."ad_id" IS 'Publicación o anuncio a la que hace referencia';
COMMENT ON COLUMN "woin_online_purchases"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_online_purchases"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_face_to_face_purchases" (
"id" serial NOT NULL,
"cash" decimal(17,2) NOT NULL,
"purchase_id" integer NOT NULL,
"vendor_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_face_to_face_purchases" UNIQUE ("purchase_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_face_to_face_purchases" IS 'Tabla que contiene las ventas de tipo cara a cara es decir, sin una publicación.';
COMMENT ON COLUMN "woin_face_to_face_purchases"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_face_to_face_purchases"."cash" IS 'Cantidad pagada en efectivo';
COMMENT ON COLUMN "woin_face_to_face_purchases"."purchase_id" IS 'Venta a la que hace referencia';
COMMENT ON COLUMN "woin_face_to_face_purchases"."vendor_id" IS 'Vendedor o woiner que ofrece el producto';
COMMENT ON COLUMN "woin_face_to_face_purchases"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_face_to_face_purchases"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_categories" (
"id" serial NOT NULL,
"name" varchar(45) NOT NULL,
"description" text,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_categories" IS 'Tabla que contiene las categorías que identifican o agrupan las publicaciones [en servicios, productos, bienes raíces, transportes, etc]';
COMMENT ON COLUMN "woin_categories"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_categories"."name" IS 'Nombre o título de la categoría';
COMMENT ON COLUMN "woin_categories"."description" IS 'Descripción o detalle extenso de la categoría';
COMMENT ON COLUMN "woin_categories"."state" IS 'Estado de la categoría (activa, inactiva, etc)';
COMMENT ON COLUMN "woin_categories"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_categories"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_multimedias_categories" (
"id" serial NOT NULL,
"multmedia_id" integer NOT NULL,
"category_id" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_multimedia_id_category_id" UNIQUE ("multmedia_id", "category_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_multimedias_categories" IS 'Tabla intercepto que contiene las multimedias relacionadas a una categoría [de publicación]';
COMMENT ON COLUMN "woin_multimedias_categories"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_multimedias_categories"."multmedia_id" IS 'Identificador de la multimedia';
COMMENT ON COLUMN "woin_multimedias_categories"."category_id" IS 'Identificador de la categoría [de publicación]';
COMMENT ON COLUMN "woin_multimedias_categories"."state" IS 'Estado de la multimedia para esa categoría';
COMMENT ON COLUMN "woin_multimedias_categories"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_multimedias_categories"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_subcategories" (
"id" serial NOT NULL,
"name" varchar(45) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"category_id" integer NOT NULL,
"parent_id" bigint NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_subcategories" IS 'Tabla que contiene las subcategorias que pertenecen a una categoria [de publicación]';
COMMENT ON COLUMN "woin_subcategories"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_subcategories"."name" IS 'Nombre de la subcategoria (aseo, motocicleta, etc)';
COMMENT ON COLUMN "woin_subcategories"."state" IS 'Estado de la subcategoria (activa, inactiva)';
COMMENT ON COLUMN "woin_subcategories"."category_id" IS 'Categoría a la que pertenece la subcategoría';
COMMENT ON COLUMN "woin_subcategories"."parent_id" IS 'Subcategoría Padre';
COMMENT ON COLUMN "woin_subcategories"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_subcategories"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_columns_subcategories" (
"id" serial NOT NULL,
"column_id" integer NOT NULL,
"subcategory_id" integer NOT NULL,
"type" smallint NOT NULL,
"state" smallint NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_columns_subcategories" IS 'Tabla intercepto de columnas y subcategorias';
COMMENT ON COLUMN "woin_columns_subcategories"."column_id" IS 'Identificador de la columna a la que pertenece';
COMMENT ON COLUMN "woin_columns_subcategories"."subcategory_id" IS 'Identificador de la subcategoria a la que pertenece';
COMMENT ON COLUMN "woin_columns_subcategories"."type" IS 'Tipo de columna (primaria, secundaria, etc)';
COMMENT ON COLUMN "woin_columns_subcategories"."state" IS 'Estado de la columna perteneciente a una subcategoria';
COMMENT ON COLUMN "woin_columns_subcategories"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_columns_subcategories"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_offices" (
"id" serial NOT NULL,
"name" varchar(50) NOT NULL,
"type" smallint NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"company_id" integer NOT NULL,
"location_id" integer NOT NULL,
"wallet_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_offices" IS 'Tabla que contiene las oficinas de una compañía y sus respectivos datos';
COMMENT ON COLUMN "woin_offices"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_offices"."name" IS 'Nombre de la oficina';
COMMENT ON COLUMN "woin_offices"."type" IS 'Tipo de oficina (principal, secundaria, etc)';
COMMENT ON COLUMN "woin_offices"."state" IS 'Estado de la oficina (activa, inactiva, etc)';
COMMENT ON COLUMN "woin_offices"."company_id" IS 'Compañía a la que pertenece la oficina';
COMMENT ON COLUMN "woin_offices"."location_id" IS 'Ubicación gps de la oficina';
COMMENT ON COLUMN "woin_offices"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_offices"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_pay_for_view_views" (
"id" serial NOT NULL,
"woiner_id" integer NOT NULL,
"pay_for_view_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_pay_for_view_views_woiner" UNIQUE ("woiner_id", "pay_for_view_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_pay_for_view_views" IS 'Tabla que contiene los woiners que han visto un pago por ver';
COMMENT ON COLUMN "woin_pay_for_view_views"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_pay_for_view_views"."woiner_id" IS 'Woiner que vio el pago por ver';
COMMENT ON COLUMN "woin_pay_for_view_views"."pay_for_view_id" IS 'Pago por ver que fue visto por el woiner';
COMMENT ON COLUMN "woin_pay_for_view_views"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_pay_for_view_views"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_pay_for_views" (
"id" serial NOT NULL,
"initial_amount" decimal(17,2) NOT NULL,
"current_amount" decimal(17,2) NOT NULL,
"person_amount" decimal(17,2) NOT NULL,
"view_duration" bigint NOT NULL,
"view_gift" integer NOT NULL,
"paid_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_pay_for_views" IS 'Tabla que contiene los pagos por ver realizados por un woiner (el woiner que vea el anuncio ganará cierta cantidad de puntos)';
COMMENT ON COLUMN "woin_pay_for_views"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_pay_for_views"."initial_amount" IS 'Monto o dinero inicial para el pago por ver';
COMMENT ON COLUMN "woin_pay_for_views"."current_amount" IS 'Monto o dinero actual, lo que resta por regalar';
COMMENT ON COLUMN "woin_pay_for_views"."person_amount" IS 'Monto o dinero a regalar por persona';
COMMENT ON COLUMN "woin_pay_for_views"."view_duration" IS 'Duración por vista de woiner para regalar';
COMMENT ON COLUMN "woin_pay_for_views"."view_gift" IS 'Vistas esperadas para regalar';
COMMENT ON COLUMN "woin_pay_for_views"."paid_id" IS 'Anuncio pago al que hace referencia';
COMMENT ON COLUMN "woin_pay_for_views"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_pay_for_views"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_wallets" (
"id" serial NOT NULL,
"number" varchar(20) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"balance" decimal(17,2) NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"woiner_id" integer NOT NULL,
"wallet_parent" integer,
"country_id" integer,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_woin_wallets_number" UNIQUE ("number")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_wallets" IS 'Tabla que contiene las billeteras de los woiners que contienen los puntos woins';
COMMENT ON COLUMN "woin_wallets"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_wallets"."number" IS 'Número de la cuenta que la identifica a nivel público';
COMMENT ON COLUMN "woin_wallets"."state" IS 'Estado de la billetera (activa, inactiva, etc)';
COMMENT ON COLUMN "woin_wallets"."balance" IS 'Balance o total de puntos en la billetera';
COMMENT ON COLUMN "woin_wallets"."type" IS 'Tipo de billetera (general, redimible, no redimible, comprometida)';
COMMENT ON COLUMN "woin_wallets"."woiner_id" IS 'Woiner al que pertenece la billetera';
COMMENT ON COLUMN "woin_wallets"."wallet_parent" IS 'Billetera padre, en caso de ser una subcuenta de una cuenta principal';
COMMENT ON COLUMN "woin_wallets"."country_id" IS 'País al que pertenece la billetera';
COMMENT ON COLUMN "woin_wallets"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_wallets"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_movements" (
"id" serial NOT NULL,
"amount" decimal(17,2) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"transaction_id" integer NOT NULL,
"wallet_id" integer NOT NULL,
"current_balance" decimal(17,2) NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_woin_movements_transaction_id_type" UNIQUE ("type", "transaction_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_movements" IS 'Tabla que contiene los movimientos generados por transacciones, estos movimientos actualizan el balance de las billteras';
COMMENT ON COLUMN "woin_movements"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_movements"."amount" IS 'Dinero total del movimiento';
COMMENT ON COLUMN "woin_movements"."state" IS 'Estado del movimiento (visible, no visible)';
COMMENT ON COLUMN "woin_movements"."type" IS 'Tipo del movimiento (ingreso, egreso)';
COMMENT ON COLUMN "woin_movements"."transaction_id" IS 'Transacción a la que hace referencia el movimiento';
COMMENT ON COLUMN "woin_movements"."wallet_id" IS 'Billetera a la que pertenece el movimiento';
COMMENT ON COLUMN "woin_movements"."current_balance" IS 'Balance actual de la billetera luego del movimiento';
COMMENT ON COLUMN "woin_movements"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_movements"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_transactions" (
"id" serial NOT NULL,
"amount" decimal(17,2) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"type" smallint NOT NULL DEFAULT 0,
"detail" text NOT NULL,
"wallet_sender" integer NOT NULL,
"wallet_receiver" integer NOT NULL,
"session_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_transactions" IS 'Tabla que contiene las transacciones hechas por woiners (internas, transferencias, compras, regalos, recargas, comisiones)';
COMMENT ON COLUMN "woin_transactions"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_transactions"."amount" IS 'Monto o cantidad de puntos de la transacción';
COMMENT ON COLUMN "woin_transactions"."state" IS 'Estado de la transacción (espera, correcta, rechazada, errónea)';
COMMENT ON COLUMN "woin_transactions"."type" IS 'Tipo de la transacción (interna, transferencia, compra, etc)';
COMMENT ON COLUMN "woin_transactions"."detail" IS 'Detalle, descripción o cuerpo de la transacción';
COMMENT ON COLUMN "woin_transactions"."wallet_sender" IS 'Billetera que envía los puntos en la transacción';
COMMENT ON COLUMN "woin_transactions"."wallet_receiver" IS 'Billetera que recibe los puntos en la transacción';
COMMENT ON COLUMN "woin_transactions"."session_id" IS 'Usuario que ejecuta la acción';
COMMENT ON COLUMN "woin_transactions"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_transactions"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_recharges" (
"id" serial NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"transaction_id" integer NOT NULL,
"agreement_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_recharges" IS 'Tabla que contiene las recargas hechas por woiners en el sistema';
COMMENT ON COLUMN "woin_recharges"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_recharges"."state" IS 'Estado de la recarga (error, espera, correcto, etc)';
COMMENT ON COLUMN "woin_recharges"."transaction_id" IS 'Transacción generada por la recarga';
COMMENT ON COLUMN "woin_recharges"."agreement_id" IS 'Convenio al que hace referencia la recarga';
COMMENT ON COLUMN "woin_recharges"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_recharges"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_agreements" (
"id" serial NOT NULL,
"number" varchar(20) NOT NULL,
"name" varchar(100) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_agreements" IS 'Tabla que contiene los convenios que se usarán para transacciones que usen medios externos o internos';
COMMENT ON COLUMN "woin_agreements"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_agreements"."number" IS 'Número del convenio';
COMMENT ON COLUMN "woin_agreements"."name" IS 'Nombre del convenio (recargas y procesos externos)';
COMMENT ON COLUMN "woin_agreements"."state" IS 'Estado del convenio (activo, inactivo, etc)';
COMMENT ON COLUMN "woin_agreements"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_agreements"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_emails_gifts" (
"id" serial NOT NULL,
"email" varchar(50) NOT NULL,
"amount" decimal(17,2) NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"woiner_id" integer NOT NULL,
"transaction_id" integer,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_emails_gifts" IS 'Tabla que contiene los regalos a personas no registradas (usando sus emails)';
COMMENT ON COLUMN "woin_emails_gifts"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_emails_gifts"."email" IS 'Correo electrónico al que se envía el regalo';
COMMENT ON COLUMN "woin_emails_gifts"."amount" IS 'Monto a regalar';
COMMENT ON COLUMN "woin_emails_gifts"."state" IS 'Estado del regalo (aceptado, erróneo,  vencido, etc)';
COMMENT ON COLUMN "woin_emails_gifts"."woiner_id" IS 'Woiner que hace el regalo';
COMMENT ON COLUMN "woin_emails_gifts"."transaction_id" IS 'Transacción generada por el regalo';
COMMENT ON COLUMN "woin_emails_gifts"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_emails_gifts"."updated_at" IS 'Fecha de última de modificación del registro';

CREATE TABLE "woin_gifts" (
"id" serial NOT NULL,
"amount" decimal(17,2) NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"transactions" integer[] NOT NULL,
"wallet_sender" integer NOT NULL,
"wallet_receiver" integer NOT NULL,
"user_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_gifts" IS 'Tabla que contiene los regalos hechos entre los woiners para producir comisiones entre las redes de estos';
COMMENT ON COLUMN "woin_gifts"."id" IS 'Registro único del registro';
COMMENT ON COLUMN "woin_gifts"."amount" IS 'Monto que será repartido por la red de ambos woiners';
COMMENT ON COLUMN "woin_gifts"."state" IS 'Estado del regalo (espera, correcto, incorrecto)';
COMMENT ON COLUMN "woin_gifts"."transactions" IS 'Transacciones generadas por el regalo';
COMMENT ON COLUMN "woin_gifts"."wallet_sender" IS 'Biiletera que envía el regalo';
COMMENT ON COLUMN "woin_gifts"."wallet_receiver" IS 'Wallet que recibe el regalo';
COMMENT ON COLUMN "woin_gifts"."user_id" IS 'Usuario que ejecuta la acción';
COMMENT ON COLUMN "woin_gifts"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_gifts"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_woiners_networks" (
"id" serial NOT NULL,
"level" smallint NOT NULL,
"type" smallint NOT NULL,
"gift_percentage" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
CONSTRAINT "uq_woin_woiners_networks_type_level" UNIQUE ("level", "type")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_woiners_networks" IS 'Tabla que contiene la configuración de la red de woiners';
COMMENT ON COLUMN "woin_woiners_networks"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_woiners_networks"."level" IS 'Nivel en la red';
COMMENT ON COLUMN "woin_woiners_networks"."type" IS 'Tipo de nivel (vendedor, comprador)';
COMMENT ON COLUMN "woin_woiners_networks"."gift_percentage" IS 'Porcentaje de regalo (1 a 100)';
COMMENT ON COLUMN "woin_woiners_networks"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_woiners_networks"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_woin_woiners" (
"id" serial NOT NULL,
"woiner_id" integer NOT NULL,
"country_id" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_woin_woiners" IS 'Tabla que contiene los woiners representantes por país [Woin Colombia, Woin Perú, etc]';
COMMENT ON COLUMN "woin_woin_woiners"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_woin_woiners"."woiner_id" IS 'Representante de woin por país';
COMMENT ON COLUMN "woin_woin_woiners"."country_id" IS 'País en el que está ubicado el representante';
COMMENT ON COLUMN "woin_woin_woiners"."state" IS 'Estado del representante del país';

CREATE TABLE "woin_dynamics_keys" (
"id" serial NOT NULL,
"dynamic_key" integer NOT NULL,
"state" smallint NOT NULL DEFAULT 1,
"woiner_id" integer NOT NULL,
"transaction_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "woin_dynamics_keys"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_dynamics_keys"."dynamic_key" IS 'Clave dinámica para validar la transacción';
COMMENT ON COLUMN "woin_dynamics_keys"."state" IS 'Estado de la clave dinámica [use, active, ...]';

CREATE TABLE "woin_transactions_transactions" (
"id" serial NOT NULL,
"parent_id" integer NOT NULL,
"child_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_woin_transactions_transactions_child_id" UNIQUE ("child_id")
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_transactions_transactions" IS 'Tabla que contiene las transacciones padres con sus respectivos hijos';
COMMENT ON COLUMN "woin_transactions_transactions"."parent_id" IS 'Transacción padre';
COMMENT ON COLUMN "woin_transactions_transactions"."child_id" IS 'Transacción generada por la transacción padre';
COMMENT ON COLUMN "woin_transactions_transactions"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_transactions_transactions"."updated_at" IS 'Última fecha de modificación del registro';

CREATE TABLE "woin_ads_indicators" (
"id" serial NOT NULL,
"ad_id" integer NOT NULL,
"rating" decimal(2,1) NOT NULL DEFAULT 0,
"
expected_views" integer NOT NULL DEFAULT 0,
"current_views" integer NOT NULL DEFAULT 0,
"views_percentage" decimal(6,3) NOT NULL DEFAULT 0,
"purchases" integer NOT NULL DEFAULT 0,
"purchases_percentage" decimal(6,3) NOT NULL DEFAULT 0,
"total_duration" bigint NOT NULL DEFAULT 0,
"remaining_duration" bigint NOT NULL DEFAULT 0,
"shared" int NOT NULL,
"likes" int NOT NULL DEFAULT 0,
"percentage_likes_views" decimal(6,3) NOT NULL DEFAULT 0,
"index_view" decimal(6,3) NOT NULL DEFAULT 0,
"index_preferred" decimal(6,3) NOT NULL DEFAULT 0,
"index_purchased" decimal(6,3) NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_ads_indicators" IS 'Tabla que contiene índices y variables para priorizar los anuncios con diferentes enfoques';
COMMENT ON COLUMN "woin_ads_indicators"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_ads_indicators"."ad_id" IS 'Anuncio al que pertenecen los indicadores';
COMMENT ON COLUMN "woin_ads_indicators"."rating" IS 'Calificación del anuncio';
COMMENT ON COLUMN "woin_ads_indicators"."
expected_views" IS 'Vistas que se esperan cumplir en el tiempo del anuncio';
COMMENT ON COLUMN "woin_ads_indicators"."current_views" IS 'Vistas actuales del anuncio';
COMMENT ON COLUMN "woin_ads_indicators"."views_percentage" IS 'Porcentaje de vistas (current/expected)';
COMMENT ON COLUMN "woin_ads_indicators"."purchases" IS 'Compras actuales del anuncio';
COMMENT ON COLUMN "woin_ads_indicators"."purchases_percentage" IS 'Porcentaje de compras respecto a las vistas (compras/vistas)';
COMMENT ON COLUMN "woin_ads_indicators"."total_duration" IS 'Duración total del anuncio en milisegundos';
COMMENT ON COLUMN "woin_ads_indicators"."remaining_duration" IS 'Duración restante del anuncio antes de caducar';
COMMENT ON COLUMN "woin_ads_indicators"."shared" IS 'Veces que se ha compartido el anuncio';
COMMENT ON COLUMN "woin_ads_indicators"."likes" IS 'Likes dados al anuncio';
COMMENT ON COLUMN "woin_ads_indicators"."percentage_likes_views" IS 'Porcentaje de likes respecto a las vistas del anuncio (likes/vistas)';
COMMENT ON COLUMN "woin_ads_indicators"."index_view" IS 'Índice que prioriza a los anuncios en riesgo de no cumplir las vistas esperadas';
COMMENT ON COLUMN "woin_ads_indicators"."index_preferred" IS 'Índice que prioriza a los anuncios que son preferidos por los woiners';
COMMENT ON COLUMN "woin_ads_indicators"."index_purchased" IS 'Índice que prioriza los anuncios que han sido más comprados y han ofrecido una mejor experiencia a los woiners';
COMMENT ON COLUMN "woin_ads_indicators"."created_at" IS 'Fecha de creacuión del registro';
COMMENT ON COLUMN "woin_ads_indicators"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_woiners_preferences" (
"id" serial NOT NULL,
"woiner_id" integer NOT NULL,
"subcategory_id" integer NOT NULL,
"priority" smallint NOT NULL DEFAULT 0,
"state" smallint NOT NULL DEFAULT 0,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "woin_woiners_preferences"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_woiners_preferences"."woiner_id" IS 'Woiner al que pertenece las preferencias';
COMMENT ON COLUMN "woin_woiners_preferences"."subcategory_id" IS 'Subcategoría a la que hace referencia la preferencia';
COMMENT ON COLUMN "woin_woiners_preferences"."priority" IS 'Prioridad de la preferencia (0 = principal, 1 = secundaria, etc)';
COMMENT ON COLUMN "woin_woiners_preferences"."state" IS 'Estado de la preferencia (activa, inactiva)';
COMMENT ON COLUMN "woin_woiners_preferences"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_woiners_preferences"."updated_at" IS 'Fecha de la última modificación del registro';

CREATE TABLE "woin_ads_views" (
"id" serial NOT NULL,
"woiner_id" integer NOT NULL,
"ad_id" integer NOT NULL,
"preferred" smallint NOT NULL DEFAULT 0,
"like" smallint NOT NULL DEFAULT 0,
"shared" smallint NOT NULL DEFAULT 0,
"created_at" integer NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" integer NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_ads_views" IS 'Tabla que contiene los anuncios que han visto los woiners para tener estadísticas y personalizar los anuncios mostrados al woiner';
COMMENT ON COLUMN "woin_ads_views"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_ads_views"."woiner_id" IS 'Woiner que vio la publicación o anuncio';
COMMENT ON COLUMN "woin_ads_views"."ad_id" IS 'Anuncio o publicación vista';
COMMENT ON COLUMN "woin_ads_views"."preferred" IS 'Estado que indica si esa vista es una preferencia del woiner';
COMMENT ON COLUMN "woin_ads_views"."like" IS 'Campo que dice si el woiner le dio o no like al anuncio';
COMMENT ON COLUMN "woin_ads_views"."shared" IS 'Campo que dice si el woiner compartió o no un anuncio';
COMMENT ON COLUMN "woin_ads_views"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_ads_views"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_ads_companies" (
"id" serial NOT NULL,
"ad_id" integer NOT NULL,
"company_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") ,
CONSTRAINT "uq_woin_ads_companies_companie_id" UNIQUE ("company_id")
)
WITHOUT OIDS;
COMMENT ON COLUMN "woin_ads_companies"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_ads_companies"."ad_id" IS 'Anuncio que pertenece a la compañía';
COMMENT ON COLUMN "woin_ads_companies"."company_id" IS 'Compañia que publica el anuncio';
COMMENT ON COLUMN "woin_ads_companies"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_ads_companies"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_resource_scope" (
"id" serial NOT NULL,
"country_id" integer,
"governorate_id" integer,
"city_id" integer,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_resource_scope" IS 'Tabla que contiene el alcance de un recurso del sistema (mundial, nacional, departamental, municipal)';
COMMENT ON COLUMN "woin_resource_scope"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_resource_scope"."country_id" IS 'País donde tendrá alcance el recurso (Si es null será un alcance mundial)';
COMMENT ON COLUMN "woin_resource_scope"."governorate_id" IS 'Gobrernación donde tendrá alcance el recurso (Si es null será un alcance nacional)';
COMMENT ON COLUMN "woin_resource_scope"."city_id" IS 'Ciudad o Municipio donde tendrá alcance el recurso (Si es null será un alcance departamental)';
COMMENT ON COLUMN "woin_resource_scope"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_resource_scope"."updated_at" IS 'Fecha de última actualización del registro';

CREATE TABLE "woin_products" (
"id" serial NOT NULL,
"name" varchar(200) NOT NULL,
"description" varchar(255),
"subcategory_id" integer NOT NULL,
"price" decimal(17,2) NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_products" IS 'Tabla que contiene los productos de la plataforma';
COMMENT ON COLUMN "woin_products"."id" IS 'Identificación única del registro';
COMMENT ON COLUMN "woin_products"."name" IS 'Nombre del producto/servicio/inmueble/transporte/inmueble';
COMMENT ON COLUMN "woin_products"."description" IS 'Descripción del producto';
COMMENT ON COLUMN "woin_products"."subcategory_id" IS 'Subcategoría a la que pertenece el producto';
COMMENT ON COLUMN "woin_products"."price" IS 'Precio del producto';
COMMENT ON COLUMN "woin_products"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_products"."updated_at" IS 'Fecha de última actualización del registro';

CREATE TABLE "woin_ads_products" (
"id" serial NOT NULL,
"ad_id" integer NOT NULL,
"prodct_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_ads_products" IS 'Tabla que contiene los productos promocionados en una publicación';
COMMENT ON COLUMN "woin_ads_products"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_ads_products"."ad_id" IS 'Anuncio al que hace referencia';
COMMENT ON COLUMN "woin_ads_products"."prodct_id" IS 'Producto que se publica';
COMMENT ON COLUMN "woin_ads_products"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_ads_products"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_users_roles" (
"id" serial NOT NULL,
"user_id" integer NOT NULL,
"role_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_users_roles" IS 'Tabla que contiene el usuario con cada uno de los roles relacionados a este';
COMMENT ON COLUMN "woin_users_roles"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_users_roles"."user_id" IS 'Usuario al que se hace referencia';
COMMENT ON COLUMN "woin_users_roles"."role_id" IS 'Rol que tiene el usuario';
COMMENT ON COLUMN "woin_users_roles"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_users_roles"."updated_at" IS 'Fecha de última actualización del registro';

CREATE TABLE "woin_user_groups" (
"id" serial NOT NULL,
"group_id" integer NOT NULL,
"user_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_user_groups" IS 'Tabla que contiene los usuarios pertenecientes a un grupo';
COMMENT ON COLUMN "woin_user_groups"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_user_groups"."group_id" IS 'Grupo al que pertenece el usuario';
COMMENT ON COLUMN "woin_user_groups"."user_id" IS 'Usuario al que se hace referencia';
COMMENT ON COLUMN "woin_user_groups"."created_at" IS 'Fecha de creación del registro';
COMMENT ON COLUMN "woin_user_groups"."updated_at" IS 'Fecha de última modificación del registro';

CREATE TABLE "woin_groups" (
"id" serial NOT NULL,
"name" varchar(45) NOT NULL,
"role_id" integer NOT NULL,
"created_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
"updated_at" bigint NOT NULL DEFAULT date_part('epoch', TRANSACTION_TIMESTAMP())::BIGINT*1000,
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON TABLE "woin_groups" IS 'Tabla que contiene los grupos de usuarios por rol';
COMMENT ON COLUMN "woin_groups"."id" IS 'Identificador único del registro';
COMMENT ON COLUMN "woin_groups"."name" IS 'Nombre del grupo';
COMMENT ON COLUMN "woin_groups"."role_id" IS 'Rol que está relacionado al grupo de usuarios';
COMMENT ON COLUMN "woin_groups"."created_at" IS 'Fecha de creacion del registro';
COMMENT ON COLUMN "woin_groups"."updated_at" IS 'Fecha de ultima actualizacion del registro';


ALTER TABLE "woin_documents" ADD CONSTRAINT "fk_woin_documents_woin_persons_1" FOREIGN KEY ("person_id") REFERENCES "woin_persons" ("id");
ALTER TABLE "woin_woiners" ADD CONSTRAINT "fk_woin_woiners_woin_persons_1" FOREIGN KEY ("person_id") REFERENCES "woin_persons" ("id");
ALTER TABLE "woin_woiners" ADD CONSTRAINT "fk_woin_woiners_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "woin_sessions" ADD CONSTRAINT "fk_woin_sessions_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "devices" ADD CONSTRAINT "fk_devices_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "woin_sessions" ADD CONSTRAINT "fk_woin_sessions_devices_1" FOREIGN KEY ("device_id") REFERENCES "devices" ("id");
ALTER TABLE "woin_documents" ADD CONSTRAINT "fk_woin_documents_cities_1" FOREIGN KEY ("city_id") REFERENCES "cities" ("id");
ALTER TABLE "cities" ADD CONSTRAINT "fk_cities_governorates_1" FOREIGN KEY ("governorate_id") REFERENCES "governorates" ("id");
ALTER TABLE "governorates" ADD CONSTRAINT "fk_governorates_countries_1" FOREIGN KEY ("country_id") REFERENCES "countries" ("id");
ALTER TABLE "phones" ADD CONSTRAINT "fk_phones_countries_1" FOREIGN KEY ("country_id") REFERENCES "countries" ("id");
ALTER TABLE "woin_companies_phones" ADD CONSTRAINT "fk_woin_companies_phones_phones_1" FOREIGN KEY ("phone_id") REFERENCES "phones" ("id");
ALTER TABLE "woin_companies_phones" ADD CONSTRAINT "fk_woin_companies_phones_woin_companies_1" FOREIGN KEY ("company_id") REFERENCES "woin_companies" ("id");
ALTER TABLE "woin_roles_permissions" ADD CONSTRAINT "fk_woin_roles_permissions_woin_permissions_1" FOREIGN KEY ("permission_id") REFERENCES "woin_permissions" ("id");
ALTER TABLE "woin_roles_permissions" ADD CONSTRAINT "fk_woin_roles_permissions_woin_roles_1" FOREIGN KEY ("role_id") REFERENCES "woin_roles" ("id");
ALTER TABLE "woin_visitors" ADD CONSTRAINT "fk_woin_visitors_woin_roles_1" FOREIGN KEY ("role_id") REFERENCES "woin_roles" ("id");
ALTER TABLE "woin_sessions" ADD CONSTRAINT "fk_woin_sessions_woin_locations_1" FOREIGN KEY ("location_id") REFERENCES "woin_locations" ("id");
ALTER TABLE "woin_companies" ADD CONSTRAINT "fk_woin_companies_woin_locations_1" FOREIGN KEY ("location_id") REFERENCES "woin_locations" ("id");
ALTER TABLE "woin_offices" ADD CONSTRAINT "fk_woin_offices_woin_locations_1" FOREIGN KEY ("location_id") REFERENCES "woin_locations" ("id");
ALTER TABLE "woin_offices" ADD CONSTRAINT "fk_woin_offices_woin_companies_1" FOREIGN KEY ("company_id") REFERENCES "woin_companies" ("id");
ALTER TABLE "woin_paids_ads" ADD CONSTRAINT "fk_woin_paids_ads_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_companies" ADD CONSTRAINT "fk_woin_companies_woin_emwoiners_1" FOREIGN KEY ("emwoiner_id") REFERENCES "woin_emwoiners" ("id");
ALTER TABLE "woin_frees_ads" ADD CONSTRAINT "fk_woin_frees_ads_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_cliwoiners" ADD CONSTRAINT "fk_woin_cliwoiners_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_emwoiners" ADD CONSTRAINT "fk_woin_emwoiners_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_ads" ADD CONSTRAINT "fk_woin_ads_woin_ads_1" FOREIGN KEY ("ad_parent") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_ads" ADD CONSTRAINT "fk_woin_ads_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_paids_ads" ADD CONSTRAINT "fk_woin_paids_ads_woin_ads_packets_1" FOREIGN KEY ("packet_id") REFERENCES "woin_ads_packets" ("id");
ALTER TABLE "woin_online_purchases" ADD CONSTRAINT "fk_woin_online_purchases_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_multimedias_ads" ADD CONSTRAINT "fk_woin_multimedias_ads_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_multimedias_ads" ADD CONSTRAINT "fk_woin_multimedias_ads_woin_multimedias_1" FOREIGN KEY ("multimedia_id") REFERENCES "woin_multimedias" ("id");
ALTER TABLE "woin_califications" ADD CONSTRAINT "fk_woin_califications_woin_woiners_1" FOREIGN KEY ("woiner_sender_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_califications" ADD CONSTRAINT "fk_woin_califications_woin_woiners_2" FOREIGN KEY ("woiner_receiver_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_multimedias_categories" ADD CONSTRAINT "fk_woin_multimedias_categories_woin_multimedias_1" FOREIGN KEY ("multmedia_id") REFERENCES "woin_multimedias" ("id");
ALTER TABLE "woin_multimedias_categories" ADD CONSTRAINT "fk_woin_multimedias_categories_woin_categories_1" FOREIGN KEY ("category_id") REFERENCES "woin_categories" ("id");
ALTER TABLE "woin_face_to_face_purchases" ADD CONSTRAINT "fk_woin_face_to_face_purchases_woin_purchases_1" FOREIGN KEY ("purchase_id") REFERENCES "woin_purchases" ("id");
ALTER TABLE "woin_subcategories" ADD CONSTRAINT "fk_woin_subcategories_woin_categories_1" FOREIGN KEY ("category_id") REFERENCES "woin_categories" ("id");
ALTER TABLE "woin_columns_subcategories" ADD CONSTRAINT "fk_woin_columns_subcategories_woin_subcategories_1" FOREIGN KEY ("subcategory_id") REFERENCES "woin_subcategories" ("id");
ALTER TABLE "woin_social_networks" ADD CONSTRAINT "fk_woin_social_networks_woin_multimedias_1" FOREIGN KEY ("multimedia_id") REFERENCES "woin_multimedias" ("id");
ALTER TABLE "woin_social_profiles" ADD CONSTRAINT "fk_woin_social_profiles_woin_social_networks_1" FOREIGN KEY ("social_network_id") REFERENCES "woin_social_networks" ("id");
ALTER TABLE "woin_frees_ads" ADD CONSTRAINT "fk_woin_frees_ads_woin_ads_packets_1" FOREIGN KEY ("packet_id") REFERENCES "woin_ads_packets" ("id");
ALTER TABLE "woin_woiners_configs" ADD CONSTRAINT "fk_woin_woiner_configs_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_pay_for_view_views" ADD CONSTRAINT "fk_woin_pay_for_view_views_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_pay_for_view_views" ADD CONSTRAINT "fk_woin_pay_for_view_views_woin_pay_for_view_1" FOREIGN KEY ("pay_for_view_id") REFERENCES "woin_pay_for_views" ("id");
ALTER TABLE "woin_wallets" ADD CONSTRAINT "fk_woin_wallets_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_wallets" ADD CONSTRAINT "fk_woin_wallets_woin_wallets_1" FOREIGN KEY ("wallet_parent") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_movements" ADD CONSTRAINT "fk_woin_movements_woin_wallets_1" FOREIGN KEY ("wallet_id") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_movements" ADD CONSTRAINT "fk_woin_movements_woin_transactions_1" FOREIGN KEY ("transaction_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_recharges" ADD CONSTRAINT "fk_woin_recharges_woin_movements_1" FOREIGN KEY ("transaction_id") REFERENCES "woin_movements" ("id");
ALTER TABLE "woin_recharges" ADD CONSTRAINT "fk_woin_recharges_woin_agreements_1" FOREIGN KEY ("agreement_id") REFERENCES "woin_agreements" ("id");
ALTER TABLE "woin_pay_for_views" ADD CONSTRAINT "fk_woin_pay_for_view_woin_paids_ads_1" FOREIGN KEY ("paid_id") REFERENCES "woin_paids_ads" ("id");
ALTER TABLE "woin_social_profiles" ADD CONSTRAINT "fk_woin_social_profiles_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_columns_values" ADD CONSTRAINT "fk_woin_columns_values_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_columns_values" ADD CONSTRAINT "fk_woin_columns_values_woin_columns_1" FOREIGN KEY ("column_id") REFERENCES "woin_columns" ("id");
ALTER TABLE "woin_columns_subcategories" ADD CONSTRAINT "fk_woin_columns_subcategories_woin_columns_1" FOREIGN KEY ("column_id") REFERENCES "woin_columns" ("id");
ALTER TABLE "woin_visitors" ADD CONSTRAINT "fk_woin_visitors_woin_locations_1" FOREIGN KEY ("location_id") REFERENCES "woin_locations" ("id");
ALTER TABLE "woin_woiners" ADD CONSTRAINT "fk_woin_woiners_woin_multimedias_1" FOREIGN KEY ("profile_id") REFERENCES "woin_multimedias" ("id");
ALTER TABLE "woin_transactions" ADD CONSTRAINT "fk_woin_transactions_woin_wallets_1" FOREIGN KEY ("wallet_sender") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_transactions" ADD CONSTRAINT "fk_woin_transactions_woin_wallets_2" FOREIGN KEY ("wallet_receiver") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_persons" ADD CONSTRAINT "fk_woin_persons_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "woin_woiners" ADD CONSTRAINT "fk_woin_woiners_woin_roles_1" FOREIGN KEY ("role_id") REFERENCES "woin_roles" ("id");
ALTER TABLE "woin_emails_gifts" ADD CONSTRAINT "fk_woin_emails_gifts_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_emails_gifts" ADD CONSTRAINT "fk_woin_emails_gifts_woin_transactions_1" FOREIGN KEY ("transaction_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_paids_ads" ADD CONSTRAINT "fk_woin_paids_ads_woin_transactions_1" FOREIGN KEY ("transaction_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_offices" ADD CONSTRAINT "fk_woin_offices_woin_wallets_1" FOREIGN KEY ("wallet_id") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_purchases" ADD CONSTRAINT "fk_woin_purchases_woin_transactions_1" FOREIGN KEY ("transaction_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_purchases" ADD CONSTRAINT "fk_woin_purchases_woin_woiners_1" FOREIGN KEY ("purchaser_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_face_to_face_purchases" ADD CONSTRAINT "fk_woin_face_to_face_purchases_woin_woiners_1" FOREIGN KEY ("vendor_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_online_purchases" ADD CONSTRAINT "fk_woin_online_purchases_woin_purchases_1" FOREIGN KEY ("purchase_id") REFERENCES "woin_purchases" ("id");
ALTER TABLE "woin_gifts" ADD CONSTRAINT "fk_woin_gifts_woin_woiners_1" FOREIGN KEY ("wallet_sender") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_gifts" ADD CONSTRAINT "fk_woin_gifts_woin_woiners_2" FOREIGN KEY ("wallet_receiver") REFERENCES "woin_wallets" ("id");
ALTER TABLE "woin_woiners" ADD CONSTRAINT "fk_woin_woiners_woin_woiners_1" FOREIGN KEY ("reference") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_woin_woiners" ADD CONSTRAINT "fk_woin_woin_woiners_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_woin_woiners" ADD CONSTRAINT "fk_woin_woin_woiners_countries_1" FOREIGN KEY ("country_id") REFERENCES "countries" ("id");
ALTER TABLE "woin_wallets" ADD CONSTRAINT "fk_woin_wallets_countries_1" FOREIGN KEY ("country_id") REFERENCES "countries" ("id");
ALTER TABLE "woin_gifts" ADD CONSTRAINT "fk_woin_gifts_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "woin_dynamics_keys" ADD CONSTRAINT "fk_woin_dynamics_keys_woin_transactions_1" FOREIGN KEY ("transaction_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_dynamics_keys" ADD CONSTRAINT "fk_woin_dynamics_keys_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_transactions_transactions" ADD CONSTRAINT "fk_woin_transactions_transactions_woin_transactions_1" FOREIGN KEY ("parent_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_transactions_transactions" ADD CONSTRAINT "fk_woin_transactions_transactions_woin_transactions_2" FOREIGN KEY ("child_id") REFERENCES "woin_transactions" ("id");
ALTER TABLE "woin_purchases" ADD CONSTRAINT "fk_woin_purchases_woin_woiners_2" FOREIGN KEY ("vendor_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_ads_indicators" ADD CONSTRAINT "fk_woin_ads_priorizations_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_ads" ADD CONSTRAINT "fk_woin_ads_woin_subcategories_1" FOREIGN KEY ("subcategory_id") REFERENCES "woin_subcategories" ("id");
ALTER TABLE "woin_woiners_preferences" ADD CONSTRAINT "fk_woin_woiners_preferences_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_woiners_preferences" ADD CONSTRAINT "fk_woin_woiners_preferences_woin_subcategories_1" FOREIGN KEY ("subcategory_id") REFERENCES "woin_subcategories" ("id");
ALTER TABLE "woin_ads_views" ADD CONSTRAINT "fk_woin_ads_views_woin_woiners_1" FOREIGN KEY ("woiner_id") REFERENCES "woin_woiners" ("id");
ALTER TABLE "woin_ads_views" ADD CONSTRAINT "fk_woin_ads_views_woin_ads_indicators_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_ads_companies" ADD CONSTRAINT "fk_woin_ads_companies_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_ads_companies" ADD CONSTRAINT "fk_woin_ads_companies_woin_companies_1" FOREIGN KEY ("company_id") REFERENCES "woin_companies" ("id");
ALTER TABLE "woin_subcategories" ADD CONSTRAINT "fk_woin_subcategories_woin_subcategories_1" FOREIGN KEY ("parent_id") REFERENCES "woin_subcategories" ("id");
ALTER TABLE "woin_products" ADD CONSTRAINT "fk_woin_products_woin_subcategories_1" FOREIGN KEY ("subcategory_id") REFERENCES "woin_subcategories" ("id");
ALTER TABLE "woin_ads_products" ADD CONSTRAINT "fk_woin_ads_products_woin_ads_1" FOREIGN KEY ("ad_id") REFERENCES "woin_ads" ("id");
ALTER TABLE "woin_ads_products" ADD CONSTRAINT "fk_woin_ads_products_woin_products_1" FOREIGN KEY ("prodct_id") REFERENCES "woin_products" ("id");
ALTER TABLE "woin_users_roles" ADD CONSTRAINT "fk_woin_users_roles_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "woin_users_roles" ADD CONSTRAINT "fk_woin_users_roles_woin_roles_1" FOREIGN KEY ("role_id") REFERENCES "woin_roles" ("id");
ALTER TABLE "woin_user_groups" ADD CONSTRAINT "fk_woin_user_groups_woin_users_1" FOREIGN KEY ("user_id") REFERENCES "woin_users" ("id");
ALTER TABLE "woin_user_groups" ADD CONSTRAINT "fk_woin_user_groups_woin_groups_1" FOREIGN KEY ("group_id") REFERENCES "woin_groups" ("id");
ALTER TABLE "woin_groups" ADD CONSTRAINT "fk_woin_groups_woin_roles_1" FOREIGN KEY ("role_id") REFERENCES "woin_roles" ("id");
ALTER TABLE "woin_transactions" ADD CONSTRAINT "fk_woin_transactions_woin_sessions_1" FOREIGN KEY ("session_id") REFERENCES "woin_sessions" ("id");

