﻿using Application.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Interface
{
    interface IController<T> where T : class
    {
        public ActionResult<IResponse<T>> Get(int id);
        
        public ActionResult<IResponse<T>> Edit(int id, T entity);

        public ActionResult<IResponse<T>> Add(T entity);

        public ActionResult<IResponse<T>> Del(int id);

        public ActionResult<IResponse<T>> GetAll();

        

    }
}
