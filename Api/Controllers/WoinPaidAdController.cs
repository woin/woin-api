/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name WoinPaidAdController
 * @file Api/Controller/
 * @observations use Controller Poryect
 * @HU 0: Controller
 * @task 10 Crear Controller
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Infrastructure.Data;
using Application.Interface;
using Microsoft.Extensions.DependencyInjection;
using Application.Base;
using Api.Interface;
using Application.Services;
using Infrastructure.Data.Interface;
using Domain.Interface;
using Infrastructure.UnitsOfWorks;
using Infrastructure.Data.Contract;

namespace Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class WoinPaidAdController : ControllerBase, IController<WoinPaidAd>
	{
		private readonly IWoinPaidAdService _service;
		private IResponse<WoinPaidAd> _response;


		public WoinPaidAdController(WoinContext context)
		{
			_service = new WoinPaidAdService(new UnitOfWork(context), new WoinPaidAdContract(context));
			_response = new Response<WoinPaidAd>
			{
				Message = "Entity is Empty"
			};

		}


		/// <summary>
		/// Creates a WoinPaidAd.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     POST /Todo
		///     {
		///     }
		///
		/// </remarks>
		/// <param name="entity"></param>
		/// <returns>A newly created WoinPaidAd</returns>
		/// <response code="201">Return the newly created WoinPaidAd</response>
		/// <response code="400">If the item is null</response>            
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		[HttpPost]
		public ActionResult<IResponse<WoinPaidAd>> Add(WoinPaidAd entity)
		{
			try
			{
				if (entity == null)
				{
					return BadRequest(_response);
				}
				_response = _service.Create(entity);
				_response.Entity = entity.ToObject();
				return Ok(_response);
			}
			catch(Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}

		/// <summary>
		/// Delete a WoinPaidAd.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     DELETE /Todo
		///     {  
		///     }
		///
		/// </remarks>
		/// <param name="id"></param>
		/// <returns>Returns Boolean Delete WoinPaidAd</returns>
		/// <response code="201">Returns Boolean Delete WoinPaidAd</response>
		/// <response code="400">If the item is null</response>            
		[HttpDelete("{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinPaidAd>> Del(int id)
		{
			try
			{
				if (id == 0)
				{
					return BadRequest(_response);
				}
				_response = _service.Delete(id);
				return Ok(_response);
			}
			catch (Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}


		/// <summary>
		/// Edit a WoinPaidAd.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     PUT /Todo
		///     {   
		///     }
		///
		/// </remarks>
		/// <param name="id"></param>
		/// <param name="entity"></param>
		/// <returns>A Edit  WoinPaidAd</returns>
		/// <response code="201">Return A Edit WoinPaidAd</response>
		/// <response code="400">If the item is null</response>            
		[HttpPut("{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinPaidAd>> Edit(int id, WoinPaidAd entity)
		{
			try
			{
				if (id != entity.Id)
				{
					return BadRequest(_response);
				}
				_response = _service.Update(entity);
				return Ok(_response);
			}catch(Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}

  
		/// <summary>
		/// Get a WoinPaidAd.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     GET /Todo
		///     {    
		///     }
		///
		/// </remarks>
		/// <param name="id"></param>
		/// <returns>Return A WoinPaidAd</returns>
		/// <response code="201">Return A WoinPaidAd</response>
		/// <response code="400">If the item is null</response>            
		[HttpGet("{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinPaidAd>> Get(int id)
		{
			try
			{
				if (id == 0)
				{
					return BadRequest(_response);
				}
				_response = _service.Find(id);
				_response.Entity = _response.Entity.ToObject();
				return Ok(_response);
			}
			catch (Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}


		/// <summary>
		/// Get all Entities.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     GET /Todo
		///     { 
		///     }
		///
		/// </remarks>
		/// <returns>Returns All items</returns>
		/// <response code="201">Returns All items</response>
		/// <response code="400">If the item is null</response>            
		[HttpGet]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinPaidAd>> GetAll()
		{
			try
			{
				_response.Message = "OK";
				_response.Entities = _service.GetAll();
				return Ok(_response);
			}
			catch (Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}

		private bool Exists(int id)
		{
			return _service.Any(x => x.Id == id);
		}

	}
}

