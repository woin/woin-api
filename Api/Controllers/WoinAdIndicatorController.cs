/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name WoinAdIndicatorController
 * @file Api/Controller/
 * @observations use Controller Poryect
 * @HU 0: Controller
 * @task 10 Crear Controller
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Infrastructure.Data;
using Application.Interface;
using Microsoft.Extensions.DependencyInjection;
using Application.Base;
using Api.Interface;
using Application.Services;
using Infrastructure.Data.Interface;
using Domain.Interface;
using Infrastructure.UnitsOfWorks;
using Infrastructure.Data.Contract;

namespace Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class WoinAdIndicatorController : ControllerBase, IController<WoinAdIndicator>
	{
		private readonly IWoinAdIndicatorService _service;
		private IResponse<WoinAdIndicator> _response;


		public WoinAdIndicatorController(WoinContext context)
		{
			_service = new WoinAdIndicatorService(new UnitOfWork(context), new WoinAdIndicatorContract(context));
			_response = new Response<WoinAdIndicator>
			{
				Message = "Entity is Empty"
			};

		}


		/// <summary>
		/// Creates a WoinAdIndicator.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     POST /Todo
		///     {
		///     }
		///
		/// </remarks>
		/// <param name="entity"></param>
		/// <returns>A newly created WoinAdIndicator</returns>
		/// <response code="201">Return the newly created WoinAdIndicator</response>
		/// <response code="400">If the item is null</response>            
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		[HttpPost]
		public ActionResult<IResponse<WoinAdIndicator>> Add(WoinAdIndicator entity)
		{
			try
			{
				if (entity == null)
				{
					return BadRequest(_response);
				}
				_response = _service.Create(entity);
				_response.Entity = entity.ToObject();
				return Ok(_response);
			}
			catch(Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}

		/// <summary>
		/// Delete a WoinAdIndicator.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     DELETE /Todo
		///     {  
		///     }
		///
		/// </remarks>
		/// <param name="id"></param>
		/// <returns>Returns Boolean Delete WoinAdIndicator</returns>
		/// <response code="201">Returns Boolean Delete WoinAdIndicator</response>
		/// <response code="400">If the item is null</response>            
		[HttpDelete("{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinAdIndicator>> Del(int id)
		{
			try
			{
				if (id == 0)
				{
					return BadRequest(_response);
				}
				_response = _service.Delete(id);
				return Ok(_response);
			}
			catch (Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}


		/// <summary>
		/// Edit a WoinAdIndicator.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     PUT /Todo
		///     {   
		///     }
		///
		/// </remarks>
		/// <param name="id"></param>
		/// <param name="entity"></param>
		/// <returns>A Edit  WoinAdIndicator</returns>
		/// <response code="201">Return A Edit WoinAdIndicator</response>
		/// <response code="400">If the item is null</response>            
		[HttpPut("{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinAdIndicator>> Edit(int id, WoinAdIndicator entity)
		{
			try
			{
				if (id != entity.Id)
				{
					return BadRequest(_response);
				}
				_response = _service.Update(entity);
				return Ok(_response);
			}catch(Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}

  
		/// <summary>
		/// Get a WoinAdIndicator.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     GET /Todo
		///     {    
		///     }
		///
		/// </remarks>
		/// <param name="id"></param>
		/// <returns>Return A WoinAdIndicator</returns>
		/// <response code="201">Return A WoinAdIndicator</response>
		/// <response code="400">If the item is null</response>            
		[HttpGet("{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinAdIndicator>> Get(int id)
		{
			try
			{
				if (id == 0)
				{
					return BadRequest(_response);
				}
				_response = _service.Find(id);
				_response.Entity = _response.Entity.ToObject();
				return Ok(_response);
			}
			catch (Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}


		/// <summary>
		/// Get all Entities.
		/// </summary>
		/// <remarks>
		/// Sample request:
		///
		///     GET /Todo
		///     { 
		///     }
		///
		/// </remarks>
		/// <returns>Returns All items</returns>
		/// <response code="201">Returns All items</response>
		/// <response code="400">If the item is null</response>            
		[HttpGet]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[Produces("application/json")]
		public ActionResult<IResponse<WoinAdIndicator>> GetAll()
		{
			try
			{
				_response.Message = "OK";
				_response.Entities = _service.GetAll();
				return Ok(_response);
			}
			catch (Exception e)
			{
				_response.Message = e.Message;
				return BadRequest(_response);
			}
		}

		private bool Exists(int id)
		{
			return _service.Any(x => x.Id == id);
		}

	}
}

