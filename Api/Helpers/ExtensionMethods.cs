﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Controllers
 * @file Helpers/ExtensionMethods
 * @observations use abstractions for Entities
 * @HU 0: Dominio
 * @Task 9: Crear Sistema de Seguridad
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public static class ExtensionMethods
{
   
}