﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


public class AllowedExtensionsAttribute : ValidationAttribute
{
    private readonly string[] _Extensions;
    public AllowedExtensionsAttribute(string[] Extensions)
    {
        _Extensions = Extensions;
    }

    protected override ValidationResult IsValid(
    object value, ValidationContext validationContext)
    {
        IFormFile file = value as IFormFile;
        if(file == null)
        {
            return new ValidationResult(GetErrorMessage());
        }
        var extension = Path.GetExtension(file.FileName);
        if (!(file == null))
        {
            if (!_Extensions.Contains(extension.ToLower()))
            {
                return new ValidationResult(GetErrorMessage());
            }
        }

        return ValidationResult.Success;
    }

    public string GetErrorMessage()
    {
        return $"This File extension is not allowed!";
    }
}