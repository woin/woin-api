﻿using System;
using System.Collections.Generic;
using System.Text;

public static class MessageCodeException
{
    public static String MessageCode(Exception e)
    {
        var code = e.ToString().Trim().Split("SqlState")[1].Trim().Split("MessageText")[0].ToString().Trim().Split("\r\n")[0].Trim().Split(": ")[1].Trim().ToString();
        switch (code)
        {
            case "23505":
                {
                    return "Error: 23505\n  Message:  llave duplicada viola restricción de unicidad";
                }
        }
        return code;
    }
}
