﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Iservice
 * @file Base/Interface
 * @observations use Base Service for Applications
 * @HU 0: Dominio
 * @task 7 Crear Servicios genericos
 */


using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interface
{
    public interface IService<T>  where T : class
    {
        public IResponse<T> Find(object id);
        public IResponse<T> Create(T entity);
        public IResponse<T> Delete(T entity);
        public IResponse<T> Delete(object id);
        public IResponse<T> Update(T entity);
        public IEnumerable<T> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);
        public Task<IResponse<T>> FindAsync(object id);
        public Task<IResponse<T>> CreateAsync(T entity);
        public Task<IResponse<T>> DeleteAsync(T entity);
        public Task<IResponse<T>> DeleteAsync(object id);
        public Task<IResponse<T>> UpdateAsync(T entity);
        public IEnumerable<T> FromSqlRaw(string query, string includeProperties = "", int pageIndex = 0, int pageSize = int.MaxValue, params object[] parameters);
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        public IEnumerable<T> FindBy(
            Expression<Func<T, bool>> filter = null,
            int pageIndex = 0, int pageSize = int.MaxValue
        );

        public bool Any(Expression<Func<T, bool>> predicate);
    }
}
