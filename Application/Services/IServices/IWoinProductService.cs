/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name IWoinProductService
 * @file Application/Services/
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Crear Servicio
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Application.Base;
using Infrastructure.Data.Repositories;
using Infrastructure.Data.Contract;
using Application.Interface;

namespace Application.Services
{

	public interface IWoinProductService : IService<WoinProduct>
	{
	}
}

