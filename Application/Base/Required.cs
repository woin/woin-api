﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Service
 * @file Base/Required
 * @observations use Base Service for Applications
 * @HU 0: Dominio
 * @task 7 Crear Servicios genericos
 */


using System.ComponentModel.DataAnnotations;

public static class Required
{
    public static bool ValidateString(string stringName, int min = 4 , int max = 12)
    {
       
        if (stringName == null) return false;
        return stringName.Length >= min && stringName.Length <= max;
    }

    public static bool IsValidEmail(string email)
    {
        if (email == null) return false;
        return new EmailAddressAttribute().IsValid(email);

    }
}
