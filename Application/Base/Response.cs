﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Service
 * @file Base/Response
 * @observations use Base Service for Applications
 * @HU 0: Dominio
 * @task 7 Crear Servicios genericos
 */

using Application.Interface;
using Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Base
{
    public class Response<T> : IResponse<T> where T : class
    {
        public Response()
        {

        }
        public string Message { get; set; } = "FAIL Y/O EMPTY";
        public bool Status { get; set; } = false;
        public T Entity { get; set; } = null;

        public IEnumerable<T> Entities { set; get; }
    }
}
