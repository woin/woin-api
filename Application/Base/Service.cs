﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Service
 * @file Base/Service
 * @observations use Base Service for Applications
 * @HU 0: Dominio
 * @task 7 Crear Servicios genericos
 */

using Application.Base;
using Application.Interface;
using Domain.Base;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Base
{
    public  class Service<T> : IService<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<T> _repository;
        public  IResponse<T> _response;
        


        public Service(IUnitOfWork unitOfWork, IRepository<T> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _response = new Response<T>();
        }


        public virtual IResponse<T> Create(T entity)
        {
            if (entity == null) throw new ServiceException("Entity "+entity.GetType().Name+" empty or null");
            try
            {
                
                var t = _repository.Add(entity);

                if (_unitOfWork.Commit() > 0)
                {
                    _response.Entity = t;
                    _response.Status = true;
                    _response.Message = "OK";
                }

            }
            catch(Exception e)
            {
                
                _response.Entity = null;
                _response.Status = false;
                _response.Message =  MessageCodeException.MessageCode(e).ToString(); 
            }
            return _response;
        }

        public virtual IResponse<T> Delete(T entity)
        {
            if (entity == null) throw new ServiceException("Entity " + entity.GetType().Name + " empty or null");
            _repository.Delete(entity);
            if (_unitOfWork.Commit() > 0)
            {
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

        public virtual IResponse<T> Delete(object id)
        {
            if (id == null) throw new ServiceException("id empty or null");
            _repository.Delete(id);
            if (_unitOfWork.Commit() > 0)
            {
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

        public virtual IResponse<T> Find(object id)
        {
            if (id == null) throw new ServiceException("id empty or null");
            var t = _repository.Find(id);
            if(t != null)
            {
                _response.Entity = t;
                _response.Status = true;
                _response.Message = "OK";

            }
          
            return _response;

        }

        public virtual IEnumerable<T> GetAll(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _repository.GetAll(pageIndex, pageSize);
        }

        public virtual IResponse<T> Update(T entity)
        {
            if (entity == null) throw new ServiceException("Entity " + entity.GetType().Name + " empty or null");
            var t = _repository.Edit(entity);
            if (_unitOfWork.Commit() > 0)
            {
                _response.Entity = t;
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

        public virtual async Task<IResponse<T>> FindAsync(object id)
        {
            if (id == null) throw new ServiceException("id empty or null");
            var t = await _repository.FindAsync(id);
            if (t != null)
            {
                _response.Entity = t;
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

        public virtual async Task<IResponse<T>> CreateAsync(T entity)
        {
            if (entity == null) throw new ServiceException("Entity " + entity.GetType().Name + " empty or null");
            try
            {
                var t = await _repository.AddAsync(entity);
                if (_unitOfWork.Commit(true).Result > 0)
                {
                    _response.Entity = t;
                    _response.Status = true;
                    _response.Message = "OK";
                }
            }
            catch(Exception e)
            {
               
                _response.Entity = null;
                _response.Status = false;
                _response.Message = MessageCodeException.MessageCode(e).ToString();
            }
           
            return _response;
        }

        public virtual async Task<IResponse<T>> DeleteAsync(T entity)
        {
            if (entity == null) throw new ServiceException("Entity " + entity.GetType().Name + " empty or null");
            await _repository.DeleteAsync(entity);
            if(await _unitOfWork.Commit(true) > 0)
            {
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

        public virtual async  Task<IResponse<T>> DeleteAsync(object id)
        {
            if (id == null) throw new ServiceException("id empty or null");
            await _repository.DeleteAsync(id);
            if (await _unitOfWork.Commit(true) > 0)
            {
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync() => await _repository.GetAllAsync();

        public virtual async Task<IResponse<T>> UpdateAsync(T entity)
        {
            if (entity == null) throw new ServiceException("Entity " + entity.GetType().Name + " empty or null");
            var t =   _repository.Edit(entity);
            if (await _unitOfWork.Commit(true) > 0) {
                _response.Entity = t;
                _response.Status = true;
                _response.Message = "OK";
            }
            return _response;
        }

      
        public virtual IEnumerable<T> FromSqlRaw(string query, string includeProperties = "", int pageIndex = 0, int pageSize = int.MaxValue, params object[] parameters)
        {
            return _repository.FromSqlRaw(query, pageIndex, pageSize, parameters);
        }
        
        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _repository.FindBy(predicate);
        }


        public IEnumerable<T> FindBy(Expression<Func<T, bool>> filter = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _repository.FindBy(filter, pageIndex, pageSize);
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return _repository.Any(predicate);
        }
    }
}
