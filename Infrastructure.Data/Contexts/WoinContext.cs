﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Domain.Entities;
using Infrastructure.Data.Base;

namespace Infrastructure.Data
{
    public partial class WoinContext : DbContextBase
    {
        public WoinContext()
        {
        }

        public WoinContext(DbContextOptions<WoinContext> options)
            : base(options)
        {
           
        }

        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<Governorate> Governorates { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<PrioritizedAdPreferred> PrioritizedsAdsPreferreds { get; set; }
        public virtual DbSet<PrioritizedAdPurchased> PrioritizedsAdsPurchaseds { get; set; }
        public virtual DbSet<PrioritizedAdView> PrioritizedsAdsViews { get; set; }
        public virtual DbSet<WoinAd> WoinAds { get; set; }
        public virtual DbSet<WoinAdCompany> WoinAdsCompanies { get; set; }
        public virtual DbSet<WoinAdIndicator> WoinAdsIndicators { get; set; }
        public virtual DbSet<WoinAdPacket> WoinAdsPackets { get; set; }
        public virtual DbSet<WoinAdProduct> WoinAdsProducts { get; set; }
        public virtual DbSet<WoinAdView> WoinAdsViews { get; set; }
        public virtual DbSet<WoinAgreement> WoinAgreements { get; set; }
        public virtual DbSet<WoinCalification> WoinCalifications { get; set; }
        public virtual DbSet<WoinCategory> WoinCategories { get; set; }
        public virtual DbSet<WoinCliwoiner> WoinCliwoiners { get; set; }
        public virtual DbSet<WoinColumn> WoinColumns { get; set; }
        public virtual DbSet<WoinColumnSubcategory> WoinColumnsSubcategories { get; set; }
        public virtual DbSet<WoinColumnValue> WoinColumnsValues { get; set; }
        public virtual DbSet<WoinCompany> WoinCompanies { get; set; }
        public virtual DbSet<WoinCompanyPhone> WoinCompaniesPhones { get; set; }
        public virtual DbSet<WoinDocument> WoinDocuments { get; set; }
        public virtual DbSet<WoinDynamicKey> WoinDynamicsKeys { get; set; }
        public virtual DbSet<WoinEmailGift> WoinEmailsGifts { get; set; }
        public virtual DbSet<WoinEmWoiner> WoinEmwoiners { get; set; }
        public virtual DbSet<WoinFaceToFacePurchase> WoinFaceToFacePurchases { get; set; }
        public virtual DbSet<WoinFreeAd> WoinFreesAds { get; set; }
        public virtual DbSet<WoinGift> WoinGifts { get; set; }
        public virtual DbSet<WoinGroup> WoinGroups { get; set; }
        public virtual DbSet<WoinLocation> WoinLocations { get; set; }
        public virtual DbSet<WoinMovement> WoinMovements { get; set; }
        public virtual DbSet<WoinMultimedia> WoinMultimedias { get; set; }
        public virtual DbSet<WoinMultimediaAd> WoinMultimediasAds { get; set; }
        public virtual DbSet<WoinMultimediaCategory> WoinMultimediasCategories { get; set; }
        public virtual DbSet<WoinOffice> WoinOffices { get; set; }
        public virtual DbSet<WoinOnlinePurchase> WoinOnlinePurchases { get; set; }
        public virtual DbSet<WoinPaidAd> WoinPaidsAds { get; set; }
        public virtual DbSet<WoinPayForViewView> WoinPayForViewViews { get; set; }
        public virtual DbSet<WoinPayForView> WoinPayForViews { get; set; }
        public virtual DbSet<WoinPermission> WoinPermissions { get; set; }
        public virtual DbSet<WoinPerson> WoinPersons { get; set; }
        public virtual DbSet<WoinProduct> WoinProducts { get; set; }
        public virtual DbSet<WoinPurchase> WoinPurchases { get; set; }
        public virtual DbSet<WoinRecharge> WoinRecharges { get; set; }
        public virtual DbSet<WoinResourceScope> WoinResourceScope { get; set; }
        public virtual DbSet<WoinRole> WoinRoles { get; set; }
        public virtual DbSet<WoinRolePermission> WoinRolesPermissions { get; set; }
        public virtual DbSet<WoinSession> WoinSessions { get; set; }
        public virtual DbSet<WoinSocialNetwork> WoinSocialNetworks { get; set; }
        public virtual DbSet<WoinSocialProfile> WoinSocialProfiles { get; set; }
        public virtual DbSet<WoinSubcategory> WoinSubcategories { get; set; }
        public virtual DbSet<WoinTransaction> WoinTransactions { get; set; }
        public virtual DbSet<WoinTransactionTransaction> WoinTransactionsTransactions { get; set; }
        public virtual DbSet<WoinUserGroup> WoinUserGroups { get; set; }
        public virtual DbSet<WoinUser> WoinUsers { get; set; }
        public virtual DbSet<WoinUserRole> WoinUsersRoles { get; set; }
        public virtual DbSet<WoinVisitor> WoinVisitors { get; set; }
        public virtual DbSet<WoinWallet> WoinWallets { get; set; }
        public virtual DbSet<WoinWoinWoiner> WoinWoinWoiners { get; set; }
        public virtual DbSet<WoinWoiner> WoinWoiners { get; set; }
        public virtual DbSet<WoinWoinerConfig> WoinWoinersConfigs { get; set; }
        public virtual DbSet<WoinWoinerNetwork> WoinWoinersNetworks { get; set; }
        public virtual DbSet<WoinWoinerPreference> WoinWoinersPreferences { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
           

            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=woin;User Id=zeros;Password=3005;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasDefaultSchema(DefaultSchema);



        

            modelBuilder.Entity<City>(entity =>
            {
                

                entity.ToTable("cities");

                entity.HasComment("Tabla que contiene las ciudades que se usarán en la aplicación");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.GovernorateId)
                    .HasColumnName("governorate_id")
                    .HasComment("Gobernación a la que pertenece");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .HasComment("Nombre de la ciudad");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado actual de la ciudad");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.HasOne(d => d.Governorate)
                    .WithMany(p => p.Cities)
                    .HasForeignKey(d => d.GovernorateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_cities_governorates_1");



               

            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("countries");

                entity.HasComment("Tabla que contiene los países que se usarán en la aplicación");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .HasComment("Nombre del país");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado actual del país");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.ToTable("devices");

                entity.HasComment("Tabla que contiene los dispositivos pertenecientes a los usuarios");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Ip)
                    .HasColumnName("ip")
                    .HasMaxLength(45)
                    .HasComment("Identificación IP del dispositivo");

                entity.Property(e => e.Mac)
                    .HasColumnName("mac")
                    .HasMaxLength(45)
                    .HasComment("Identificación MAC del dispositivo");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("0")
                    .HasComment("Nombre del dispositivo");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado actual del dispositivo");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario al que pertenece el dispositivo");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_devices_woin_users_1");
            });

            modelBuilder.Entity<Governorate>(entity =>
            {
                entity.ToTable("governorates");

                entity.HasComment("Tabla que contiene las gobernaciones quienes contienen a ciudades y pertenecen a un país");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasComment("País al que pertenece la gobernación");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .HasComment("Nombre de la gobernación");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado actual de la gobernación");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Governorates)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_governorates_countries_1");
            });

            modelBuilder.Entity<Phone>(entity =>
            {
                entity.ToTable("phones");

                entity.HasComment("Tabla que contiene los teléfonos usados en la aplicación por woiners, compañías, oficinas, etc");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasComment("País al que pertenece el teléfono");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasColumnName("number")
                    .HasMaxLength(45)
                    .HasComment("Número telefónico");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado actual del teléfono");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Phones)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_phones_countries_1");
            });

            modelBuilder.Entity<PrioritizedAdPreferred>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("prioritizeds_ads_preferreds");

                entity.Property(e => e.AdParent).HasColumnName("ad_parent");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.CurrentStock).HasColumnName("current_stock");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.DiscountPercentage).HasColumnName("discount_percentage");

                entity.Property(e => e.FinalTime).HasColumnName("final_time");

                entity.Property(e => e.GiftPercentage).HasColumnName("gift_percentage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Index)
                    .HasColumnName("index")
                    .HasColumnType("numeric(6,3)");

                entity.Property(e => e.InitialStock).HasColumnName("initial_stock");

                entity.Property(e => e.InitialTime).HasColumnName("initial_time");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(17,2)");

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.SubcategoryId).HasColumnName("subcategory_id");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(100);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.WoinerId).HasColumnName("woiner_id");
            });

            modelBuilder.Entity<PrioritizedAdPurchased>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("prioritizeds_ads_purchaseds");

                entity.Property(e => e.AdParent).HasColumnName("ad_parent");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.CurrentStock).HasColumnName("current_stock");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.DiscountPercentage).HasColumnName("discount_percentage");

                entity.Property(e => e.FinalTime).HasColumnName("final_time");

                entity.Property(e => e.GiftPercentage).HasColumnName("gift_percentage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Index)
                    .HasColumnName("index")
                    .HasColumnType("numeric(6,3)");

                entity.Property(e => e.InitialStock).HasColumnName("initial_stock");

                entity.Property(e => e.InitialTime).HasColumnName("initial_time");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(17,2)");

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.SubcategoryId).HasColumnName("subcategory_id");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(100);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.WoinerId).HasColumnName("woiner_id");
            });

            modelBuilder.Entity<PrioritizedAdView>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("prioritizeds_ads_views");

                entity.Property(e => e.AdParent).HasColumnName("ad_parent");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.CurrentStock).HasColumnName("current_stock");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.DiscountPercentage).HasColumnName("discount_percentage");

                entity.Property(e => e.FinalTime).HasColumnName("final_time");

                entity.Property(e => e.GiftPercentage).HasColumnName("gift_percentage");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Index)
                    .HasColumnName("index")
                    .HasColumnType("numeric");

                entity.Property(e => e.InitialStock).HasColumnName("initial_stock");

                entity.Property(e => e.InitialTime).HasColumnName("initial_time");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(17,2)");

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.SubcategoryId).HasColumnName("subcategory_id");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(100);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.WoinerId).HasColumnName("woiner_id");
            });

            modelBuilder.Entity<WoinAd>(entity =>
            {
                entity.ToTable("woin_ads");

                entity.HasComment("Tabla que contiene las publicaciones o anuncios creados en la aplicación");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdParent)
                    .HasColumnName("ad_parent")
                    .HasComment("Publicación anterior o padre (en caso de que esta sea creada a partir de una publicación anterior)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.CurrentStock)
                    .HasColumnName("current_stock")
                    .HasComment("Cantidad actual disponible");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasComment("Descripción o cuerpo de la publicación");

                entity.Property(e => e.DiscountPercentage)
                    .HasColumnName("discount_percentage")
                    .HasComment("Porcentaje descontado de la publicación");

                entity.Property(e => e.FinalTime)
                    .HasColumnName("final_time")
                    .HasComment("Tiempo de finalización de la publicación");

                entity.Property(e => e.GiftPercentage)
                    .HasColumnName("gift_percentage")
                    .HasComment("Porcentaje del price que se regalará");

                entity.Property(e => e.InitialStock)
                    .HasColumnName("initial_stock")
                    .HasComment("Cantidad inicial disponible de la publicación");

                entity.Property(e => e.InitialTime)
                    .HasColumnName("initial_time")
                    .HasComment("Tiempo de inicio para mostrar la publicación");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Precio de venta del objeto/servicio publicado");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasComment("Producto/recurso que se está promocionando");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado actual de la publicación");

                entity.Property(e => e.SubcategoryId)
                    .HasColumnName("subcategory_id")
                    .HasComment("Subcategoría a la que pertenece el anuncio");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(100)
                    .HasComment("Título de la publicación");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de anuncio (gratis, pago)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner al que pertenece la publicación");

                entity.HasOne(d => d.AdParentNavigation)
                    .WithMany(p => p.InverseAdParentNavigation)
                    .HasForeignKey(d => d.AdParent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_woin_ads_1");

                entity.HasOne(d => d.Subcategory)
                    .WithMany(p => p.WoinAds)
                    .HasForeignKey(d => d.SubcategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_woin_subcategories_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinAds)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_woin_woiners_1");
            });

            modelBuilder.Entity<WoinAdCompany>(entity =>
            {
                entity.ToTable("woin_ads_companies");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("uq_woin_ads_companies_companie_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Anuncio que pertenece a la compañía");

                entity.Property(e => e.CompanyId)
                    .HasColumnName("company_id")
                    .HasComment("Compañia que publica el anuncio");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinAdsCompanies)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_companies_woin_ads_1");

                entity.HasOne(d => d.Company)
                    .WithOne(p => p.WoinAdsCompanies)
                    .HasForeignKey<WoinAdCompany>(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_companies_woin_companies_1");
            });

            modelBuilder.Entity<WoinAdIndicator>(entity =>
            {
                entity.ToTable("woin_ads_indicators");

                entity.HasComment("Tabla que contiene índices y variables para priorizar los anuncios con diferentes enfoques");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Anuncio al que pertenecen los indicadores");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creacuión del registro");

                entity.Property(e => e.CurrentViews)
                    .HasColumnName("current_views")
                    .HasComment("Vistas actuales del anuncio");

                entity.Property(e => e.ExpectedViews)
                    .HasColumnName(@"
expected_views")
                    .HasComment("Vistas que se esperan cumplir en el tiempo del anuncio");

                entity.Property(e => e.IndexPreferred)
                    .HasColumnName("index_preferred")
                    .HasColumnType("numeric(6,3)")
                    .HasComment("Índice que prioriza a los anuncios que son preferidos por los woiners");

                entity.Property(e => e.IndexPurchased)
                    .HasColumnName("index_purchased")
                    .HasColumnType("numeric(6,3)")
                    .HasComment("Índice que prioriza los anuncios que han sido más comprados y han ofrecido una mejor experiencia a los woiners");

                entity.Property(e => e.IndexView)
                    .HasColumnName("index_view")
                    .HasColumnType("numeric(6,3)")
                    .HasComment("Índice que prioriza a los anuncios en riesgo de no cumplir las vistas esperadas");

                entity.Property(e => e.Likes)
                    .HasColumnName("likes")
                    .HasComment("Likes dados al anuncio");

                entity.Property(e => e.PercentageLikesViews)
                    .HasColumnName("percentage_likes_views")
                    .HasColumnType("numeric(6,3)")
                    .HasComment("Porcentaje de likes respecto a las vistas del anuncio (likes/vistas)");

                entity.Property(e => e.Purchases)
                    .HasColumnName("purchases")
                    .HasComment("Compras actuales del anuncio");

                entity.Property(e => e.PurchasesPercentage)
                    .HasColumnName("purchases_percentage")
                    .HasColumnType("numeric(6,3)")
                    .HasComment("Porcentaje de compras respecto a las vistas (compras/vistas)");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("numeric(2,1)")
                    .HasComment("Calificación del anuncio");

                entity.Property(e => e.RemainingDuration)
                    .HasColumnName("remaining_duration")
                    .HasComment("Duración restante del anuncio antes de caducar");

                entity.Property(e => e.Shared)
                    .HasColumnName("shared")
                    .HasComment("Veces que se ha compartido el anuncio");

                entity.Property(e => e.TotalDuration)
                    .HasColumnName("total_duration")
                    .HasComment("Duración total del anuncio en milisegundos");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");

                entity.Property(e => e.ViewsPercentage)
                    .HasColumnName("views_percentage")
                    .HasColumnType("numeric(6,3)")
                    .HasComment("Porcentaje de vistas (current/expected)");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinAdsIndicators)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_priorizations_woin_ads_1");
            });

            modelBuilder.Entity<WoinAdPacket>(entity =>
            {
                entity.ToTable("woin_ads_packets");

                entity.HasComment("Tabla que contiene los paquetes ofrecidos por woin para las publicaciones hechas por woiners");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.BannerTime)
                    .HasColumnName("banner_time")
                    .HasComment("Tiempo que durará la publicación en el banner");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Duration)
                    .HasColumnName("duration")
                    .HasComment("Duración (visibilidad) del paquete en total");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Precio o costo del paquete publicitario");

                entity.Property(e => e.Schedule)
                    .HasColumnName("schedule")
                    .HasComment("Tipo de horario de visibilidad (tradicional, especial)");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del paquete (activo, inactivo, etc)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de paquete (gratis, pago)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");
            });

            modelBuilder.Entity<WoinAdProduct>(entity =>
            {
                entity.ToTable("woin_ads_products");

                entity.HasComment("Tabla que contiene los productos promocionados en una publicación");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Anuncio al que hace referencia");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.ProdctId)
                    .HasColumnName("prodct_id")
                    .HasComment("Producto que se publica");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinAdsProducts)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_products_woin_ads_1");

                entity.HasOne(d => d.Prodct)
                    .WithMany(p => p.WoinAdsProducts)
                    .HasForeignKey(d => d.ProdctId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_products_woin_products_1");
            });

            modelBuilder.Entity<WoinAdView>(entity =>
            {
                entity.ToTable("woin_ads_views");

                entity.HasComment("Tabla que contiene los anuncios que han visto los woiners para tener estadísticas y personalizar los anuncios mostrados al woiner");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Anuncio o publicación vista");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Like)
                    .HasColumnName("like")
                    .HasComment("Campo que dice si el woiner le dio o no like al anuncio");

                entity.Property(e => e.Preferred)
                    .HasColumnName("preferred")
                    .HasComment("Estado que indica si esa vista es una preferencia del woiner");

                entity.Property(e => e.Shared)
                    .HasColumnName("shared")
                    .HasComment("Campo que dice si el woiner compartió o no un anuncio");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner que vio la publicación o anuncio");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinAdsViews)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_views_woin_ads_indicators_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinAdsViews)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_ads_views_woin_woiners_1");
            });

            modelBuilder.Entity<WoinAgreement>(entity =>
            {
                entity.ToTable("woin_agreements");

                entity.HasComment("Tabla que contiene los convenios que se usarán para transacciones que usen medios externos o internos");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .HasComment("Nombre del convenio (recargas y procesos externos)");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasColumnName("number")
                    .HasMaxLength(20)
                    .HasComment("Número del convenio");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del convenio (activo, inactivo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");
            });

            modelBuilder.Entity<WoinCalification>(entity =>
            {
                entity.ToTable("woin_califications");

                entity.HasComment("Tabla que contiene las calificaciones hechas de un woiner a otro");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .HasComment("Comentario hecho al momento de calificar");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasComment("Valor de la calificación (1…5)");

                entity.Property(e => e.WoinerReceiverId)
                    .HasColumnName("woiner_receiver_id")
                    .HasComment("Woiner al que se califica (calificado)");

                entity.Property(e => e.WoinerSenderId)
                    .HasColumnName("woiner_sender_id")
                    .HasComment("Woiner que califica (calificador)");

                entity.HasOne(d => d.WoinerReceiver)
                    .WithMany(p => p.WoinCalificationsWoinerReceiver)
                    .HasForeignKey(d => d.WoinerReceiverId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_califications_woin_woiners_2");

                entity.HasOne(d => d.WoinerSender)
                    .WithMany(p => p.WoinCalificationsWoinerSender)
                    .HasForeignKey(d => d.WoinerSenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_califications_woin_woiners_1");
            });

            modelBuilder.Entity<WoinCategory>(entity =>
            {
                entity.ToTable("woin_categories");

                entity.HasComment("Tabla que contiene las categorías que identifican o agrupan las publicaciones [en servicios, productos, bienes raíces, transportes, etc]");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasComment("Descripción o detalle extenso de la categoría");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .HasComment("Nombre o título de la categoría");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la categoría (activa, inactiva, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");
            });

            modelBuilder.Entity<WoinCliwoiner>(entity =>
            {
                entity.ToTable("woin_cliwoiners");

                entity.HasComment("Tabla hija de woin_woiners, con el fin de generar una herencia");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner padre o del que se hereda");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinCliwoiners)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_cliwoiners_woin_woiners_1");
            });

            modelBuilder.Entity<WoinColumn>(entity =>
            {
                entity.ToTable("woin_columns");

                entity.HasComment("Tabla de columnas que se usarán como propiedades en las publicaciones (marca, color, tamaño, etc)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .HasComment("Nombre de la columna o título de la propiedad");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la columna o propiedad (activa, …)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");
            });

            modelBuilder.Entity<WoinColumnSubcategory>(entity =>
            {
                entity.ToTable("woin_columns_subcategories");

                entity.HasComment("Tabla intercepto de columnas y subcategorias");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ColumnId)
                    .HasColumnName("column_id")
                    .HasComment("Identificador de la columna a la que pertenece");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la columna perteneciente a una subcategoria");

                entity.Property(e => e.SubcategoryId)
                    .HasColumnName("subcategory_id")
                    .HasComment("Identificador de la subcategoria a la que pertenece");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de columna (primaria, secundaria, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Column)
                    .WithMany(p => p.WoinColumnsSubcategories)
                    .HasForeignKey(d => d.ColumnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_columns_subcategories_woin_columns_1");

                entity.HasOne(d => d.Subcategory)
                    .WithMany(p => p.WoinColumnsSubcategories)
                    .HasForeignKey(d => d.SubcategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_columns_subcategories_woin_subcategories_1");
            });

            modelBuilder.Entity<WoinColumnValue>(entity =>
            {
                entity.ToTable("woin_columns_values");

                entity.HasComment("Tabla que contiene los valores de columnas o propiedades de una publicación");

                entity.HasIndex(e => new { e.AdId, e.ColumnId })
                    .HasName("uq_columns_values_ad_column")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Publicación a la que pertenece este valor o contenido");

                entity.Property(e => e.ColumnId)
                    .HasColumnName("column_id")
                    .HasComment("Columna a la que hace referencia el contenido o valor");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content")
                    .HasMaxLength(45)
                    .HasComment("Contenido de la columna para una respectiva publicación");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del contenido de la columna (eliminada, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinColumnsValues)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_columns_values_woin_ads_1");

                entity.HasOne(d => d.Column)
                    .WithMany(p => p.WoinColumnsValues)
                    .HasForeignKey(d => d.ColumnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_columns_values_woin_columns_1");
            });

            modelBuilder.Entity<WoinCompany>(entity =>
            {
                entity.ToTable("woin_companies");

                entity.HasComment("Tabla que contiene las compañías pertenecientes a un emwoiner");

                entity.HasIndex(e => e.Name)
                    .HasName("uq_woin_companies_name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.EmwoinerId)
                    .HasColumnName("emwoiner_id")
                    .HasComment("Emwoiner al que pertenece la compañía");

                entity.Property(e => e.LocationId)
                    .HasColumnName("location_id")
                    .HasComment("Localización o ubicación de la compañía en gps");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .HasComment("Nombre de la compañía");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la compañía (activa, inactiva, etc)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de compañía");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Emwoiner)
                    .WithMany(p => p.WoinCompanies)
                    .HasForeignKey(d => d.EmwoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_companies_woin_emwoiners_1");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.WoinCompanies)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_companies_woin_locations_1");
            });

            modelBuilder.Entity<WoinCompanyPhone>(entity =>
            {
                entity.ToTable("woin_companies_phones");

                entity.HasComment("Tabla intercepto que contiene teléfonos relacionados a compañías");

                entity.HasIndex(e => new { e.CompanyId, e.PhoneId })
                    .HasName("uq_companies_id_phones_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CompanyId)
                    .HasColumnName("company_id")
                    .HasComment("Identificador de la compañía a la que pertenece");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.PhoneId)
                    .HasColumnName("phone_id")
                    .HasComment("Identificador del teléfono al que se hace referencia");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del teléfono para esa compañía");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.WoinCompaniesPhones)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_companies_phones_woin_companies_1");

                entity.HasOne(d => d.Phone)
                    .WithMany(p => p.WoinCompaniesPhones)
                    .HasForeignKey(d => d.PhoneId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_companies_phones_phones_1");
            });

            modelBuilder.Entity<WoinDocument>(entity =>
            {
                entity.ToTable("woin_documents");

                entity.HasComment("Tabla que contiene los documentos personales (identificaciones)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CityId)
                    .HasColumnName("city_id")
                    .HasComment("Ciudad de registro del documento");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasColumnName("number")
                    .HasMaxLength(15)
                    .HasComment("Número del documento de identidad");

                entity.Property(e => e.PersonId)
                    .HasColumnName("person_id")
                    .HasComment("Persona a la que pertenece el documento");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del documento (activo, inactivo, en espera, etc)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de documento (cédula, extranjero, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.WoinDocuments)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_documents_cities_1");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.WoinDocuments)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_documents_woin_persons_1");
            });

            modelBuilder.Entity<WoinDynamicKey>(entity =>
            {
                entity.ToTable("woin_dynamics_keys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)");

                entity.Property(e => e.DynamicKey)
                    .HasColumnName("dynamic_key")
                    .HasComment("Clave dinámica para validar la transacción");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasDefaultValueSql("1")
                    .HasComment("Estado de la clave dinámica [use, active, ...]");

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)");

                entity.Property(e => e.WoinerId).HasColumnName("woiner_id");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.WoinDynamicsKeys)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_dynamics_keys_woin_transactions_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinDynamicsKeys)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_dynamics_keys_woin_woiners_1");
            });

            modelBuilder.Entity<WoinEmailGift>(entity =>
            {
                entity.ToTable("woin_emails_gifts");

                entity.HasComment("Tabla que contiene los regalos a personas no registradas (usando sus emails)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Monto a regalar");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .HasComment("Correo electrónico al que se envía el regalo");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del regalo (aceptado, erróneo,  vencido, etc)");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasComment("Transacción generada por el regalo");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner que hace el regalo");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.WoinEmailsGifts)
                    .HasForeignKey(d => d.TransactionId)
                    .HasConstraintName("fk_woin_emails_gifts_woin_transactions_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinEmailsGifts)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_emails_gifts_woin_woiners_1");
            });

            modelBuilder.Entity<WoinEmWoiner>(entity =>
            {
                entity.ToTable("woin_emwoiners");

                entity.HasComment("Tabla que contiene los registros de los emwoiners, que son tipos de woiners");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner al que hace referencia");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinEmwoiners)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_emwoiners_woin_woiners_1");
            });

            modelBuilder.Entity<WoinFaceToFacePurchase>(entity =>
            {
                entity.ToTable("woin_face_to_face_purchases");

                entity.HasComment("Tabla que contiene las ventas de tipo cara a cara es decir, sin una publicación.");

                entity.HasIndex(e => e.PurchaseId)
                    .HasName("uq_face_to_face_purchases")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Cash)
                    .HasColumnName("cash")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Cantidad pagada en efectivo");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.PurchaseId)
                    .HasColumnName("purchase_id")
                    .HasComment("Venta a la que hace referencia");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.VendorId)
                    .HasColumnName("vendor_id")
                    .HasComment("Vendedor o woiner que ofrece el producto");

                entity.HasOne(d => d.Purchase)
                    .WithOne(p => p.WoinFaceToFacePurchases)
                    .HasForeignKey<WoinFaceToFacePurchase>(d => d.PurchaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_face_to_face_purchases_woin_purchases_1");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.WoinFaceToFacePurchases)
                    .HasForeignKey(d => d.VendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_face_to_face_purchases_woin_woiners_1");
            });

            modelBuilder.Entity<WoinFreeAd>(entity =>
            {
                entity.ToTable("woin_frees_ads");

                entity.HasComment("Tabla que contiene los anuncios o publicaciones gratis");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Anuncio al que hace referencia");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.End)
                    .HasColumnName("end")
                    .HasComment("Fecha de fin del anuncio gratis");

                entity.Property(e => e.PacketId)
                    .HasColumnName("packet_id")
                    .HasComment("Paquete usado para publicar el anuncio");

                entity.Property(e => e.Start)
                    .HasColumnName("start")
                    .HasComment("Fecha de inicio del anuncio gratis");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinFreesAds)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_frees_ads_woin_ads_1");

                entity.HasOne(d => d.Packet)
                    .WithMany(p => p.WoinFreesAds)
                    .HasForeignKey(d => d.PacketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_frees_ads_woin_ads_packets_1");
            });

            modelBuilder.Entity<WoinGift>(entity =>
            {
                entity.ToTable("woin_gifts");

                entity.HasComment("Tabla que contiene los regalos hechos entre los woiners para producir comisiones entre las redes de estos");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Registro único del registro");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Monto que será repartido por la red de ambos woiners");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del regalo (espera, correcto, incorrecto)");

                entity.Property(e => e.Transactions)
                    .IsRequired()
                    .HasColumnName("transactions")
                    .HasComment("Transacciones generadas por el regalo");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario que ejecuta la acción");

                entity.Property(e => e.WalletReceiver)
                    .HasColumnName("wallet_receiver")
                    .HasComment("Wallet que recibe el regalo");

                entity.Property(e => e.WalletSender)
                    .HasColumnName("wallet_sender")
                    .HasComment("Biiletera que envía el regalo");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WoinGifts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_gifts_woin_users_1");

                entity.HasOne(d => d.WalletReceiverNavigation)
                    .WithMany(p => p.WoinGiftsWalletReceiverNavigation)
                    .HasForeignKey(d => d.WalletReceiver)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_gifts_woin_woiners_2");

                entity.HasOne(d => d.WalletSenderNavigation)
                    .WithMany(p => p.WoinGiftsWalletSenderNavigation)
                    .HasForeignKey(d => d.WalletSender)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_gifts_woin_woiners_1");
            });

            modelBuilder.Entity<WoinGroup>(entity =>
            {
                entity.ToTable("woin_groups");

                entity.HasComment("Tabla que contiene los grupos de usuarios por rol");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creacion del registro");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .HasComment("Nombre del grupo");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasComment("Rol que está relacionado al grupo de usuarios");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de ultima actualizacion del registro");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.WoinGroups)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_groups_woin_roles_1");
            });

            modelBuilder.Entity<WoinLocation>(entity =>
            {
                entity.ToTable("woin_locations");

                entity.HasComment("Tabla que contiene los registros de ubicaciones gps");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Altitude)
                    .HasColumnName("altitude")
                    .HasColumnType("numeric(18,15)")
                    .HasComment("Altitud de coordenada gps");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasColumnType("numeric(18,15)")
                    .HasComment("Latitud de coordenada gps");

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasColumnType("numeric(18,15)")
                    .HasComment("Longitud de coordenada gps");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");
            });

            modelBuilder.Entity<WoinMovement>(entity =>
            {
                entity.ToTable("woin_movements");

                entity.HasComment("Tabla que contiene los movimientos generados por transacciones, estos movimientos actualizan el balance de las billteras");

                entity.HasIndex(e => new { e.Type, e.TransactionId })
                    .HasName("uq_woin_movements_transaction_id_type")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Dinero total del movimiento");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.CurrentBalance)
                    .HasColumnName("current_balance")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Balance actual de la billetera luego del movimiento");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del movimiento (visible, no visible)");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasComment("Transacción a la que hace referencia el movimiento");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo del movimiento (ingreso, egreso)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WalletId)
                    .HasColumnName("wallet_id")
                    .HasComment("Billetera a la que pertenece el movimiento");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.WoinMovements)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_movements_woin_transactions_1");

                entity.HasOne(d => d.Wallet)
                    .WithMany(p => p.WoinMovements)
                    .HasForeignKey(d => d.WalletId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_movements_woin_wallets_1");
            });

            modelBuilder.Entity<WoinMultimedia>(entity =>
            {
                entity.ToTable("woin_multimedias");

                entity.HasComment("Tabla que contiene las multimedias que serán usadas por otras tablas");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasComment("Descripción o detalle de la multimedia");

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasColumnName("source")
                    .HasComment("Ruta o ubicación del recurso");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la multimedia (eliminada, activa)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de multimedia (imagen, vídeo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");
            });

            modelBuilder.Entity<WoinMultimediaAd>(entity =>
            {
                entity.ToTable("woin_multimedias_ads");

                entity.HasComment("Tabla intercepto que contiene las multimedias relacionadas a una publicación");

                entity.HasIndex(e => new { e.MultimediaId, e.AdId })
                    .HasName("uq_multimedia_id_ad_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Identificador de la publicación o anuncio");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.MultimediaId)
                    .HasColumnName("multimedia_id")
                    .HasComment("Identificador de la multimedia");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la multimedia para esa publicación");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinMultimediasAds)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_multimedias_ads_woin_ads_1");

                entity.HasOne(d => d.Multimedia)
                    .WithMany(p => p.WoinMultimediasAds)
                    .HasForeignKey(d => d.MultimediaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_multimedias_ads_woin_multimedias_1");
            });

            modelBuilder.Entity<WoinMultimediaCategory>(entity =>
            {
                entity.ToTable("woin_multimedias_categories");

                entity.HasComment("Tabla intercepto que contiene las multimedias relacionadas a una categoría [de publicación]");

                entity.HasIndex(e => new { e.MultmediaId, e.CategoryId })
                    .HasName("uq_multimedia_id_category_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasComment("Identificador de la categoría [de publicación]");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.MultmediaId)
                    .HasColumnName("multmedia_id")
                    .HasComment("Identificador de la multimedia");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la multimedia para esa categoría");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.WoinMultimediasCategories)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_multimedias_categories_woin_categories_1");

                entity.HasOne(d => d.Multmedia)
                    .WithMany(p => p.WoinMultimediasCategories)
                    .HasForeignKey(d => d.MultmediaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_multimedias_categories_woin_multimedias_1");
            });

            modelBuilder.Entity<WoinOffice>(entity =>
            {
                entity.ToTable("woin_offices");

                entity.HasComment("Tabla que contiene las oficinas de una compañía y sus respectivos datos");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CompanyId)
                    .HasColumnName("company_id")
                    .HasComment("Compañía a la que pertenece la oficina");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.LocationId)
                    .HasColumnName("location_id")
                    .HasComment("Ubicación gps de la oficina");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .HasComment("Nombre de la oficina");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la oficina (activa, inactiva, etc)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de oficina (principal, secundaria, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WalletId).HasColumnName("wallet_id");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.WoinOffices)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_offices_woin_companies_1");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.WoinOffices)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_offices_woin_locations_1");

                entity.HasOne(d => d.Wallet)
                    .WithMany(p => p.WoinOffices)
                    .HasForeignKey(d => d.WalletId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_offices_woin_wallets_1");
            });

            modelBuilder.Entity<WoinOnlinePurchase>(entity =>
            {
                entity.ToTable("woin_online_purchases");

                entity.HasComment("Tabla que contiene las ventas online, es decir, aquellas que se hacen por medio de una publicación o anuncio");

                entity.HasIndex(e => e.PurchaseId)
                    .HasName("uq_online_purchases_purchase_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Publicación o anuncio a la que hace referencia");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.PurchaseId)
                    .HasColumnName("purchase_id")
                    .HasComment("Venta a la que hace referencia");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasDefaultValueSql("1")
                    .HasComment("Cantidad de artículos en la compra");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinOnlinePurchases)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_online_purchases_woin_ads_1");

                entity.HasOne(d => d.Purchase)
                    .WithOne(p => p.WoinOnlinePurchases)
                    .HasForeignKey<WoinOnlinePurchase>(d => d.PurchaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_online_purchases_woin_purchases_1");
            });

            modelBuilder.Entity<WoinPaidAd>(entity =>
            {
                entity.ToTable("woin_paids_ads");

                entity.HasComment("Tabla que contiene los anuncios o publicaciones pagas a través de un paquete ofrecido por woin");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AdId)
                    .HasColumnName("ad_id")
                    .HasComment("Anuncio al que hace referencia");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.End)
                    .HasColumnName("end")
                    .HasComment("Fecha de fin del anuncio gratis");

                entity.Property(e => e.PacketId)
                    .HasColumnName("packet_id")
                    .HasComment("Paquete usado para publicar el anuncio");

                entity.Property(e => e.Start)
                    .HasColumnName("start")
                    .HasComment("Fecha de inicio del anuncio gratis");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasComment("Transacción generada para el pago de paquete");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Ad)
                    .WithMany(p => p.WoinPaidsAds)
                    .HasForeignKey(d => d.AdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_paids_ads_woin_ads_1");

                entity.HasOne(d => d.Packet)
                    .WithMany(p => p.WoinPaidsAds)
                    .HasForeignKey(d => d.PacketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_paids_ads_woin_ads_packets_1");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.WoinPaidsAds)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_paids_ads_woin_transactions_1");
            });

            modelBuilder.Entity<WoinPayForViewView>(entity =>
            {
                entity.ToTable("woin_pay_for_view_views");

                entity.HasComment("Tabla que contiene los woiners que han visto un pago por ver");

                entity.HasIndex(e => new { e.WoinerId, e.PayForViewId })
                    .HasName("uq_pay_for_view_views_woiner")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.PayForViewId)
                    .HasColumnName("pay_for_view_id")
                    .HasComment("Pago por ver que fue visto por el woiner");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner que vio el pago por ver");

                entity.HasOne(d => d.PayForView)
                    .WithMany(p => p.WoinPayForViewViews)
                    .HasForeignKey(d => d.PayForViewId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_pay_for_view_views_woin_pay_for_view_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinPayForViewViews)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_pay_for_view_views_woin_woiners_1");
            });

            modelBuilder.Entity<WoinPayForView>(entity =>
            {
                entity.ToTable("woin_pay_for_views");

                entity.HasComment("Tabla que contiene los pagos por ver realizados por un woiner (el woiner que vea el anuncio ganará cierta cantidad de puntos)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.CurrentAmount)
                    .HasColumnName("current_amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Monto o dinero actual, lo que resta por regalar");

                entity.Property(e => e.InitialAmount)
                    .HasColumnName("initial_amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Monto o dinero inicial para el pago por ver");

                entity.Property(e => e.PaidId)
                    .HasColumnName("paid_id")
                    .HasComment("Anuncio pago al que hace referencia");

                entity.Property(e => e.PersonAmount)
                    .HasColumnName("person_amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Monto o dinero a regalar por persona");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.ViewDuration)
                    .HasColumnName("view_duration")
                    .HasComment("Duración por vista de woiner para regalar");

                entity.Property(e => e.ViewGift)
                    .HasColumnName("view_gift")
                    .HasComment("Vistas esperadas para regalar");

                entity.HasOne(d => d.Paid)
                    .WithMany(p => p.WoinPayForViews)
                    .HasForeignKey(d => d.PaidId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_pay_for_view_woin_paids_ads_1");
            });

            modelBuilder.Entity<WoinPermission>(entity =>
            {
                entity.ToTable("woin_permissions");

                entity.HasComment("Tabla que contiene los permisos que permitirán ejecutar acciones en la aplicación");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasComment("Descripción o detalle del permiso, explicación de este");

                entity.Property(e => e.Level)
                    .HasColumnName("level")
                    .HasComment("Nivel del permiso (prioritario, crítico, etc)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .HasComment("Nombre del permiso o código asignado por woin");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del permiso (activo, inactivo)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");
            });

            modelBuilder.Entity<WoinPerson>(entity =>
            {
                entity.ToTable("woin_persons");

                entity.HasComment("Tabla que contiene las personas que participan en el sistema");

                entity.HasIndex(e => e.UserId)
                    .HasName("uq_persons_users")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Birthdate)
                    .HasColumnName("birthdate")
                    .HasComment("Fecha de nacimiento de la persona");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.FirstLastname)
                    .IsRequired()
                    .HasColumnName("first_lastname")
                    .HasMaxLength(45)
                    .HasComment("Primer apellido de la persona");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(50)
                    .HasComment("Primer nombre de la persona");

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasComment("Género de la persona (masculino, femenino, otro)");

                entity.Property(e => e.SecondLastname)
                    .HasColumnName("second_lastname")
                    .HasMaxLength(45)
                    .HasComment("Segundo apellido de la persona");

                entity.Property(e => e.Secondname)
                    .HasColumnName("secondname")
                    .HasMaxLength(50)
                    .HasComment("Segundo nombre de la persona");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario al que pertenece la persona");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.WoinPersons)
                    .HasForeignKey<WoinPerson>(d => d.UserId)
                    .HasConstraintName("fk_woin_persons_woin_users_1");
            });

            modelBuilder.Entity<WoinProduct>(entity =>
            {
                entity.ToTable("woin_products");

                entity.HasComment("Tabla que contiene los productos de la plataforma");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificación única del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .HasComment("Descripción del producto");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(200)
                    .HasComment("Nombre del producto/servicio/inmueble/transporte/inmueble");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Precio del producto");

                entity.Property(e => e.SubcategoryId)
                    .HasColumnName("subcategory_id")
                    .HasComment("Subcategoría a la que pertenece el producto");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última actualización del registro");

                entity.HasOne(d => d.Subcategory)
                    .WithMany(p => p.WoinProducts)
                    .HasForeignKey(d => d.SubcategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_products_woin_subcategories_1");
            });

            modelBuilder.Entity<WoinPurchase>(entity =>
            {
                entity.ToTable("woin_purchases");

                entity.HasComment("Tabla que contiene las ventas registradas en la aplicación");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Detail)
                    .IsRequired()
                    .HasColumnName("detail")
                    .HasMaxLength(255)
                    .HasComment("Detalle de la compra");

                entity.Property(e => e.Gift)
                    .HasColumnName("gift")
                    .HasColumnType("numeric(17,2)");

                entity.Property(e => e.Points)
                    .HasColumnName("points")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Valor de la venta");

                entity.Property(e => e.PurchaserId)
                    .HasColumnName("purchaser_id")
                    .HasComment("Comprado o woiner que recibe el producto");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la compra (espera, correcta, errónea, cancelada, etc)");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasComment("Transacción generada por la compra");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.VendorId)
                    .HasColumnName("vendor_id")
                    .HasComment("Vendedor o woiner que recibe el pago");

                entity.HasOne(d => d.Purchaser)
                    .WithMany(p => p.WoinPurchasesPurchaser)
                    .HasForeignKey(d => d.PurchaserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_purchases_woin_woiners_1");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.WoinPurchases)
                    .HasForeignKey(d => d.TransactionId)
                    .HasConstraintName("fk_woin_purchases_woin_transactions_1");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.WoinPurchasesVendor)
                    .HasForeignKey(d => d.VendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_purchases_woin_woiners_2");
            });

            modelBuilder.Entity<WoinRecharge>(entity =>
            {
                entity.ToTable("woin_recharges");

                entity.HasComment("Tabla que contiene las recargas hechas por woiners en el sistema");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.AgreementId)
                    .HasColumnName("agreement_id")
                    .HasComment("Convenio al que hace referencia la recarga");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la recarga (error, espera, correcto, etc)");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasComment("Transacción generada por la recarga");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Agreement)
                    .WithMany(p => p.WoinRecharges)
                    .HasForeignKey(d => d.AgreementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_recharges_woin_agreements_1");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.WoinRecharges)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_recharges_woin_movements_1");
            });

            modelBuilder.Entity<WoinResourceScope>(entity =>
            {
                entity.ToTable("woin_resource_scope");

                entity.HasComment("Tabla que contiene el alcance de un recurso del sistema (mundial, nacional, departamental, municipal)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CityId)
                    .HasColumnName("city_id")
                    .HasComment("Ciudad o Municipio donde tendrá alcance el recurso (Si es null será un alcance departamental)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasComment("País donde tendrá alcance el recurso (Si es null será un alcance mundial)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.GovernorateId)
                    .HasColumnName("governorate_id")
                    .HasComment("Gobrernación donde tendrá alcance el recurso (Si es null será un alcance nacional)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última actualización del registro");
            });

            modelBuilder.Entity<WoinRole>(entity =>
            {
                entity.ToTable("woin_roles");

                entity.HasComment("Tabla que contiene los roles que se manejarán en el sistema (woiner, cliwoiner, etc)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasComment("Descripción o detalle del rol");

                entity.Property(e => e.Level)
                    .HasColumnName("level")
                    .HasComment("Nivel del rol (primario, secundario, etc)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .HasComment("Nombre del rol o código asignado por woin");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del rol (activo, inactivo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");
            });

            modelBuilder.Entity<WoinRolePermission>(entity =>
            {
                entity.ToTable("woin_roles_permissions");

                entity.HasComment("Tabla intercepto que contiene los permisos relacionados a un rol");

                entity.HasIndex(e => new { e.RoleId, e.PermissionId })
                    .HasName("uq_roles_id_permission_id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.PermissionId)
                    .HasColumnName("permission_id")
                    .HasComment("Identificador del permiso relacionado");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasComment("Identificador del rol");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del rol (activo, inactivo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.WoinRolesPermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_roles_permissions_woin_permissions_1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.WoinRolesPermissions)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_roles_permissions_woin_roles_1");
            });

            modelBuilder.Entity<WoinSession>(entity =>
            {
                entity.ToTable("woin_sessions");

                entity.HasComment("Tabla que contiene las sesiones de los usuarios en el sistema");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("device_id")
                    .HasComment("Dispositivo desde el que se inició sesión");

                entity.Property(e => e.LocationId)
                    .HasColumnName("location_id")
                    .HasComment("Ubicación gps desde la que se inició la sesión");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la sesión (activo, inactivo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario al que pertenece la sesión");

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.WoinSessions)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("fk_woin_sessions_devices_1");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.WoinSessions)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_sessions_woin_locations_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WoinSessions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_sessions_woin_users_1");
            });

            modelBuilder.Entity<WoinSocialNetwork>(entity =>
            {
                entity.ToTable("woin_social_networks");

                entity.HasComment("Tabla que contiene las redes sociales que se usarán para los perfiles de los woiners");

                entity.HasIndex(e => e.Domain)
                    .HasName("uq_woin_social_networks_domain")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .HasComment("Dominio de la red social (woin.com, etc)");

                entity.Property(e => e.MultimediaId)
                    .HasColumnName("multimedia_id")
                    .HasComment("Multimedia de la red social (ícono, imagen)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .HasComment("Nombre de la red social (facebook, twitter, etc)");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la red social (activa, inactiva, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Multimedia)
                    .WithMany(p => p.WoinSocialNetworks)
                    .HasForeignKey(d => d.MultimediaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_social_networks_woin_multimedias_1");
            });

            modelBuilder.Entity<WoinSocialProfile>(entity =>
            {
                entity.ToTable("woin_social_profiles");

                entity.HasComment("Tabla que contiene los perfiles de redes sociales de los woiners");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Profile)
                    .IsRequired()
                    .HasColumnName("profile")
                    .HasComment("Perfil del woiner (@username)");

                entity.Property(e => e.SocialNetworkId)
                    .HasColumnName("social_network_id")
                    .HasComment("Red social al que hace referencia el perfil");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de los perfiles sociales");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner al que pertenece el perfil");

                entity.HasOne(d => d.SocialNetwork)
                    .WithMany(p => p.WoinSocialProfiles)
                    .HasForeignKey(d => d.SocialNetworkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_social_profiles_woin_social_networks_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinSocialProfiles)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_social_profiles_woin_woiners_1");
            });

            modelBuilder.Entity<WoinSubcategory>(entity =>
            {
                entity.ToTable("woin_subcategories");

                entity.HasComment("Tabla que contiene las subcategorias que pertenecen a una categoria [de publicación]");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasComment("Categoría a la que pertenece la subcategoría");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .HasComment("Nombre de la subcategoria (aseo, motocicleta, etc)");

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent_id")
                    .HasComment("Subcategoría Padre");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la subcategoria (activa, inactiva)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.WoinSubcategories)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_subcategories_woin_categories_1");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_subcategories_woin_subcategories_1");
            });

            modelBuilder.Entity<WoinTransaction>(entity =>
            {
                entity.ToTable("woin_transactions");

                entity.HasComment("Tabla que contiene las transacciones hechas por woiners (internas, transferencias, compras, regalos, recargas, comisiones)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Monto o cantidad de puntos de la transacción");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Detail)
                    .IsRequired()
                    .HasColumnName("detail")
                    .HasComment("Detalle, descripción o cuerpo de la transacción");

                entity.Property(e => e.SessionId)
                    .HasColumnName("session_id")
                    .HasComment("Usuario que ejecuta la acción");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la transacción (espera, correcta, rechazada, errónea)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de la transacción (interna, transferencia, compra, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WalletReceiver)
                    .HasColumnName("wallet_receiver")
                    .HasComment("Billetera que recibe los puntos en la transacción");

                entity.Property(e => e.WalletSender)
                    .HasColumnName("wallet_sender")
                    .HasComment("Billetera que envía los puntos en la transacción");

                entity.HasOne(d => d.Session)
                    .WithMany(p => p.WoinTransactions)
                    .HasForeignKey(d => d.SessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_transactions_woin_sessions_1");

                entity.HasOne(d => d.WalletReceiverNavigation)
                    .WithMany(p => p.WoinTransactionsWalletReceiverNavigation)
                    .HasForeignKey(d => d.WalletReceiver)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_transactions_woin_wallets_2");

                entity.HasOne(d => d.WalletSenderNavigation)
                    .WithMany(p => p.WoinTransactionsWalletSenderNavigation)
                    .HasForeignKey(d => d.WalletSender)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_transactions_woin_wallets_1");
            });

            modelBuilder.Entity<WoinTransactionTransaction>(entity =>
            {
                entity.ToTable("woin_transactions_transactions");

                entity.HasComment("Tabla que contiene las transacciones padres con sus respectivos hijos");

                entity.HasIndex(e => e.ChildId)
                    .HasName("uq_woin_transactions_transactions_child_id")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChildId)
                    .HasColumnName("child_id")
                    .HasComment("Transacción generada por la transacción padre");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent_id")
                    .HasComment("Transacción padre");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Última fecha de modificación del registro");

                entity.HasOne(d => d.Child)
                    .WithOne(p => p.WoinTransactionsTransactionsChild)
                    .HasForeignKey<WoinTransactionTransaction>(d => d.ChildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_transactions_transactions_woin_transactions_2");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.WoinTransactionsTransactionsParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_transactions_transactions_woin_transactions_1");
            });

            modelBuilder.Entity<WoinUserGroup>(entity =>
            {
                entity.ToTable("woin_user_groups");

                entity.HasComment("Tabla que contiene los usuarios pertenecientes a un grupo");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasComment("Grupo al que pertenece el usuario");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario al que se hace referencia");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.WoinUserGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_user_groups_woin_groups_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WoinUserGroups)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_user_groups_woin_users_1");
            });

            modelBuilder.Entity<WoinUser>(entity =>
            {
                entity.ToTable("woin_users");

                entity.HasComment("Tabla que contiene los usuarios registrados en la aplicación o sistema");

                entity.HasIndex(e => e.Username)
                    .HasName("username_uq")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .HasComment("Usuario del usuario para inicio de sesión y recuperación de contraseña");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .HasComment("Contraseña del usuario para inicio de sesión");

                entity.Property(e => e.RememberPassword)
                    .HasColumnName("remember_password")
                    .HasMaxLength(255)
                    .HasComment("Contraseña anterior del usuario");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasComment("Rol del usuario con el que se gestionarán sus permisos");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del usuario (activo, inactivo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(12)
                    .HasComment("Nombre de usuario para inicio de sesión");
            });

            modelBuilder.Entity<WoinUserRole>(entity =>
            {
                entity.ToTable("woin_users_roles");

                entity.HasComment("Tabla que contiene el usuario con cada uno de los roles relacionados a este");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasComment("Rol que tiene el usuario");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última actualización del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario al que se hace referencia");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.WoinUsersRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_users_roles_woin_roles_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WoinUsersRoles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_users_roles_woin_users_1");
            });

            modelBuilder.Entity<WoinVisitor>(entity =>
            {
                entity.ToTable("woin_visitors");

                entity.HasComment("Tabla que contiene los visitantes o personas que ingresan a la aplicación si registrarse");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Ip)
                    .HasColumnName("ip")
                    .HasMaxLength(45)
                    .HasComment("Identificación IP del visitante");

                entity.Property(e => e.LocationId)
                    .HasColumnName("location_id")
                    .HasComment("Localización del visitante");

                entity.Property(e => e.Mac)
                    .HasColumnName("mac")
                    .HasMaxLength(45)
                    .HasComment("Identificación MAC del visitante");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasComment("Rol que se le asignará al visitante para definir sus permisos");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.WoinVisitors)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("fk_woin_visitors_woin_locations_1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.WoinVisitors)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_visitors_woin_roles_1");
            });

            modelBuilder.Entity<WoinWallet>(entity =>
            {
                entity.ToTable("woin_wallets");

                entity.HasComment("Tabla que contiene las billeteras de los woiners que contienen los puntos woins");

                entity.HasIndex(e => e.Number)
                    .HasName("uq_woin_wallets_number")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Balance)
                    .HasColumnName("balance")
                    .HasColumnType("numeric(17,2)")
                    .HasComment("Balance o total de puntos en la billetera");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasComment("País al que pertenece la billetera");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasColumnName("number")
                    .HasMaxLength(20)
                    .HasComment("Número de la cuenta que la identifica a nivel público");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la billetera (activa, inactiva, etc)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de billetera (general, redimible, no redimible, comprometida)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WalletParent)
                    .HasColumnName("wallet_parent")
                    .HasComment("Billetera padre, en caso de ser una subcuenta de una cuenta principal");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner al que pertenece la billetera");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.WoinWallets)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("fk_woin_wallets_countries_1");

                entity.HasOne(d => d.WalletParentNavigation)
                    .WithMany(p => p.InverseWalletParentNavigation)
                    .HasForeignKey(d => d.WalletParent)
                    .HasConstraintName("fk_woin_wallets_woin_wallets_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinWallets)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_wallets_woin_woiners_1");
            });

            modelBuilder.Entity<WoinWoinWoiner>(entity =>
            {
                entity.ToTable("woin_woin_woiners");

                entity.HasComment("Tabla que contiene los woiners representantes por país [Woin Colombia, Woin Perú, etc]");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasComment("País en el que está ubicado el representante");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del representante del país");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Representante de woin por país");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.WoinWoinWoiners)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woin_woiners_countries_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinWoinWoiners)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woin_woiners_woin_woiners_1");
            });

            modelBuilder.Entity<WoinWoiner>(entity =>
            {
                entity.ToTable("woin_woiners");

                entity.HasComment("Tabla que contiene los woiners quienes son los principales actores del sistema");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.Biography)
                    .HasColumnName("biography")
                    .HasComment("Biografía o descripción del woiner");

                entity.Property(e => e.Codewoiner)
                    .IsRequired()
                    .HasColumnName("codewoiner")
                    .HasMaxLength(12)
                    .HasComment("Código que representa al woiner de manera pública");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.PersonId)
                    .HasColumnName("person_id")
                    .HasComment("Persona a la que pertenece el woiner");

                entity.Property(e => e.ProfileId)
                    .HasColumnName("profile_id")
                    .HasComment("Imagen o multimedia de perfil del woiner");

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasComment("Woiner referido, es decir el que nos trajo a la plataforma convirtiéndose en padre de la red");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasComment("Rol que se le asigna al woiner para controlar sus permisos");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado del woiner (activo, inactivo, etc)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasComment("Usuario al que pertenece el woiner");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.WoinWoiners)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiners_woin_persons_1");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.WoinWoiners)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiners_woin_multimedias_1");

                entity.HasOne(d => d.ReferenceNavigation)
                    .WithMany(p => p.InverseReferenceNavigation)
                    .HasForeignKey(d => d.Reference)
                    .HasConstraintName("fk_woin_woiners_woin_woiners_1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.WoinWoiners)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiners_woin_roles_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WoinWoiners)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiners_woin_users_1");
            });

            modelBuilder.Entity<WoinWoinerConfig>(entity =>
            {
                entity.ToTable("woin_woiners_configs");

                entity.HasComment("Tabla que contiene las configuraciones de los woiners");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.GiftForCash)
                    .IsRequired()
                    .HasColumnName("gift_for_cash")
                    .HasColumnType("bit(1)")
                    .HasComment("Valor que indica si el woiner regalará por compras en efectivo (0 = No, 1 = Sí)");

                entity.Property(e => e.MinimumGiftLimitAmount)
                    .HasColumnName("minimum_gift_limit_amount")
                    .HasComment("Monto a partir del cual quiere regalar menos del porcentaje mínimo de regalo");

                entity.Property(e => e.MinimumGiftPercentage)
                    .HasColumnName("minimum_gift_percentage")
                    .HasDefaultValueSql("10")
                    .HasComment("Porcentaje mínimo de regalo del woiner en sus anuncios");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la configuración del woiner");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última de modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner al que pertenece la configuración");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinWoinersConfigs)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiner_configs_woin_woiners_1");
            });

            modelBuilder.Entity<WoinWoinerNetwork>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("woin_woiners_networks");

                entity.HasComment("Tabla que contiene la configuración de la red de woiners");

                entity.HasIndex(e => new { e.Level, e.Type })
                    .HasName("uq_woin_woiners_networks_type_level")
                    .IsUnique();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.GiftPercentage)
                    .HasColumnName("gift_percentage")
                    .HasComment("Porcentaje de regalo (1 a 100)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Level)
                    .HasColumnName("level")
                    .HasComment("Nivel en la red");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasComment("Tipo de nivel (vendedor, comprador)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de última modificación del registro");
            });

            modelBuilder.Entity<WoinWoinerPreference>(entity =>
            {
                entity.ToTable("woin_woiners_preferences");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasComment("Identificador único del registro");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de creación del registro");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasComment("Prioridad de la preferencia (0 = principal, 1 = secundaria, etc)");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasComment("Estado de la preferencia (activa, inactiva)");

                entity.Property(e => e.SubcategoryId)
                    .HasColumnName("subcategory_id")
                    .HasComment("Subcategoría a la que hace referencia la preferencia");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("((date_part('epoch'::text, transaction_timestamp()))::bigint * 1000)")
                    .HasComment("Fecha de la última modificación del registro");

                entity.Property(e => e.WoinerId)
                    .HasColumnName("woiner_id")
                    .HasComment("Woiner al que pertenece las preferencias");

                entity.HasOne(d => d.Subcategory)
                    .WithMany(p => p.WoinWoinersPreferences)
                    .HasForeignKey(d => d.SubcategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiners_preferences_woin_subcategories_1");

                entity.HasOne(d => d.Woiner)
                    .WithMany(p => p.WoinWoinersPreferences)
                    .HasForeignKey(d => d.WoinerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_woin_woiners_preferences_woin_woiners_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
