﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Repository
 * @file Infrastructure/Repositories
 * @observations use Base Infrastructure for System
 * @HU 0: Dominio
 * @task 5 Crear repositorio
 */


using Domain.Base;
using Domain.Interface;
using Infrastructure.Data.Base;
using Infrastructure.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {

        protected IDbContext _db;
        protected readonly DbSet<T> _dbset;
        protected readonly string _table = "";

        public string Columns { get; set; } = "*";

        public Repository(IDbContext context)
        {
            _db = context;
            _dbset = context?.Set<T>();
            
        }

        public Repository(IDbContext context, string table)
        {
            _db = context;
            
            _dbset = context?.Set<T>();
            _table = context.DefaultSchema +"."+ table;
           
        }

        public T Add(T entity) => _dbset?.Add(entity)?.Entity;

        public void AddRange(IEnumerable<T> entities) => _dbset?.AddRange(entities);

        public void Delete(object id) => _dbset?.Remove(Find(id));

        public void Delete(T entity) => _dbset?.Remove(entity);

        public void DeleteRange(IEnumerable<T> entities) => _dbset?.RemoveRange(entities);

        public T Edit(T entity) =>  _dbset?.Update(entity).Entity;

        public void EditRange(IEnumerable<T> entities) => _dbset?.UpdateRange(entities);

        public T Find(object id) => _dbset?.Find(id);

        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset?.Where(predicate)?.Paginate(pageIndex, pageSize);
        }

        
        public async Task<IEnumerable<T>> GetAllAsync(int pageIndex = 0, int pageSize = int.MaxValue) => (await _dbset?.ToListAsync()).Paginate(pageIndex, pageSize);

        public virtual async Task<T> FindAsync(object id)
        {
            return await _dbset.FindAsync(id);
        }

      
        public virtual async Task<T> AddAsync(T entity)
        {
           return (await _dbset.AddAsync(entity)).Entity;
           
        }

        public virtual async Task AddRangeAsync(IEnumerable<T> entities) =>
            await _dbset?.AddRangeAsync(entities);

        public async Task DeleteAsync(T entity) =>
            await Task.FromResult(_dbset?.Remove(entity));

        public async Task DeleteAsync(object id) => await Task.FromResult(_dbset?.Remove(await FindAsync(id)));

        public async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate, int pageIndex = 0, int pageSize = int.MaxValue) => (await _dbset?.Where(predicate)?.ToListAsync()).Paginate(pageIndex, pageSize);

        

        public virtual IEnumerable<T> GetAll(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var db = _db;
            db.ChangeTracker.LazyLoadingEnabled = false;
            IQueryable<T> query = db.Set<T>();
            return query?.Paginate(pageIndex, pageSize);
        }

        public IEnumerable<T> FromSqlRaw(string query, int pageIndex = 0, int pageSize = int.MaxValue, params object[] parameters)
        {

            var q = _dbset?.FromSqlRaw(query, parameters);
            if(_dbset.Count() > 0)
            {
                if(q.Count() > 0)
                {
                    return q?.Paginate(pageIndex, pageSize);
                }
                
            }
            return null;
        }

        public async Task<IEnumerable<T>> FromSqlRawAsync(string query, int pageIndex = 0, int pageSize = int.MaxValue, params object[] parameters)
        {
            var q = _dbset?.FromSqlRaw(query, parameters);
            if (_dbset.Count() > 0)
            {
                if (q.Count() > 0)
                {
                    return (await q.ToListAsync())?.Paginate(pageIndex, pageSize);
                }

            }
            return null;
        }

        public IEnumerable<T> Where(string column, string value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table )} WHERE {column} = {value} {limit} ")?.Paginate(pageIndex, pageSize);
        }

        public IEnumerable<T> Where(List<WhereList> whereLists = null, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue, Expression<Func<T, object>> include = null)
        {
            string where = "";
          

            if (whereLists.Count > 0)
            {
                whereLists?.ForEach(x =>
                {
                    if (x.Key != string.Empty && x.Key != "" && x.Value != string.Empty && x.Value != "")
                    {
                        where += $" {x.Condition}  {x.Key} {x.Operator} '{x.Value}' ";
                    }
                });
                return _dbset
                    ?.FromSqlRaw($"SELECT {Columns} FROM {(table != "" ? table : _table)} WHERE {where}  {limit}")?.Paginate(pageIndex, pageSize);
            }


            return _dbset
                ?.FromSqlRaw($"SELECT {Columns} FROM {(table != "" ? table : _table)} {limit}")?.Paginate(pageIndex, pageSize);

        }


        public IEnumerable<T> WhereIn(string column, int[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
             ?.FromSqlRaw($"SELECT {Columns} FROM  {(table != "" ? table : _table)} WHERE {column} IN( {string.Join(",", values)} ) {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> WhereIn(string column, string[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
           
            return _dbset
                 ?.FromSqlRaw($"SELECT {Columns} FROM  {(table != "" ? table : _table)} WHERE {column} IN( {Quotes.GetQuotes(values)} ) {limit}")?.Paginate(pageIndex, pageSize);
        }

        public IEnumerable<T> WhereIn(string column, double[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
              ?.FromSqlRaw($"SELECT {Columns} FROM  {(table != "" ? table : _table)} WHERE {column} IN( {string.Join(",", values)} ) {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> WhereIn(string column, float[] values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
              ?.FromSqlRaw($"SELECT {Columns} FROM  {(table != "" ? table : _table)}  WHERE {column} IN( {string.Join(",", values)} ) {limit}")?.Paginate(pageIndex, pageSize);
        }

        public IEnumerable<T> WhereIn(string column, string values, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
               ?.FromSqlRaw($"SELECT {Columns} FROM  {(table != "" ? table : _table)} WHERE {column} IN( '{values}' ) {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> GetLike(string column, string value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
              ?.FromSqlRaw($"SELECT {Columns} FROM  {(table != "" ? table : _table)}  WHERE {column} LIKE '%{value}%' {limit}")?.Paginate(pageIndex, pageSize);
        }


        public void Dispose()
        {
            _db.Dispose();
        }

        public IEnumerable<T> Where(string column, int value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> Where(string column, float value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
             ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> Where(string column, double value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table )} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);
     
        }

        public IEnumerable<T> Where(string column, short value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> Where(string column, bool value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> Where(string column, BitArray value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> Where(string column, long value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = {value} {limit}")?.Paginate(pageIndex, pageSize);

        }

        public IEnumerable<T> Where(string column, char value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = '{value}' {limit}")?.Paginate(pageIndex, pageSize);
        }


        public IEnumerable<T> Where(string column, decimal value, string table = "", string limit = "LIMIT 100", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _dbset
            ?.FromSqlRaw($"SELECT {Columns} FROM {(table == "" ? table : _db.Table)} WHERE {column} = '{value}' {limit}")?.Paginate(pageIndex, pageSize);
        }

        public bool Any(Expression<Func<T, bool>> predicate = null)
        {
            return _dbset.Any(predicate);
        }
    }

    




}
