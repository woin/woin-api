﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public static partial class PagedList {



    public static int PageIndex { get; private set; }
    public static int PageSize { get; private set; }
    public static int TotalCount { get; private set; }
    public static int TotalPages { get; private set; }


    public static bool HasPreviousPage
    {
        get { return (PageIndex > 0); }
    }
    public static bool HasNextPage
    {
        get { return (PageIndex + 1 < TotalPages); }
    }

    public static List<T> Paginate<T>(this IList<T> source, int pageIndex = 0, int pageSize = int.MaxValue)
    {
        if (source.Count > 0)
        {
            TotalCount = source.Count();
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            PageSize = pageSize;
            PageIndex = pageIndex;

            return source.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            
        }

        return source.ToList();
    }

    public static List<T> Paginate<T>(this IQueryable<T> source, int pageIndex = 0, int pageSize = int.MaxValue)
    {
        if (source.Count() > 0)
        {
            TotalCount = source.Count();
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            PageSize = pageSize;
            PageIndex = pageIndex;

            return source.Skip(pageIndex * pageSize).Take(pageSize).ToList();

        }

        return source.ToList();
    }


    public static List<T> Paginate<T>(this IEnumerable<T> source, int pageIndex = 0, int pageSize = int.MaxValue)
    {
        if (source.Count() > 0)
        {
            TotalCount = source.Count();
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            PageSize = pageSize;
            PageIndex = pageIndex;

            return source.Skip(pageIndex * pageSize).Take(pageSize).ToList();

        }

        return source.ToList();
    }

}

public class PagedList<T> : List<T>
{

    public PagedList(IQueryable<T> source, int pageIndex, int pageSize)
    {
        if (source.Count() > 0)
        {
            int total = source.Count();
            this.TotalCount = total;
            this.TotalPages = total / pageSize;

            if (total % pageSize > 0)
                TotalPages++;

            this.PageSize = pageSize;
            this.PageIndex = pageIndex;
            this.AddRange(source.Skip(pageIndex * pageSize).Take(pageSize).ToList());

        }

        
    }


    public PagedList(IList<T> source, int pageIndex, int pageSize)
    {
        if(source.Count > 0)
        {
            TotalCount = source.Count();
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            this.PageSize = pageSize;
            this.PageIndex = pageIndex;
            this.AddRange(source.Skip(pageIndex * pageSize).Take(pageSize).ToList());
        }
    }


    public PagedList(IEnumerable<T> source, int pageIndex, int pageSize, int totalCount)
    {
        if (source.Count() > 0)
        {
            TotalCount = totalCount;
            TotalPages = TotalCount / pageSize;

            if (TotalCount % pageSize > 0)
                TotalPages++;

            this.PageSize = pageSize;
            this.PageIndex = pageIndex;
            this.AddRange(source.Skip(pageIndex * pageSize).Take(pageSize).ToList());

        }
    }

    public int PageIndex { get; private set; }
    public int PageSize { get; private set; }
    public int TotalCount { get; private set; }
    public int TotalPages { get; private set; }

    public bool HasPreviousPage
    {
        get { return (PageIndex > 0); }
    }
    public bool HasNextPage
    {
        get { return (PageIndex + 1 < TotalPages); }
    }
}