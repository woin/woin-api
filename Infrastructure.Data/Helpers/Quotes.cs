﻿using System.Linq;

public static class Quotes
{
    public static string GetQuotes(string[] values)
    {
        string str = "";
        int count = values.Count();
        for (int i = 0; i < count; i++)
        {
            if (count == i + 1)
                str += $"'{values[i]}'";
            else
                str += $"'{values[i]}',";
        }
        return str;
    }
}