﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name UnitOfWork
 * @file Infrastructure/UnitOfWork
 * @observations use Base Infrastructure for System
 * @HU 0: Dominio
 * @task 4 Crear Contexto
 */

using Domain.Interface;
using Infrastructure.Data.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.UnitsOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private  IDbContext _context;

      

        public UnitOfWork(IDbContext context)
        {

            _context = context;
           

        }


        public int Commit()
        {
            return _context.SaveChanges();
        }

        public virtual async Task<int> Commit(bool Async = true)
        {
            return await _context.SaveChangesAsync();
        }

        public virtual void CommitCurrent()
        {
            _context.Database.BeginTransaction().Commit();
        }

        public virtual void RollBackCurrent()
        {
            _context.Database.BeginTransaction().Rollback();
        }


        public virtual async Task CommitCurrentAsync()
        {
            await _context.Database.BeginTransaction().CommitAsync();
        }

        public virtual async Task RollBackCurrentAsync()
        {
            await _context.Database.BeginTransaction().RollbackAsync();
        }

        public virtual void CloseCurrent()
        {
            _context.Database.CloseConnection();
        }

        public virtual async Task CloseCurrentAsyn()
        {
            await _context.Database.CloseConnectionAsync();
        }

        public virtual IDbContextTransaction BeginTransaction()
        {
            return _context.Database.BeginTransaction();
        }

        public virtual async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return await _context.Database.BeginTransactionAsync();
        }


        public void Dispose()
        {
            Dispose(true);
        }
        private void Dispose(bool disposing)
        {
            if (disposing && _context != null)
            {
                ((DbContext)_context).Dispose();
                _context = null;
            }
        }
    }
}
