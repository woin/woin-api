﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Base
 * @file Infrastructure/Base
 * @observations use Base Infrastructure for System
 * @HU 0: Dominio
 * @task 4 Crear Contexto
 */

using Domain.Base;
using Infrastructure.Data.Interface;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Data.Base
{
    public class DbContextBase : DbContext, IDbContext
    {


        public DbContextBase(DbContextOptions options) : base(options)
        {
            DefaultSchema = "woinapp";
            
        }
        public DbContextBase()
        {
            DefaultSchema = "woinapp";
        }

        public Action<string> Log { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string DefaultSchema { get ; set ; }
       
        public Dictionary<string, string> ListEntitiesTables { get ; set ; }
        public string Table { get; set; }
        public override ChangeTracker ChangeTracker { get => base.ChangeTracker; }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }


        public override int SaveChanges()
        {
           
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await base.SaveChangesAsync(cancellationToken);
        }

        
    }
}
