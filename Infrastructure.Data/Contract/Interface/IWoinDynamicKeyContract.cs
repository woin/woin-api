/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name IContractWoinDynamicKey
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;

namespace Infrastructure.Data.Contract
{

	public interface IWoinDynamicKeyContract : IRepository<WoinDynamicKey>
	{
        /*Create your code No generic in repository*/


        public WoinDynamicKey Get(int id);
        public WoinDynamicKey GetDynamicKey(int key, short state);
        public WoinDynamicKey GetDynamicKey(int dynamicKey, string key, int value /*woinerId, transactionId*/,  short state);
        public IEnumerable<WoinDynamicKey> GetDynamicKey(string key, int value /*woinerId, transactionId*/, int pageIndex = 0, int pageSize = int.MaxValue);
        public IEnumerable<WoinDynamicKey> GetDynamicKey(short state, int pageIndex = 0, int pageSize = int.MaxValue);

        public WoinDynamicKey GetWoinDynamicKey(int woinWoinerId, int key, short state, int time = 30, int pageIndex = 0, int pageSize = int.MaxValue);


    }

}

