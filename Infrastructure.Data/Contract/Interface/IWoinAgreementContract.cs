/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name IContractWoinAgreement
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;

namespace Infrastructure.Data.Contract
{

	public interface IWoinAgreementContract : IRepository<WoinAgreement>
	{

        public WoinAgreement Get(int id);

        public IEnumerable<WoinAgreement> GetWoinAgreements(string key, string value, int pageIndex = 0, int pageSize = int.MaxValue);

        public IEnumerable<WoinAgreement> GetWoinAgreements(short state, int pageIndex = 0, int pageSize = int.MaxValue);

        /*Create your code No generic in repository*/
    }

}

