/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractWoinUser
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Contract
{

	public class WoinUserContract : Repository<WoinUser>, IWoinUserContract
	{
        /*Create your code No generic in repository*/
        public WoinUserContract(IDbContext context) : base(context, "woin_users"){
        }

        public WoinUser Get(int id)
        {
            return base.Find(id);
        }



        public WoinUser GetWoinUser(string key, string value)
        {
            return base.Where(key, value).FirstOrDefault();
        }

        public IEnumerable<WoinUser> GetWoinUsers(short state, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.State == state, pageIndex, pageSize);
        }

        public IEnumerable<WoinUser> GetWoinUsers(int roleId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.RoleId == roleId, pageIndex, pageSize);
        }
    }
}

