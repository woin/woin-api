/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractWoinAd
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data.Contract
{

    public class WoinAdContract : Repository<WoinAd>, IWoinAdContract
    {
		public WoinAdContract(IDbContext context) : base(context, "woin_ads"){}

        public WoinAd Get(int id)
        {
            return base.Find(id);
        }

        public IEnumerable<WoinAd> GetAds(string title, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.GetLike("title", title, pageIndex: pageIndex, pageSize: pageSize);
        }

        public IEnumerable<WoinAd> GetAds(string key, int value, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.Where(key, value, pageIndex: pageIndex, pageSize: pageSize);
        }

        public IEnumerable<WoinAd> GetAds(string key, short value, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.Where(key, value, pageIndex: pageIndex, pageSize: pageSize);
        }

        public IEnumerable<WoinAd> GetAds(string key, long value, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.Where(key, value, pageIndex: pageIndex, pageSize: pageSize);
        }

        public IEnumerable<WoinAd> GetAds(decimal price, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.Price == price, pageIndex, pageSize);
        }

        public IEnumerable<WoinAd> GetAdsParent(int parent, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.AdParent == parent, pageIndex, pageSize);
        }


        public IEnumerable<WoinAd> GetPreference(int woinerId, int pageCurrent = 0, int totalPage = int.MaxValue, int type = 0)
        {
            return FromSqlRaw("SELECT * FROM get_ads_by_preferences({0},{1},{2},{3})", woinerId, pageCurrent, totalPage, type)
                 ?.ToList();
        }


        /*Create your code No generic in repository*/



    }
}

