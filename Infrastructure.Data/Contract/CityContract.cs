/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractCity
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data.Contract
{

	public class CityContract : Repository<City>, ICityContract
	{
		public CityContract(IDbContext context) : base(context, "cities") {}

        

        public City Get(int id)
        {
            return base.Find(id);
        }

        public IEnumerable<City> GetCities(int GovernorateId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.GovernorateId == GovernorateId).Paginate(pageIndex, pageSize);
        }

        public IEnumerable<City> GetCities(string nameLike, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.GetLike("name", nameLike, pageSize: pageSize, pageIndex : pageIndex);
        }

        public IEnumerable<City> GetCities(short state, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.State == state, pageIndex, pageSize);
        }

        public City GetCity(string name, int governorateId)
        {
            return base.FindBy(x => x.Name == name && x.GovernorateId == governorateId).FirstOrDefault();
        }
        /*Create your code No generic in repository*/
    }
}

