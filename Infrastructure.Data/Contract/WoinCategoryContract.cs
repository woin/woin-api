/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractWoinCategory
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data.Contract
{

	public class WoinCategoryContract : Repository<WoinCategory>, IWoinCategoryContract
	{
		public WoinCategoryContract(IDbContext context) : base(context){}

        public WoinCategory Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<WoinCategory> GetWoinCategories(short state, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            throw new NotImplementedException();
        }

        public WoinCategory WoinCategory(string name)
        {
            throw new NotImplementedException();
        }
        /*Create your code No generic in repository*/
    }
}

