/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractWoinWoiner
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data.Contract
{

	public class WoinWoinerContract : Repository<WoinWoiner>, IWoinWoinerContract
	{
		public WoinWoinerContract(IDbContext context) : base(context){}

        public WoinWoiner GetWoiner(int id)
        {
            return base.Find(id);
        }

        public WoinWoiner GetWoiner(string codeWoiner)
        {
            return base.FindBy(x => x.Codewoiner == codeWoiner)?.FirstOrDefault();
        }

        public WoinWoiner GetWoiner(int id, int userId)
        {
            return base.FindBy(x => x.Id == id && x.UserId == userId)?.FirstOrDefault();
        }

        public WoinWoiner GetWoiner(int id, int userId, int roleId)
        {
            return base.FindBy(x => x.Id == id && x.UserId == userId && x.RoleId == roleId)?.FirstOrDefault();
        }

   
        public IEnumerable<WoinWoiner> GetWoiners(WoinMultimedia woinMultimedia)
        {
            return base.FindBy(x => x.ProfileId == woinMultimedia.Id).ToList();
        }

        public IEnumerable<WoinWoiner> GetWoiners(WoinRole woinRole)
        {
            return base.FindBy(x => x.RoleId == woinRole.Id).ToList();
        }

        public IEnumerable<WoinWoiner> GetWoiners(WoinUser woinUser)
        {
            return base.FindBy(x => x.UserId == woinUser.Id).ToList();
        }

        public IEnumerable<WoinWoiner> GetWoiners(WoinPerson woinPerson)
        {
            return base.FindBy(x => x.PersonId == woinPerson.Id).ToList();
        }

        public IEnumerable<WoinWoiner> GetWoiners(WoinWoiner reference)
        {
            return base.FindBy(x => x.Reference == reference.Id).ToList();
        }

        public IEnumerable<WoinWoiner> GetWoiners(string key, int value, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            throw new NotImplementedException();
        }
        /*Create your code No generic in repository*/
    }
}

