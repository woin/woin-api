/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractGovernorate
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data.Contract
{

	public class GovernorateContract : Repository<Governorate>, IGovernorateContract
	{
		public GovernorateContract(IDbContext context) : base(context, "governorates"){}

        public Governorate Get(int id)
        {
            return base.Find(id);
        }

        public IEnumerable<Governorate> GetGovernorates(string name, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.GetLike("name", name, pageIndex: pageIndex, pageSize: pageSize);
        }

        public IEnumerable<Governorate> GetGovernorates(short state, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.State == state);
        }

        public IEnumerable<Governorate> GetGovernorates(int countryId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return base.FindBy(x => x.CountryId == countryId, pageIndex: pageIndex, pageSize: pageSize);
        }
        /*Create your code No generic in repository*/
    }
}

