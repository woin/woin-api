/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name ContractWoinDynamicKey
 * @file Infrastructure/Data/Contract
 * @observations use Contract Repository System
 * @HU 0: Contract
 * @task 4 Create Contrac
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interface;
using Infrastructure.Data.Interface;
using Infrastructure.Data.Repositories;

namespace Infrastructure.Data.Contract
{

	public class WoinDynamicKeyContract : Repository<WoinDynamicKey>, IWoinDynamicKeyContract
	{
        private long unixTimestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970,1,1))).TotalMilliseconds;

		public WoinDynamicKeyContract(IDbContext context) : base(context){
        }

        public WoinDynamicKey Get(int id)
        {
            throw new NotImplementedException();
        }

        public WoinDynamicKey GetDynamicKey(int key, short state)
        {
            throw new NotImplementedException();
        }

        public WoinDynamicKey GetDynamicKey(int dynamicKey, string key, int value, short state)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<WoinDynamicKey> GetDynamicKey(string key, int value, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<WoinDynamicKey> GetDynamicKey(short state, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            throw new NotImplementedException();
        }

        public WoinDynamicKey GetWoinDynamicKey(int woinWoinerId, int key, short state, int time = 30, int pageIndex = 0, int pageSize = int.MaxValue)
        {
           return base.FindBy(x => x.WoinerId == woinWoinerId && x.DynamicKey == key && (unixTimestamp - x.CreatedAt) /1000 < time, pageIndex, pageSize)?.FirstOrDefault();
        }


        /*Create your code No generic in repository*/



    }
}

