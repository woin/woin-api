﻿/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app.woin.back-core
 * @since  0.1 rev
 * @author Carlos Andrés Castilla García <carlos-ac97@hotmail.com>
 * @name Context
 * @file Infrastructure/Context
 * @observations use Base Infrastructure for System
 * @HU 0: Dominio
 * @task 4 Crear Contexto
 */

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Data.Interface
{
    public interface IDbContext : IDisposable
    {
        DbSet<T> Set<T>() where T : class;
        Action<string> Log { get; set; }
        EntityEntry Entry(object entity);
        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        void SetModified(object entity);
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        public  ChangeTracker ChangeTracker { get; }
        public string DefaultSchema { set; get; }

        public string Table { set; get; }
        public Dictionary<string, string> ListEntitiesTables { set; get; }

        public DatabaseFacade Database { get; }


    }
}
